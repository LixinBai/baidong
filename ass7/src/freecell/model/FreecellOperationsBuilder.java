package freecell.model;

/**
 * This is the interface of a builder which can instantiate a freecell model.
 */
public interface FreecellOperationsBuilder {

  /**
   * Take the number of cascade piles and return the obtained builder.
   *
   * @param c number of cascade piles.
   * @return a builder whose number of cascade piles is c.
   */
  FreecellOperationsBuilder cascades(int c);

  /**
   * Take the number of open piles and return the obtained builder.
   *
   * @param o number of open piles.
   * @return a builder whose number of open piles is o.
   */
  FreecellOperationsBuilder opens(int o);

  /**
   * Instantiate a freecell model according to value assigned to it. If no value is assigned, assign
   * the default values.
   *
   * @param <K> the card type of this freecell model.
   * @return the resulted freecell model.
   */
  <K> FreecellOperations<K> build();
}
