package freecell.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import freecell.card.Card;
import freecell.card.Suit;
import freecell.card.Value;
import freecell.pile.CascadePile;
import freecell.pile.FoundationPile;
import freecell.pile.OpenPile;
import freecell.pile.Pile;

/**
 * This is a model for single-card-movement freecell game. This class can deal with all inputs from
 * controller and give a response. Specially, this model only allow single-card movements.
 */
public class FreecellModel implements FreecellOperations<Card> {
  protected Pile[] open;
  protected Pile[] cascade;
  private Pile[] foundation;
  protected boolean gameStart;

  protected FreecellModel(int cascades, int opens) {
    open = new OpenPile[opens];
    cascade = new CascadePile[cascades];
    foundation = new FoundationPile[4];
    reset();
    System.out.println("xxxxx");
    System.out.println("hey");
  }

  @Override
  public List<Card> getDeck() {
    List<Card> deck = new LinkedList<>();
    Suit[] s = {Suit.Club, Suit.Diamond, Suit.Heart, Suit.Spade};
    Value[] v = {Value.A, Value.Two, Value.Three, Value.Four, Value.Five, Value.Six,
        Value.Seven, Value.Eight, Value.Nine, Value.Ten, Value.J, Value.Q, Value.K};
    for (Suit suit : s) {
      for (Value value : v) {
        deck.add(new Card(suit, value));
      }
    }
    return deck;
  }

  private void reset() {
    gameStart = false;
    for (int i = 0; i < open.length; i++) {
      open[i] = new OpenPile();
    }
    for (int i = 0; i < cascade.length; i++) {
      cascade[i] = new CascadePile();
    }
    for (int i = 0; i < foundation.length; i++) {
      foundation[i] = new FoundationPile();
    }
  }

  @Override
  public void startGame(List<Card> deck, boolean shuffle) throws IllegalArgumentException {
    reset();
    if (deck == null) {
      throw new IllegalArgumentException("Invalid deck: the deck is null.");
    }
    if (!validDeck(deck)) {
      throw new IllegalArgumentException("Invalid deck: the deck is invalid.");
    }
    gameStart = true;
    List d = new LinkedList(deck);
    if (shuffle) {
      Collections.shuffle(d);
    }
    distribute(d);
  }

  private boolean validDeck(List<Card> deck) {
    if (deck.size() != 52) {
      return false;
    }
    HashSet<Card> deckHash = new HashSet<>(deck);
    return deckHash.size() == 52;
  }

  private void distribute(List<Card> deck) {
    while (deck.size() > 0) {
      for (int i = 0; i < cascade.length; i++) {
        if (deck.size() > 0) {
          CascadePile c = (CascadePile) cascade[i];
          cascade[i] = c.distribute(deck.get(0));
          deck.remove(0);
        } else {
          break;
        }
      }
    }
  }

  /**
   * Move a card from the given source pile to the given destination pile, if the move is valid.
   * Only the last card in a pile can be moved to another pile. Any other kind of move is invalid.
   *
   * @param source         the type of the source pile see {@link PileType}
   * @param pileNumber     the pile number of the given type, starting at 0
   * @param cardIndex      the index of the card to be moved from the source pile, starting at 0.
   * @param destination    the type of the destination pile {@link PileType}
   * @param destPileNumber the pile number of the given type, starting at 0
   * @throws IllegalArgumentException if the move is not possible or the card to move is not on top
   *                                  of the source pile
   * @throws IllegalStateException    if a move is attempted before the game has starts of after the
   *                                  game is over
   */
  @Override
  public void move(PileType source, int pileNumber, int cardIndex,
                   PileType destination, int destPileNumber)
          throws IllegalArgumentException, IllegalStateException {
    if (!isGameOver() && gameStart) {
      Pile from = getPile(source, pileNumber);
      Pile to = getPile(destination, destPileNumber);
      if (cardIndex != from.getSize() - 1) {
        throw new IllegalArgumentException("Invalid move: you can only move one card at a time.");
      }
      if (from != to) {
        to.addCard(from.findCard(cardIndex));
        from.removeCard(cardIndex);
      } else {
        if (source == PileType.CASCADE) {
          from.virtualMove(cardIndex);
        }
      }
    } else {
      throw new IllegalStateException("Invalid move: Game is over or the game has not started.");
    }
  }

  protected Pile getPile(PileType type, int number) throws IllegalArgumentException {
    if (type == null) {
      throw new IllegalArgumentException("Pile type invalid.");
    }
    switch (type) {
      case OPEN:
        if (number >= open.length) {
          throw new IllegalArgumentException("Open pile " + number + "does not exist.");
        }
        return open[number];
      case CASCADE:
        if (number >= cascade.length) {
          throw new IllegalArgumentException("Cascade pile " + number + "does not exist.");
        }
        return cascade[number];
      case FOUNDATION:
        if (number >= foundation.length) {
          throw new IllegalArgumentException("Foundation pile " + number + "does not exist.");
        }
        return foundation[number];
      default:
        throw new IllegalArgumentException("Pile type invalid.");
    }
  }

  @Override
  public boolean isGameOver() {
    if (!gameStart) {
      return false;
    }
    for (Pile p :
            foundation) {
      if (p.getSize() != 13) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String getGameState() {
    if (!gameStart) {
      return "";
    }
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < foundation.length; i++) {
      s.append("F");
      s.append(i + 1);
      s.append(":");
      s.append(foundation[i].toString());
      s.append("\n");
    }
    for (int i = 0; i < open.length; i++) {
      s.append("O");
      s.append(i + 1);
      s.append(":");
      s.append(open[i].toString());
      s.append("\n");
    }
    for (int i = 0; i < cascade.length; i++) {
      s.append("C");
      s.append(i + 1);
      s.append(":");
      s.append(cascade[i].toString());
      if (i < cascade.length - 1) {
        s.append("\n");
      }
    }
    return s.toString();
  }

  /**
   * Return a builder of this freecell model to instantiate a freecell model.
   *
   * @return a builder of this freecell model.
   */
  public static FreecellBuilder getBuilder() {
    return new FreecellBuilder();
  }

  /**
   * This is a freecell builder which can instantiate a freecell model.
   */
  public static class FreecellBuilder implements FreecellOperationsBuilder {
    protected int cascades;
    protected int opens;

    /**
     * Initialize a freecell builder and assign default values to all the fields as above.
     */
    public FreecellBuilder() {
      cascades = 8;
      opens = 4;
    }

    @Override
    public FreecellOperationsBuilder cascades(int c) throws IllegalArgumentException {
      if (c < 4) {
        throw new IllegalArgumentException("Minimum number of cascade piles is 4.");
      }
      this.cascades = c;
      return this;
    }

    @Override
    public FreecellOperationsBuilder opens(int o) throws IllegalArgumentException {
      if (o < 1) {
        throw new IllegalArgumentException("Minimum number of open piles is 1.");
      }
      this.opens = o;
      return this;
    }

    @Override
    public FreecellOperations build() {

      return new FreecellModel(cascades, opens);
    }
  }

}
