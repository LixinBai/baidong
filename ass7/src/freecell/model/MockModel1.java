package freecell.model;

import java.util.List;

import freecell.card.Card;

/**
 * This mock model is to test whether the controller transmit the input of move and the output of
 * getGameState correctly.
 */
public class MockModel1 implements FreecellOperations<Card> {
  private StringBuilder log;
  private final String uniqueString;

  /**
   * Initialize a mock model to test the controller.
   *
   * @param log          a log to test the input of move is transmitted correctly.
   * @param uniqueString a string to test the output of getGameState is transmitted correctly.
   */
  public MockModel1(StringBuilder log, String uniqueString) {
    this.log = log;
    this.uniqueString = uniqueString;
  }

  @Override
  public List<Card> getDeck() {
    // This method is not tested since the controller does not transmit input or output to/from it.
    return null;
  }

  @Override
  public void startGame(List<Card> deck, boolean shuffle) throws IllegalArgumentException {
    // This method is not tested in this mock model. It will be tested in MockModel2.
  }

  @Override
  public void move(PileType source, int pileNumber, int cardIndex,
                   PileType destination, int destPileNumber)
          throws IllegalArgumentException, IllegalStateException {
    // This mock model only test whether the controller transmit input and output correctly without
    // an exception. Exception will be tested in MockModel2.
    log.append("Move card starting at index ").append(cardIndex + 1).append(" from ")
            .append(source.toString()).append(" pile ").append(pileNumber + 1).append(" to ")
            .append(destination.toString()).append(" pile ").append(destPileNumber + 1)
            .append(".\n");
  }

  @Override
  public boolean isGameOver() {
    // This method is not tested in this mock model. It will be tested in MockModel2.
    return false;
  }

  @Override
  public String getGameState() {
    return uniqueString;
  }
}
