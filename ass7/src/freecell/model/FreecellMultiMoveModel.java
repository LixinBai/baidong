package freecell.model;

import freecell.pile.Pile;

import static freecell.model.PileType.CASCADE;

/**
 * This is a model for multi-card-movement freecell game. this class can deal with all inputs from
 * controller and give a response. Specially,this model only allow movements of a list of card when
 * satisfying some conditions.
 */
public class FreecellMultiMoveModel extends FreecellModel {
  private FreecellMultiMoveModel(int cascades, int opens) {
    super(cascades, opens);
  }

  /**
   * Move one or more cards from the given source pile to the given destination pile, if the move is
   * valid.
   *
   * <p> When both source and destination piles are cascade pile, multiple-card movement is allowed
   * when the cards obey following conditions: </p>
   * <p> 1. they should form a valid build, i.e. they should be arranged in alternating colors and
   * consecutive, descending values in the cascade pile that they are moving from </p>
   * <p> 2. these cards should form a build with the last card in the destination cascade pile </p>
   * <p> 3. the maximum number of cards that can be moved when there are 𝑁 free open piles and 𝐾
   * empty cascade piles is (𝑁+1)∗2^𝐾 </p>
   *
   * <p>Otherwise, only one card is allowed to move </p>
   *
   * @param source         the type of the source pile see {@link PileType}
   * @param pileNumber     the pile number of the given type, starting at 0
   * @param cardIndex      the index of the card or the first card in the list to be moved from the
   *                       source pile, starting at 0
   * @param destination    the type of the destination pile {@link PileType}
   * @param destPileNumber the pile number of the given type, starting at 0
   * @throws IllegalArgumentException if the move is not possible or the card to move is not on top
   *                                  of the source pile, or the list of card to move does not
   *                                  satisfy all conditions
   * @throws IllegalStateException    if a move is attempted before the game has starts of after the
   *                                  game is over
   */
  @Override
  public void move(PileType source, int pileNumber, int cardIndex,
                   PileType destination, int destPileNumber)
          throws IllegalArgumentException, IllegalStateException {
    if (!isGameOver() && gameStart) {
      Pile from = getPile(source, pileNumber);
      Pile to = getPile(destination, destPileNumber);
      if (source == CASCADE && destination == CASCADE) {
        int cardNumber = from.getSize() - cardIndex;
        if (withinThreshold(cardNumber)) {
          if (from != to) {
            //move
            to.addCard(from.findCard(cardIndex));
            from.removeCard(cardIndex);
          } else {
            from.virtualMove(cardIndex);
          }
        } else {
          throw new IllegalArgumentException("Invalid move: number of cards to move "
                  + "exceeds threshold.");
        }
      } else {
        if (cardIndex != from.getSize() - 1) {
          throw new IllegalArgumentException("Invalid move: you can only move one card"
                  + " to your destination pile type at a time.");
        }
        if (from != to) {
          to.addCard(from.findCard(cardIndex));
          from.removeCard(cardIndex);
        }
      }
    } else {
      throw new IllegalStateException("Invalid move: Game is over or the game has not started.");
    }
  }

  // the maximum number of cards that can be moved is (𝑁+1)∗2^K
  // where 𝑁 is the number of free open piles and 𝐾 os the number of empty cascade piles
  private boolean withinThreshold(int cardNumber) {
    int n = 0;
    for (Pile p : this.open) {
      if (p.isEmpty()) {
        n++;
      }
    }

    int k = 0;
    for (Pile p : this.cascade) {
      if (p.isEmpty()) {
        k++;
      }
    }
    return cardNumber <= (n + 1) * Math.pow(2, k);
  }

  /**
   * Return a builder of this freecell multi-move model to instantiate a freecell multi-move model.
   *
   * @return a builder of this freecell multi-move model.
   */
  public static FreecellMultiMoveBuilder getBuilder() {
    return new FreecellMultiMoveBuilder();
  }

  /**
   * This is a freecell multi-move builder which can instantiate a freecell multi-move model.
   */
  public static class FreecellMultiMoveBuilder
          extends FreecellBuilder implements FreecellOperationsBuilder {
    @Override
    public FreecellOperations build() {
      return new FreecellMultiMoveModel(cascades, opens);
    }
  }
}
