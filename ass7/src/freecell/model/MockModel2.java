package freecell.model;

import java.util.List;

import freecell.card.Card;

/**
 * This mock model is to test whether the controller transmit the input of startGame, the output of
 * isGameOver and an IllegalArgumentException from move correctly.
 */
public class MockModel2 implements FreecellOperations<Card> {
  private StringBuilder log;
  private final String uniqueString;
  private boolean gameOver;

  /**
   * Initialize a mock model to test the controller.
   *
   * @param log          a log to test the input of startGame is transmitted correctly.
   * @param uniqueString a string to test an IllegalArgumentException from move is transmitted
   *                     correctly.
   * @param gameOver     a boolean to test the output of isGameover is transmitted correctly.
   */
  public MockModel2(StringBuilder log, String uniqueString, boolean gameOver) {
    this.log = log;
    this.uniqueString = uniqueString;
    this.gameOver = gameOver;
  }

  @Override
  public List<Card> getDeck() {
    // This method is not tested since the controller does not transmit input or output to/from it.
    return null;
  }

  @Override
  public void startGame(List<Card> deck, boolean shuffle) throws IllegalArgumentException {
    log.append("deck:").append(deck.toString()).append("\nshuffle:").append(shuffle);
  }

  @Override
  public void move(PileType source, int pileNumber, int cardIndex,
                   PileType destination, int destPileNumber)
          throws IllegalArgumentException, IllegalStateException {
    // This mock model only test whether the controller transmit an exception correctly.
    // Input and output without an exception will be tested in MockModel1.
    throw new IllegalArgumentException(uniqueString);
  }

  @Override
  public boolean isGameOver() {
    return gameOver;
  }

  @Override
  public String getGameState() {
    // This method is not tested in this mock model. It will be tested in MockModel1.
    return "game state";
  }
}
