package freecell.pile;

import java.util.LinkedList;
import java.util.List;

import freecell.card.Card;

/**
 * This class represents all possible operations on a cascade pile. a cascade pile may initially
 * contain cards in any order. However, a card from some pile can be moved to the end of a cascade
 * pile if and only if its color is different from that of the currently last card, and its value is
 * exactly one less than that of the currently last card.
 */
public class CascadePile extends PileImpl {

  private CascadePile(List<Card> list) {
    super();
    super.list = list;
  }

  /**
   * A constructor for a cascade pile.
   */
  public CascadePile() {
    super();
  }

  /**
   * Set the initial order of this cascade pile.
   *
   * @param c the card to be added
   * @return the updated pile with given card added
   */
  public Pile distribute(Card c) {
    list.add(c);
    return this;
  }

  /**
   * Add the given card to the top of this pile.
   *
   * @param cards the card to be added
   * @throws IllegalArgumentException if the card to be added has the same color with the card on
   *                                  the top of this pile, or the value of the given card is not
   *                                  exactly one less than that of the top card
   */
  @Override
  public void addCard(List<Card> cards) throws IllegalArgumentException {
    if (isBuild(cards)) {
      if (list.isEmpty()) {
        list.addAll(cards);
      } else {
        //check suit and value
        if ((list.get(list.size() - 1).differentColor(cards.get(0)))
                && list.get(list.size() - 1).valueCompareTo(cards.get(0), 1)) {
          list.addAll(cards);
        } else {
          throw new IllegalArgumentException("Invalid move: the card to bee added should "
                  + "have different color with top card "
                  + "and its value must one less than top card.");
        }
      }
    } else {
      throw new IllegalArgumentException("Invalid move: cards do not form a build.");
    }
  }

  @Override
  public void virtualMove(int index) {
    List<Card> intermediate = new LinkedList<>();
    list.forEach(c -> intermediate.add(c));
    Pile copy = new CascadePile(intermediate);

    List<Card> add = copy.findCard(index);
    copy.removeCard(index);
    copy.addCard(add);
  }

  private boolean isBuild(List<Card> cards) {
    boolean valid = true;
    if (cards.size() == 1) {
      return true;
    } else {
      for (int i = 1; i < cards.size(); i++) {
        if (!cards.get(i - 1).valueCompareTo(cards.get(i), 1)) {
          valid = false;
        }
      }
    }
    return valid;
  }

}
