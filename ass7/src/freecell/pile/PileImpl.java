package freecell.pile;

import java.util.LinkedList;
import java.util.List;

import freecell.card.Card;

/**
 * This is an abstract of all common operations for piles.
 */
public abstract class PileImpl implements Pile {
  protected List<Card> list;

  /**
   * Initialize a new empty pile.
   */
  PileImpl() {
    list = new LinkedList<>();
  }

  @Override
  public abstract void addCard(List<Card> cards) throws IllegalArgumentException;

  @Override
  public void removeCard(int index) {
    list = list.subList(0, index);
  }

  @Override
  public List<Card> findCard(int index) throws IllegalArgumentException {
    if (index >= 0 && index < list.size()) {
      return list.subList(index, getSize());
    } else {
      throw new IllegalArgumentException("Invalid move: this card index "
              + "does not exist in this pile.");
    }
  }

  @Override
  public int getSize() {
    return list.size();
  }

  @Override
  public String toString() {
    if (list.size() == 0) {
      return "";
    }
    StringBuilder s = new StringBuilder();
    s.append(" ");
    for (int i = 0; i < list.size(); i++) {
      s.append(list.get(i).toString());
      if (i < list.size() - 1) {
        s.append(", ");
      }
    }
    return s.toString();
  }

  @Override
  public boolean isEmpty() {
    return list.isEmpty();
  }

  @Override
  public void virtualMove(int index) {
    //do nothing
  }
}
