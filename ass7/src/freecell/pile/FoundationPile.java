package freecell.pile;

import java.util.List;

import freecell.card.Card;
import freecell.card.Suit;

import static freecell.card.Value.A;

/**
 * This class represents all possible operations on a foundation pile. A foundation pile holds cards
 * with a specific suit in increasing order of its value.
 */
public class FoundationPile extends PileImpl {
  Suit suit;

  /**
   * Construct a foundation pile.
   */
  public FoundationPile() {
    super();
    this.suit = null;
  }

  /**
   * Add the given card to the top of this pile. A card can be added to a foundation pile if and
   * only if its suit matches that of the pile, and its value is one more than that of the card
   * currently on top of the pile. If a foundation pile is currently empty, any ace can be added to
   * it.
   *
   * @param cards the card to be added
   * @throws IllegalArgumentException if the card is not "Ace", or its suit/value does not fit in
   *                                  this pile
   */
  @Override
  public void addCard(List<Card> cards) throws IllegalArgumentException {
    if (cards.size() != 1) {
      throw new IllegalArgumentException("Invalid move: only one card "
              + "can be added to a foundation pile");
    }
    Card c = cards.get(0);
    if (list.isEmpty()) {
      //if value is A, add, set suit
      if (c.getValue() == A) {
        list.add(c);
        this.suit = c.getSuit();
      } else {
        throw new IllegalArgumentException("Invalid move: first card in foundation pile "
                + "should be A");
      }
    } else {
      //judge suit and value
      if ((c.getSuit() == this.suit) && list.get(list.size() - 1).valueCompareTo(c, -1)) {
        list.add(c);
      } else {
        throw new IllegalArgumentException("Invalid move: card to be added should fit in the suit "
                + "and its value must one more than top card.");
      }
    }
  }


}
