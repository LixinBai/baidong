package freecell.pile;

import java.util.List;

import freecell.card.Card;

/**
 * This class represents all possible operations on an open pile. An open pile may contain at most
 * one card. An open pile is usually used as a temporary buffer during a freecell game to hold
 * cards.
 */
public class OpenPile extends PileImpl {

  /**
   * Add the given card to this open pile.
   *
   * @param cards the card to be added
   * @throws IllegalArgumentException if this pile has already held a card
   */
  @Override
  public void addCard(List<Card> cards) throws IllegalArgumentException {
    if (cards.size() != 1) {
      throw new IllegalArgumentException("Invalid move: only one card "
              + "can be added to an open pile.");
    }
    Card c = cards.get(0);
    if (list.isEmpty()) {
      list.add(c);
    } else {
      throw new IllegalArgumentException("Invalid move: an open pile may contain "
              + "at most one card.");
    }
  }
}
