package freecell.pile;

import java.util.List;

import freecell.card.Card;

/**
 * This interface represents all possible operations on a pile of cards. Only the card at the top of
 * this pile can be added or removed at one time.
 */
public interface Pile {

  /**
   * Add the given list of cards to the top of this pile.
   *
   * @param cards the list of cards to be added.
   * @throws IllegalStateException if the addition of the card failed.
   */
  void addCard(List<Card> cards) throws IllegalArgumentException;


  /**
   * Remove a list of cards from the given index in this pile.
   *
   * @param index the index of the card to be removed.
   */
  void removeCard(int index);

  /**
   * Find the list of cards starting from given index.
   *
   * @param index the index of ths first card in the list to be found.
   * @return the list of card starting from given index.
   * @throws IllegalStateException if this pile is empty or the index does not exist in this pile
   */
  List<Card> findCard(int index) throws IllegalArgumentException;

  /**
   * Get the size of this pile.
   *
   * @return the size of this pile.
   */
  int getSize();

  /**
   * Judge whether the pile is empty.
   *
   * @return whether the pile is empty
   */
  boolean isEmpty();

  /**
   * This is a helper when a pile try to move cards to itself.
   *
   * @param index the index of cards to be moved
   */
  void virtualMove(int index);

}
