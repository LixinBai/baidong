package freecell.card;

import static freecell.card.Suit.Club;
import static freecell.card.Suit.Spade;

/**
 * This class represents a card in poker. There are four suits: clubs (♣), diamonds (♦), hearts (♥),
 * and spades (♠). Hearts and diamonds are colored red; clubs and spades are colored black. There
 * are thirteen values: ace (written A), two through ten (written 2 through 10), jack (J), queen (Q)
 * and king (K).
 */
public class Card {
  private final Suit suit;
  private final Value value;

  /**
   * Construct a card.
   *
   * @param suit  the suit of the card.
   * @param value the value of the card.
   */
  public Card(Suit suit, Value value) {
    this.suit = suit;
    this.value = value;
  }

  /**
   * Get the suit of this card.
   *
   * @return the suit of this card.
   */
  public Suit getSuit() {
    return suit;
  }

  /**
   * Get the value of this card.
   *
   * @return the value of this card.
   */
  public Value getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value.toString() + suit.toString();
  }

  /**
   * Judge whether the given card has different color with this card.
   *
   * @param another the card to be compared to this card.
   * @return the judgement whether the two card have different color.
   */
  public boolean differentColor(Card another) {
    return this.getColor() != another.getColor();
  }

  public boolean valueCompareTo(Card c, int different) {
    return this.value.compareTo(c.getValue()) == different;
  }

  private int getColor() {
    if (suit == Club || suit == Spade) {
      return 1;
    }
    return 2;
  }

}
