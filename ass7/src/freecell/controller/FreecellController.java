package freecell.controller;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import freecell.card.Card;
import freecell.model.FreecellOperations;
import freecell.model.PileType;

/**
 * A freecell game controller which will work with the a FreeCell model to provide a game of
 * freecell.
 */
public class FreecellController implements IFreecellController<Card> {
  final Readable rd;
  final Appendable ap;

  /**
   * Initialize the frecell controller with given input and ouput.
   *
   * @param rd the input.
   * @param ap the output.
   * @throws IllegalArgumentException if and only if the readable or appendable objects are null.
   */
  public FreecellController(Readable rd, Appendable ap) throws IllegalArgumentException {
    if (rd == null || ap == null) {
      throw new IllegalArgumentException("Null readable or null appendable.");
    }
    this.rd = rd;
    this.ap = ap;
  }

  private PileType getPileType(char type) {
    switch (type) {
      case 'C':
        return PileType.CASCADE;
      case 'O':
        return PileType.OPEN;
      case 'F':
        return PileType.FOUNDATION;
      default:
        return null; // no other case
    }
  }

  private String inputPile(Scanner scan, Appendable ap) throws IOException {
    while (true) {
      if (!scan.hasNext()) {
        return null;
      }
      String input = scan.next();
      Pattern pile = Pattern.compile("^([OFC])([1-9]\\d*)$");
      Pattern quit = Pattern.compile("[Qq]+");
      Matcher p = pile.matcher(input);
      Matcher q = quit.matcher(input);
      if (q.find()) {
        return "q";
      }
      if (p.find()) {
        return p.group(0);
      } else {
        ap.append("Invalid pile: Please input pile again.\n");
      }
    }
  }

  private String inputCard(Scanner scan, Appendable ap) throws IOException {
    while (true) {
      if (!scan.hasNext()) {
        return null;
      }
      String input = scan.next();
      Pattern cardIndex = Pattern.compile("^[1-9]\\d*$");
      Pattern quit = Pattern.compile("[Qq]");
      Matcher c = cardIndex.matcher(input);
      Matcher q = quit.matcher(input);
      if (q.find()) {
        return "q";
      }
      if (c.find()) {
        return c.group(0);
      }
      ap.append("Invalid card index: Please input card index again.\n");
    }
  }

  private boolean quit(String match) throws IOException {
    if (match == null) {
      return true;
    }
    if (match.equals("q")) {
      ap.append("Game quit prematurely.");
      return true;
    }
    return false;
  }

  @Override
  public void playGame(List<Card> deck, FreecellOperations<Card> model, boolean shuffle)
          throws IllegalArgumentException, IllegalStateException {
    PileType source = null;
    int pileNumber = 0;
    int cardIndex = 0;
    PileType destination = null;
    int destPileNumber = 0;
    String match;
    State state = State.SourcePile;
    Scanner scan = new Scanner(rd);

    if (deck == null || model == null) {
      throw new IllegalArgumentException("Null deck or null model.");
    }
    model.startGame(deck, shuffle);
    try {
      ap.append(model.getGameState()).append("\n");
      while (!model.isGameOver()) {
        switch (state) {
          case SourcePile:
            match = inputPile(scan, ap);
            if (quit(match)) {
              return;
            }
            source = getPileType(match.charAt(0));
            pileNumber = Integer.valueOf(match.substring(1));
            state = State.CardIndex;
            break;
          case CardIndex:
            match = inputCard(scan, ap);
            if (quit(match)) {
              return;
            }
            cardIndex = Integer.valueOf(match);
            state = State.DestinationPile;
            break;
          case DestinationPile:
            match = inputPile(scan, ap);
            if (quit(match)) {
              return;
            }
            destination = getPileType(match.charAt(0));
            destPileNumber = Integer.valueOf(match.substring(1));
            state = State.Process;
            break;
          case Process:
            try {
              model.move(source, pileNumber - 1, cardIndex - 1,
                      destination, destPileNumber - 1);
              ap.append(model.getGameState()).append("\n");
            } catch (IllegalArgumentException e) {
              ap.append("Invalid move. Try again.").append(e.getMessage()).append("\n");
            }
            state = State.SourcePile;
            break;
          default: // no other case
        }
      }
      ap.append(model.getGameState()).append("\n");
      ap.append("Game over.\n");
    } catch (IOException e) {
      throw new IllegalStateException("The Readable or Appendable object failed.");
    }
  }

}

