package freecell.controller;

/**
 * <li>This enum represent four states which a frecell controller may have.</li>
 * <li>SourcePile represents that the controller is waiting client to input a source pile.</li>
 * <li>CardIndex represents that the controller is waiting client to input a card index.</li>
 * <li>DestinationPile represents that the controller is waiting client to input a destination
 * pile.</li>
 * <li>Process represents that the controller is doing the move operation.</li>
 */
public enum State {
  SourcePile, CardIndex, DestinationPile, Process
}
