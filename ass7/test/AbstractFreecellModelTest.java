import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.LinkedList;

import static freecell.model.PileType.CASCADE;
import static freecell.model.PileType.FOUNDATION;
import static freecell.model.PileType.OPEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import freecell.model.FreecellOperations;
import freecell.model.FreecellModel;

public abstract class AbstractFreecellModelTest {
  FreecellOperations f;
  List deck;


  /**
   * Construct a frecell game.
   */
  @Before
  public void setUp() {
    f = init();
    deck = f.getDeck();
    f.startGame(deck, false);
  }

  public abstract FreecellOperations init();

  @Test
  public void build() {
    try {
      //default build
      f = FreecellModel.getBuilder().build();
      f.startGame(deck, false);
      assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\nO2:\nO3:\nO4:\n"
              + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
              + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
              + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
              + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
              + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
              + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
              + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
              + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", f.getGameState());

      //4 cascades (lowest bound)
      f = FreecellModel.getBuilder().cascades(4).build();
      f.startGame(deck, false);
      assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\nO2:\nO3:\nO4:\n"
              + "C1: A♣, 5♣, 9♣, K♣, 4♦, 8♦, Q♦, 3♥, 7♥, J♥, 2♠, 6♠, 10♠\n"
              + "C2: 2♣, 6♣, 10♣, A♦, 5♦, 9♦, K♦, 4♥, 8♥, Q♥, 3♠, 7♠, J♠\n"
              + "C3: 3♣, 7♣, J♣, 2♦, 6♦, 10♦, A♥, 5♥, 9♥, K♥, 4♠, 8♠, Q♠\n"
              + "C4: 4♣, 8♣, Q♣, 3♦, 7♦, J♦, 2♥, 6♥, 10♥, A♠, 5♠, 9♠, K♠", f.getGameState());

      //53 cascades
      f = FreecellModel.getBuilder().cascades(53).build();
      f.startGame(deck, false);
      assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\nO2:\nO3:\nO4:\n"
              + "C1: A♣\nC2: 2♣\nC3: 3♣\nC4: 4♣\nC5: 5♣\nC6: 6♣\nC7: 7♣\nC8: 8♣\nC9: 9♣\n"
              + "C10: 10♣\nC11: J♣\nC12: Q♣\nC13: K♣\nC14: A♦\nC15: 2♦\nC16: 3♦\nC17: 4♦\n"
              + "C18: 5♦\nC19: 6♦\nC20: 7♦\nC21: 8♦\nC22: 9♦\nC23: 10♦\nC24: J♦\nC25: Q♦\n"
              + "C26: K♦\nC27: A♥\nC28: 2♥\nC29: 3♥\nC30: 4♥\nC31: 5♥\nC32: 6♥\nC33: 7♥\n"
              + "C34: 8♥\nC35: 9♥\nC36: 10♥\nC37: J♥\nC38: Q♥\nC39: K♥\nC40: A♠\nC41: 2♠\n"
              + "C42: 3♠\nC43: 4♠\nC44: 5♠\nC45: 6♠\nC46: 7♠\nC47: 8♠\nC48: 9♠\nC49: 10♠\n"
              + "C50: J♠\nC51: Q♠\nC52: K♠\nC53:", f.getGameState());


      //1 open piles (lowest bound)
      f = FreecellModel.getBuilder().opens(1).build();
      f.startGame(deck, false);
      assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\n"
              + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
              + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
              + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
              + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
              + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
              + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
              + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
              + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", f.getGameState());


      //53 open piles
      f = FreecellModel.getBuilder().opens(53).build();
      f.startGame(deck, false);
      assertEquals("F1:\nF2:\nF3:\nF4:\n"
              + "O1:\nO2:\nO3:\nO4:\nO5:\nO6:\nO7:\nO8:\nO9:\nO10:\nO11:\nO12:\nO13:\nO14:\n"
              + "O15:\nO16:\nO17:\nO18:\nO19:\nO20:\nO21:\nO22:\nO23:\nO24:\nO25:\nO26:\nO27:\n"
              + "O28:\nO29:\nO30:\nO31:\nO32:\nO33:\nO34:\nO35:\nO36:\nO37:\nO38:\nO39:\nO40:\n"
              + "O41:\nO42:\nO43:\nO44:\nO45:\nO46:\nO47:\nO48:\nO49:\nO50:\nO51:\nO52:\nO53:\n"
              + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
              + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
              + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
              + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
              + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
              + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
              + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
              + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", f.getGameState());

    } catch (IllegalArgumentException e) {
      fail();
    }
  }

  @Test
  public void buildException() {
    try {
      f = FreecellModel.getBuilder().cascades(1).build();
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Minimum number of cascade piles is 4.", e.getMessage());
    }

    try {
      f = FreecellModel.getBuilder().opens(0).build();
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Minimum number of open piles is 1.", e.getMessage());
    }
  }

  @Test
  public void getDeck() {
    assertEquals("[A♣, 2♣, 3♣, 4♣, 5♣, 6♣, 7♣, 8♣, 9♣, 10♣, J♣, Q♣, K♣, "
            + "A♦, 2♦, 3♦, 4♦, 5♦, 6♦, 7♦, 8♦, 9♦, 10♦, J♦, Q♦, K♦, "
            + "A♥, 2♥, 3♥, 4♥, 5♥, 6♥, 7♥, 8♥, 9♥, 10♥, J♥, Q♥, K♥, "
            + "A♠, 2♠, 3♠, 4♠, 5♠, 6♠, 7♠, 8♠, 9♠, 10♠, J♠, Q♠, K♠]", deck.toString());
  }

  @Test
  public void startGameShuffle() {
    String noShuffle;
    List expectedD = new LinkedList(deck);
    f = FreecellModel.getBuilder().build();
    f.startGame(deck, false);
    noShuffle = f.getGameState();
    f.startGame(deck, true);
    assertEquals(expectedD, deck); // do not mutate the deck
    assertNotEquals(noShuffle, f.getGameState()); // check shuffle
  }

  //start game multiple times
  @Test
  public void startGameNoShuffleMultiple() {
    List expectedD = new LinkedList(deck);
    f.startGame(deck, false);
    assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\nO2:\nO3:\nO4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", f.getGameState());
    assertEquals(expectedD, deck); // do not mutate the deck
    f.startGame(deck, false);
    assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\nO2:\nO3:\nO4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", f.getGameState());
    assertEquals(expectedD, deck); // do not mutate the deck
  }


  //the given deck is null
  @Test
  public void startGameNullDeck() {
    try {
      f.startGame(null, false);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid deck: the deck is null.", e.getMessage());
    }
  }

  //given deck is an invalid deck
  @Test
  public void startGameInvalidDeck() {
    List newDeck;

    // less than 52 cards
    try {
      newDeck = new LinkedList(deck);
      newDeck.remove(0);
      f.startGame(newDeck, false);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid deck: the deck is invalid.", e.getMessage());
      assertEquals("", f.getGameState());
      assertEquals(false, f.isGameOver());
    }

    // more than 52 cards
    try {
      newDeck = new LinkedList(deck);
      newDeck.add(deck.get(0));
      f.startGame(newDeck, false);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid deck: the deck is invalid.", e.getMessage());
      assertEquals("", f.getGameState());
      assertEquals(false, f.isGameOver());
    }

    // 52 cards but invalid
    try {
      newDeck = new LinkedList(deck);
      newDeck.remove(deck.get(0));
      newDeck.add(deck.get(1));
      f.startGame(newDeck, false);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid deck: the deck is invalid.", e.getMessage());
      assertEquals("", f.getGameState());
      assertEquals(false, f.isGameOver());
    }
  }

  //restart game after moves
  @Test
  public void startGameMoveRestart() {
    f.move(CASCADE, 2, 6, OPEN, 1);
    String stateBefore = f.getGameState();
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1:\n" + "O2: Q♠\n" + "O3:\n" + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", stateBefore);

    assertFalse(f.isGameOver());

    f.startGame(deck, false);
    String stateAfter = f.getGameState();
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1:\n" + "O2:\n" + "O3:\n" + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", stateAfter);
  }

  //source type is null
  @Test
  public void moveNullSourceFail() {
    String stateBefore = f.getGameState();
    try {
      f.move(null, 0, 6, OPEN, 0);
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //destination type is null
  @Test
  public void moveNullDestination() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 0, 6, null, 0);
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //Test movements to open piles
  //successfully move a card to open pile
  @Test
  public void moveToOpenSuccess() {
    f.move(CASCADE, 0, 6, OPEN, 3);
    String state = f.getGameState();
    assertEquals("F1:\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1:\n"
            + "O2:\n"
            + "O3:\n"
            + "O4: 10♠\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", state);
  }

  //move different card to the same open pile. the pile can only save one card
  @Test
  public void moveToSameOpenTwiceFail() {

    f.move(CASCADE, 0, 6, OPEN, 0);
    String stateBefore = f.getGameState();

    try {
      f.move(CASCADE, 0, 5, OPEN, 0);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //move a card in an open pile to the same position
  @Test
  public void moveToOpenItselfSuccess() {
    f.move(CASCADE, 0, 6, OPEN, 0);
    String stateBefore = f.getGameState();

    f.move(OPEN, 0, 0, OPEN, 0);
    String stateAfter = f.getGameState();

    assertEquals(stateBefore, stateAfter);

  }

  //the destination open pile index does not exist
  @Test
  public void moveToOpenNonExistingPile() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 0, 6, OPEN, 6);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //the start open pile index does not exist
  @Test
  public void moveFromOpenNonExistingPile() {
    String stateBefore = f.getGameState();

    try {
      f.move(OPEN, 7, 0, CASCADE, 6);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //the index of the card to be moved isn't the top of the pile
  @Test
  public void moveFromOpenWrongIndex() {
    f.move(CASCADE, 0, 6, OPEN, 3);
    String stateBefore = f.getGameState();
    assertEquals("F1:\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1:\n"
            + "O2:\n"
            + "O3:\n"
            + "O4: 10♠\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", stateBefore);

    try {
      f.move(OPEN, 3, 1, OPEN, 0);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //try to move form an empty open pile
  @Test
  public void moveFromOpenEmpty() {
    String stateBefore = f.getGameState();
    try {
      f.move(OPEN, 1, 0, CASCADE, 4);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //Test movements to foundation piles.
  //destination foundation pile is empty, and card is A
  @Test
  public void moveToEmptyFoundationSuccess() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    String state = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2:\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", state);
  }

  //destination foundation pile is empty, and card is not A
  @Test
  public void moveToEmptyFoundationFail() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 3, 6, FOUNDATION, 2);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void moveToNonemptyFoundationSuccess() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 0, 6, OPEN, 1);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2: 10♠\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateBefore);

    f.move(CASCADE, 0, 5, FOUNDATION, 0);
    String stateAfter = f.getGameState();
    assertEquals("F1: A♠, 2♠\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1: 9♠\n" + "O2: 10♠\n" + "O3:\n" + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateAfter);
  }

  //a movement whose card is right value, but the suit does not fit
  @Test
  public void moveToNonemptyFoundationDifferentSuitFail() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 3, 6, OPEN, 1);
    f.move(CASCADE, 3, 5, OPEN, 2);
    f.move(CASCADE, 3, 4, OPEN, 3);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2: K♠\n"
            + "O3: 5♠\n"
            + "O4: 10♥\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateBefore);
    try {
      f.move(CASCADE, 3, 3, FOUNDATION, 0);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //a movement whose card has right value , but the suit does not fit
  @Test
  public void moveToNonemptyFoundationWrongValueFail() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2:\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateBefore);
    try {
      f.move(CASCADE, 1, 6, FOUNDATION, 0);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //move a card in foundation to the same position
  @Test
  public void moveToFoundationItselfSuccess() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2:\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateBefore);

    f.move(FOUNDATION, 0, 0, FOUNDATION, 0);
    String stateAfter = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2:\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateAfter);
  }

  @Test
  public void moveFromFoundationNonExistingPile() {
    String stateBefore = f.getGameState();
    try {
      f.move(FOUNDATION, 9, 0, CASCADE, 0);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //a foundation pile does not allow multi-card movements
  @Test
  public void moveFromFoundationWrongCardIndex() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 0, 6, OPEN, 1);
    f.move(CASCADE, 0, 5, FOUNDATION, 0);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠, 2♠\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1: 9♠\n" + "O2: 10♠\n" + "O3:\n" + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥", stateBefore);
    try {
      f.move(FOUNDATION, 0, 0, OPEN, 2);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void moveFromFoundationEmptyPile() {
    String stateBefore = f.getGameState();
    try {
      f.move(FOUNDATION, 0, 0, OPEN, 0);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void moveToFoundationNonExistingPile() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    String stateBefore = f.getGameState();
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1: 9♠\n" + "O2:\n" + "O3:\n" + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠", stateBefore);

    try {
      f.move(CASCADE, 7, 4, FOUNDATION, 6);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }


  //Test movements to cascade piles.
  //a movement whose card has right value, but its color does not fit
  @Test
  public void moveToCascadeSameColorFail() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 0, 6, CASCADE, 1);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //a movement whose card has right color, but its value does not fit
  @Test
  public void moveToCascadeWrongValueFail() {
    f.move(CASCADE, 6, 5, OPEN, 3);
    String stateBefore = f.getGameState();
    assertEquals("F1:\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1:\n"
            + "O2:\n"
            + "O3:\n"
            + "O4: 8♠\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", stateBefore);

    try {
      f.move(CASCADE, 6, 4, CASCADE, 3);
      fail();
    } catch (IllegalArgumentException e) {
      String sTateAfter = f.getGameState();
      assertEquals(stateBefore, sTateAfter);
    }
  }

  @Test
  public void moveToCascadeSuccess() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 7, 3, CASCADE, 5);
    String state = f.getGameState();
    assertEquals("F1: A♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2:\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠, 6♥\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦", state);
  }

  // move a card in cascade pile to the same position
  @Test
  public void moveToCascadeItselfFail() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 1, 6, CASCADE, 1);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: the card to bee added should have different color "
              + "with top card and its value must one less than top card.", e.getMessage());
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void moveToCascadeItselfSuccess() {
    f = FreecellModel.getBuilder().cascades(52).build();
    List d = f.getDeck();
    f.startGame(d, false);

    f.move(CASCADE, 28, 0, CASCADE, 3);
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 3, 1, CASCADE, 3);
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    } catch (IllegalArgumentException e) {
      fail();
    }
  }

  //move to an empty cascade pile
  @Test
  public void moveToCascadeEmpty() {
    //create a game with 52 cascade piles, each has only one card
    f = FreecellModel.getBuilder().cascades(52).build();
    List d = f.getDeck();
    f.startGame(d, false);

    f.move(CASCADE, 47, 0, OPEN, 0);
    String stateBefore = f.getGameState();
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1: 9♠\n" + "O2:\n" + "O3:\n" + "O4:\n"
            + "C1: A♣\n" + "C2: 2♣\n" + "C3: 3♣\n" + "C4: 4♣\n"
            + "C5: 5♣\n" + "C6: 6♣\n" + "C7: 7♣\n" + "C8: 8♣\n"
            + "C9: 9♣\n" + "C10: 10♣\n" + "C11: J♣\n" + "C12: Q♣\n"
            + "C13: K♣\n" + "C14: A♦\n" + "C15: 2♦\n" + "C16: 3♦\n"
            + "C17: 4♦\n" + "C18: 5♦\n" + "C19: 6♦\n" + "C20: 7♦\n"
            + "C21: 8♦\n" + "C22: 9♦\n" + "C23: 10♦\n" + "C24: J♦\n"
            + "C25: Q♦\n" + "C26: K♦\n" + "C27: A♥\n" + "C28: 2♥\n"
            + "C29: 3♥\n" + "C30: 4♥\n" + "C31: 5♥\n" + "C32: 6♥\n"
            + "C33: 7♥\n" + "C34: 8♥\n" + "C35: 9♥\n" + "C36: 10♥\n"
            + "C37: J♥\n" + "C38: Q♥\n" + "C39: K♥\n" + "C40: A♠\n"
            + "C41: 2♠\n" + "C42: 3♠\n" + "C43: 4♠\n" + "C44: 5♠\n"
            + "C45: 6♠\n" + "C46: 7♠\n" + "C47: 8♠\n" + "C48:\n"
            + "C49: 10♠\n" + "C50: J♠\n" + "C51: Q♠\n" + "C52: K♠", stateBefore);

    f.move(OPEN, 0, 0, CASCADE, 47);
    String stateAfter = f.getGameState();
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1:\n" + "O2:\n" + "O3:\n" + "O4:\n"
            + "C1: A♣\n" + "C2: 2♣\n" + "C3: 3♣\n" + "C4: 4♣\n"
            + "C5: 5♣\n" + "C6: 6♣\n" + "C7: 7♣\n" + "C8: 8♣\n"
            + "C9: 9♣\n" + "C10: 10♣\n" + "C11: J♣\n" + "C12: Q♣\n"
            + "C13: K♣\n" + "C14: A♦\n" + "C15: 2♦\n" + "C16: 3♦\n"
            + "C17: 4♦\n" + "C18: 5♦\n" + "C19: 6♦\n" + "C20: 7♦\n"
            + "C21: 8♦\n" + "C22: 9♦\n" + "C23: 10♦\n" + "C24: J♦\n"
            + "C25: Q♦\n" + "C26: K♦\n" + "C27: A♥\n" + "C28: 2♥\n"
            + "C29: 3♥\n" + "C30: 4♥\n" + "C31: 5♥\n" + "C32: 6♥\n"
            + "C33: 7♥\n" + "C34: 8♥\n" + "C35: 9♥\n" + "C36: 10♥\n"
            + "C37: J♥\n" + "C38: Q♥\n" + "C39: K♥\n" + "C40: A♠\n"
            + "C41: 2♠\n" + "C42: 3♠\n" + "C43: 4♠\n" + "C44: 5♠\n"
            + "C45: 6♠\n" + "C46: 7♠\n" + "C47: 8♠\n" + "C48: 9♠\n"
            + "C49: 10♠\n" + "C50: J♠\n" + "C51: Q♠\n" + "C52: K♠", stateAfter);
  }

  @Test
  public void moveToCascadeNonExistingPile() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 4, 5, CASCADE, 10);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void moveFromCascadeNonExistingPile() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 11, 0, OPEN, 3);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void multiCardMoveFromCascadeToDifferentCascade() {
    f = FreecellModel.getBuilder().cascades(52).build();
    List d = f.getDeck();
    f.startGame(d, false);

    f.move(CASCADE, 28, 0, CASCADE, 3);

    f.move(CASCADE, 1, 0, CASCADE, 3);
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 3, 0, CASCADE, 17);
      fail();
    } catch (IllegalArgumentException e) {
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void isGameOver() {
    f = FreecellModel.getBuilder().build();

    assertFalse(f.isGameOver()); // is game over before starting a game

    f = FreecellModel.getBuilder().cascades(52).build();
    f.startGame(deck, false);
    assertFalse(f.isGameOver());

    for (int i = 0; i < 4; i++) {
      for (int j = i * 13; j < (i + 1) * 13; j++) {
        f.move(CASCADE, j, 0, FOUNDATION, i);
      }
    }
    assertEquals("F1: A♣, 2♣, 3♣, 4♣, 5♣, 6♣, 7♣, 8♣, 9♣, 10♣, J♣, Q♣, K♣\n"
            + "F2: A♦, 2♦, 3♦, 4♦, 5♦, 6♦, 7♦, 8♦, 9♦, 10♦, J♦, Q♦, K♦\n"
            + "F3: A♥, 2♥, 3♥, 4♥, 5♥, 6♥, 7♥, 8♥, 9♥, 10♥, J♥, Q♥, K♥\n"
            + "F4: A♠, 2♠, 3♠, 4♠, 5♠, 6♠, 7♠, 8♠, 9♠, 10♠, J♠, Q♠, K♠\n"
            + "O1:\nO2:\nO3:\nO4:\n"
            + "C1:\nC2:\nC3:\nC4:\nC5:\nC6:\nC7:\nC8:\nC9:\nC10:\n"
            + "C11:\nC12:\nC13:\nC14:\nC15:\nC16:\n"
            + "C17:\nC18:\nC19:\nC20:\nC21:\nC22:\n"
            + "C23:\nC24:\nC25:\nC26:\nC27:\nC28:\nC29:\nC30:\nC31:\n"
            + "C32:\nC33:\nC34:\nC35:\nC36:\nC37:\nC38:\nC39:\nC40:\n"
            + "C41:\nC42:\nC43:\nC44:\nC45:\nC46:\n"
            + "C47:\nC48:\nC49:\nC50:\nC51:\nC52:", f.getGameState());
    assertTrue(f.isGameOver());
  }

  @Test
  public void moveBeforeStart() {
    f = FreecellModel.getBuilder().build();
    String stateBefore = f.getGameState();
    assertEquals("", stateBefore);
    try {
      f.move(CASCADE, 0, 0, OPEN, 0);
      fail();
    } catch (IllegalStateException e) {
      assertEquals(stateBefore, f.getGameState());
      assertFalse(f.isGameOver());
    }
  }

  @Test
  public void gameOverMove() {
    f = FreecellModel.getBuilder().cascades(52).build();
    f.startGame(deck, false);
    for (int i = 0; i < 4; i++) {
      for (int j = i * 13; j < (i + 1) * 13; j++) {
        f.move(CASCADE, j, 0, FOUNDATION, i);
      }
    }
    assertEquals("F1: A♣, 2♣, 3♣, 4♣, 5♣, 6♣, 7♣, 8♣, 9♣, 10♣, J♣, Q♣, K♣\n"
            + "F2: A♦, 2♦, 3♦, 4♦, 5♦, 6♦, 7♦, 8♦, 9♦, 10♦, J♦, Q♦, K♦\n"
            + "F3: A♥, 2♥, 3♥, 4♥, 5♥, 6♥, 7♥, 8♥, 9♥, 10♥, J♥, Q♥, K♥\n"
            + "F4: A♠, 2♠, 3♠, 4♠, 5♠, 6♠, 7♠, 8♠, 9♠, 10♠, J♠, Q♠, K♠\n"
            + "O1:\nO2:\nO3:\nO4:\n"
            + "C1:\nC2:\nC3:\nC4:\nC5:\nC6:\nC7:\nC8:\nC9:\nC10:\n"
            + "C11:\nC12:\nC13:\nC14:\nC15:\nC16:\n"
            + "C17:\nC18:\nC19:\nC20:\nC21:\nC22:\n"
            + "C23:\nC24:\nC25:\nC26:\nC27:\nC28:\nC29:\nC30:\nC31:\n"
            + "C32:\nC33:\nC34:\nC35:\nC36:\nC37:\nC38:\nC39:\nC40:\n"
            + "C41:\nC42:\nC43:\nC44:\nC45:\nC46:\n"
            + "C47:\nC48:\nC49:\nC50:\nC51:\nC52:", f.getGameState());
    assertTrue(f.isGameOver());

    try {
      f.move(FOUNDATION, 0, 12, OPEN, 0);
      fail();
    } catch (IllegalStateException e) {
      assertEquals("F1: A♣, 2♣, 3♣, 4♣, 5♣, 6♣, 7♣, 8♣, 9♣, 10♣, J♣, Q♣, K♣\n"
              + "F2: A♦, 2♦, 3♦, 4♦, 5♦, 6♦, 7♦, 8♦, 9♦, 10♦, J♦, Q♦, K♦\n"
              + "F3: A♥, 2♥, 3♥, 4♥, 5♥, 6♥, 7♥, 8♥, 9♥, 10♥, J♥, Q♥, K♥\n"
              + "F4: A♠, 2♠, 3♠, 4♠, 5♠, 6♠, 7♠, 8♠, 9♠, 10♠, J♠, Q♠, K♠\n"
              + "O1:\nO2:\nO3:\nO4:\n"
              + "C1:\nC2:\nC3:\nC4:\nC5:\nC6:\nC7:\nC8:\nC9:\nC10:\n"
              + "C11:\nC12:\nC13:\nC14:\nC15:\nC16:\n"
              + "C17:\nC18:\nC19:\nC20:\nC21:\nC22:\n"
              + "C23:\nC24:\nC25:\nC26:\nC27:\nC28:\n"
              + "C29:\nC30:\nC31:\nC32:\nC33:\nC34:\n"
              + "C35:\nC36:\nC37:\nC38:\nC39:\nC40:\n"
              + "C41:\nC42:\nC43:\nC44:\nC45:\nC46:\n"
              + "C47:\nC48:\nC49:\nC50:\nC51:\nC52:", f.getGameState());
      assertTrue(f.isGameOver());
    }
  }

  @Test
  public void getGameState() {
    f = FreecellModel.getBuilder().build();
    assertEquals("", f.getGameState());// get game state before starting a game
    f.startGame(deck, false);
    assertEquals("F1:\nF2:\nF3:\nF4:\nO1:\nO2:\nO3:\nO4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", f.getGameState());
  }
}
