import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import freecell.controller.FreecellController;
import freecell.controller.IFreecellController;
import freecell.model.FreecellMultiMoveModel;
import freecell.model.FreecellOperations;
import freecell.model.MockModel1;
import freecell.model.MockModel2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FreecellControllerTest {
  private Reader in;
  private StringBuffer out;
  private IFreecellController<?> controller;
  private List deck;
  private StringBuilder log;
  private FreecellOperations mockModel;

  /**
   * Initialize some fields of tests.
   */
  @Before
  public void setUp() {
    in = new StringReader("");
    out = new StringBuffer();
    deck = FreecellMultiMoveModel.getBuilder().build().getDeck();
    log = new StringBuilder();
  }

  @Test
  public void testStartGame() {
    mockModel = new MockModel2(log, "kk999", false);
    controller = new FreecellController(in, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("deck:[A♣, 2♣, 3♣, 4♣, 5♣, 6♣, 7♣, 8♣, 9♣, 10♣, J♣, Q♣, K♣, A♦, 2♦, "
            + "3♦, 4♦, 5♦, 6♦, 7♦, 8♦, 9♦, 10♦, J♦, Q♦, K♦, A♥, 2♥, 3♥, 4♥, 5♥, 6♥, 7♥, 8♥, 9♥, "
            + "10♥, J♥, Q♥, K♥, A♠, 2♠, 3♠, 4♠, 5♠, 6♠, 7♠, 8♠, 9♠, 10♠, J♠, Q♠, K♠]\n"
            + "shuffle:false", log.toString());
  }

  @Test
  public void testConstructException() {
    try {
      controller = new FreecellController(null, out);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Null readable or null appendable.", e.getMessage());
    }
    try {
      controller = new FreecellController(in, null);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Null readable or null appendable.", e.getMessage());
    }
  }

  @Test
  public void testPlayException() {
    mockModel = new MockModel1(log, "abc123");
    controller = new FreecellController(in, out);
    try {
      controller.playGame(null, mockModel, false);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Null deck or null model.", e.getMessage());
    }
    try {
      controller.playGame(deck, null, false);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Null deck or null model.", e.getMessage());
    }
  }

  @Test
  public void testInputSourcePile() {
    mockModel = new MockModel1(log, "abc123");
    Reader in1 = new StringReader("x 5 xC1 xF1 xO1 C1x F1x O1x Cx1 Fx1 Ox1 xC1x xF1x xO1x"
            + " Cx Fx Ox x1 C010 F010 O010"); // invalid input
    Reader in2 = new StringReader("x 5 xC1 xF1 xO1 C1x F1x O1x Cx1 Fx1 Ox1 xC1x xF1x xO1x"
            + " Cx Fx Ox x1 C010 F010 O010 C1 5 C2"); // valid input after invaid input
    Reader in3 = new StringReader("C10000 5 C2"); // long pile number
    Reader in4 = new StringReader("O1 5 C2"); // open pile
    Reader in5 = new StringReader("F1 5 C2"); // foundation pile

    controller = new FreecellController(in1, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("", log.toString());
    controller = new FreecellController(in2, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n",
            log.toString());
    controller = new FreecellController(in3, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 10000 to CASCADE pile 2.\n",
            log.toString());
    controller = new FreecellController(in4, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 10000 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from OPEN pile 1 to CASCADE pile 2.\n",
            log.toString());
    controller = new FreecellController(in5, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 10000 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from OPEN pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from FOUNDATION pile 1 to CASCADE pile 2.\n",
            log.toString());
  }

  @Test
  public void testInputCardIndex() {
    mockModel = new MockModel1(log, "abc123");
    Reader in1 = new StringReader("C1 x C 01 x1 1x x1x -1 0"); // invalid input
    Reader in2 = new StringReader("C1 x C 01 x1 1x x1x -1 0 5 C2");// valid input after invaid
    Reader in3 = new StringReader("C1 10000 C2"); // long index

    controller = new FreecellController(in1, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("", log.toString());
    controller = new FreecellController(in2, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n",
            log.toString());
    controller = new FreecellController(in3, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 10000 from CASCADE pile 1 to CASCADE pile 2.\n",
            log.toString());
  }

  @Test
  public void testInputDestinationPile() {
    mockModel = new MockModel1(log, "abc123");
    Reader in1 = new StringReader("C1 5 xC2 xF2 xO2 C2x F2x O2x Cx2 Fx2 Ox2 xC2x xF2x xO2x"
            + " Cx Fx Ox x2 C020 F020 O020"); // invalid input
    Reader in2 = new StringReader("C1 5 xC2 xF2 xO2 C2x F2x O2x Cx2 Fx2 Ox2 xC2x xF2x xO2x Cx "
            + "Fx Ox x2 C020 F020 O020 C2"); // valid input after invaid input
    Reader in3 = new StringReader("C1 5 C20000"); // long pile number
    Reader in4 = new StringReader("C1 5 O2"); // open pile
    Reader in5 = new StringReader("C1 5 F2"); // foundation pile

    controller = new FreecellController(in1, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("", log.toString());
    controller = new FreecellController(in2, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n",
            log.toString());
    controller = new FreecellController(in3, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 20000.\n",
            log.toString());
    controller = new FreecellController(in4, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 20000.\n"
                    + "Move card starting at index 5 from CASCADE pile 1 to OPEN pile 2.\n",
            log.toString());
    controller = new FreecellController(in5, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 1 to CASCADE pile 20000.\n"
                    + "Move card starting at index 5 from CASCADE pile 1 to OPEN pile 2.\n"
                    + "Move card starting at index 5 from CASCADE pile 1 to FOUNDATION pile 2.\n",
            log.toString());
  }

  @Test
  public void testInvalidInputMessgage() {
    mockModel = new MockModel1(log, "abc123");
    Reader in1 = new StringReader("x ");
    Reader in2 = new StringReader("C1 x ");
    Reader in3 = new StringReader("C1 5 x ");

    out = new StringBuffer();
    controller = new FreecellController(in1, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("", log.toString());
    assertEquals("abc123\nInvalid pile: Please input pile again.\n", out.toString());

    out = new StringBuffer();
    controller = new FreecellController(in2, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("", log.toString());
    assertEquals("abc123\nInvalid card index: Please input card index again.\n",
            out.toString());

    out = new StringBuffer();
    controller = new FreecellController(in3, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("", log.toString());
    assertEquals("abc123\nInvalid pile: Please input pile again.\n", out.toString());
  }

  @Test
  public void testInvalidMove() {
    mockModel = new MockModel2(log, "kk999", false);
    // in this model, move is always invalid.
    Reader in = new StringReader("C1 5 C2");
    out = new StringBuffer();
    controller = new FreecellController(in, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("game state\nInvalid move. Try again.kk999\n", out.toString());
  }

  @Test
  public void testGameover() {
    mockModel = new MockModel2(log, "kk999", true);
    Reader in = new StringReader("C1 5 C2");
    out = new StringBuffer();
    controller = new FreecellController(in, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("game state\ngame state\nGame over.\n", out.toString());
  }

  @Test
  public void testTransmitGameState() {
    mockModel = new MockModel1(log, "abc123");
    Reader in0 = new StringReader("");
    Reader in1 = new StringReader("C1 1 C2");
    Reader in2 = new StringReader("C1 1 C1 C2 2 O2 C3 3 F3 O1 1 C2 O2 2 O2 O3 3 F2 "
            + "F1 1 C1 F1 2 O2 F1 3 F3");
    Reader in3 = new StringReader("C1 1 C1 C2 2 O2 C3 3 F3 O1 1 C2 O2 2 O2 O3 3 F2 "
            + "F1 1 C1 F1 2 O2 F1 3 F3 x");

    controller = new FreecellController(in0, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("abc123\n", out.toString()); // output game state after starting a game

    out = new StringBuffer();
    controller = new FreecellController(in1, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("abc123\nabc123\n",
            out.toString()); // output game state twice after one move

    out = new StringBuffer();
    controller = new FreecellController(in2, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("abc123\nabc123\nabc123\nabc123\nabc123\nabc123\nabc123\nabc123\nabc123\n"
                    + "abc123\n",
            out.toString()); // output game state ten times after nine move

    out = new StringBuffer();
    controller = new FreecellController(in3, out);
    controller.playGame(deck, mockModel, false);
    assertEquals("abc123\nabc123\nabc123\nabc123\nabc123\nabc123\nabc123\nabc123\nabc123\n"
                    + "abc123\nInvalid pile: Please input pile again.\n",
            out.toString()); // output game state ten times after nine move and one invalid move
  }

  @Test
  public void testQuit() {
    mockModel = new MockModel1(log, "abc123");
    Reader[] in = {new StringReader("q"), new StringReader("Q"),
                   new StringReader("xxqxx"), new StringReader("xxQxx"),
                   new StringReader("xqxQxx"), new StringReader("C1 q"),
                   new StringReader("C1 Q"), new StringReader("C1 xxqxx"),
                   new StringReader("C1 xxQxx"), new StringReader("C1 xqxQxx"),
                   new StringReader("C1 5 q"), new StringReader("C1 5 Q"),
                   new StringReader("C1 5 xxqxx"), new StringReader("C1 5 xxQxx"),
                   new StringReader("C1 5 xqxQxx")};
    for (Reader r : in) {
      out = new StringBuffer();
      mockModel = new MockModel1(log, "abc123");
      controller = new FreecellController(r, out);
      controller.playGame(deck, mockModel, false);
      assertEquals("abc123\nGame quit prematurely.", out.toString());
    }
  }

  @Test
  public void testIOException() {
    mockModel = new MockModel1(log, "abc123");
    controller = new FreecellController(in, new IOEAppendable());
    try {
      controller.playGame(deck, mockModel, false);
    } catch (IllegalStateException e) {
      assertEquals("The Readable or Appendable object failed.", e.getMessage());
    }
  }

  /**
   * A appendable which will throw an IOException if append is called.
   */
  class IOEAppendable implements Appendable {
    @Override
    public Appendable append(CharSequence csq) throws IOException {
      throw new IOException();
    }

    @Override
    public Appendable append(CharSequence csq, int start, int end) throws IOException {
      throw new IOException();
    }

    @Override
    public Appendable append(char c) throws IOException {
      throw new IOException();
    }
  }

}