import freecell.model.FreecellOperations;
import freecell.model.FreecellModel;


public class FreecellModelTest extends AbstractFreecellModelTest {

  public FreecellOperations init() {
    return FreecellModel.getBuilder().build();
  }
}