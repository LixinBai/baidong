import org.junit.Test;

import java.util.List;

import freecell.model.FreecellMultiMoveModel;
import freecell.model.FreecellOperations;

import static freecell.model.PileType.CASCADE;
import static freecell.model.PileType.FOUNDATION;
import static freecell.model.PileType.OPEN;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class FreecellMultiMoveModelTest extends AbstractFreecellModelTest {

  public FreecellOperations init() {
    return FreecellMultiMoveModel.getBuilder().build();
  }

  //move multiple cards from cascade to cascade, but cards do not form a build
  @Test
  public void moveNotBuildFail() {
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 0, 4, CASCADE, 6);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: cards do not form a build.", e.getMessage());
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //move multiple cards from cascade to cascade and cards form a build
  //but the first card in the list to be moved cannot be added to destination
  //or the suit of the first card in the list has same color with top card of destination
  @Test
  public void moveCardMoveFail() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 0, 6, OPEN, 1);
    f.move(CASCADE, 0, 5, FOUNDATION, 0);
    f.move(CASCADE, 1, 6, OPEN, 2);
    f.move(CASCADE, 1, 5, FOUNDATION, 0);
    f.move(CASCADE, 7, 3, CASCADE, 5);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠, 2♠, 3♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2: 10♠\n"
            + "O3: J♠\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠, 6♥\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦", stateBefore);

    //the first card has same color with top card of the destination
    try {
      f.move(CASCADE, 5, 5, CASCADE, 6);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: the card to bee added "
              + "should have different color with top card "
              + "and its value must one less than top card.", e.getMessage());

      assertEquals(stateBefore, f.getGameState());
    }

    //the value of the first card is not one less than the top card of the destination
    try {
      f.move(CASCADE, 5, 5, CASCADE, 7);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: the card to bee added "
              + "should have different color with top card "
              + "and its value must one less than top card.", e.getMessage());

      assertEquals(stateBefore, f.getGameState());
    }
  }

  //move multiple cards from cascade to cascade, but number of cards exceeds threshold
  @Test()
  public void multiCardMoveToCascadeExceedThresholdFail() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 0, 6, OPEN, 1);
    f.move(CASCADE, 0, 5, FOUNDATION, 0);
    f.move(CASCADE, 1, 6, OPEN, 2);
    f.move(CASCADE, 1, 5, FOUNDATION, 0);
    f.move(CASCADE, 7, 3, CASCADE, 5);
    f.move(CASCADE, 3, 6, OPEN, 3);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠, 2♠, 3♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2: 10♠\n"
            + "O3: J♠\n"
            + "O4: K♠\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠, 6♥\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦", stateBefore);
    try {
      f.move(CASCADE, 5, 5, CASCADE, 1);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: number of cards to move "
              + "exceeds threshold.", e.getMessage());
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //move multiple cards from cascade to cascade successfully
  //both to empty and non-empty
  @Override
  @Test
  public void multiCardMoveFromCascadeToDifferentCascade() {
    this.formBuild();

    //move to non-empty piles
    f.move(CASCADE, 3, 0, CASCADE, 17);
    f.move(CASCADE, 17, 0, CASCADE, 44);
    f.move(CASCADE, 44, 0, CASCADE, 32);
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1:\n" + "O2:\n" + "O3:\n" + "O4:\n"
            + "C1: A♣\n" + "C2:\n" + "C3: 3♣\n" + "C4:\n"
            + "C5: 5♣\n" + "C6: 6♣\n" + "C7: 7♣\n" + "C8: 8♣\n"
            + "C9: 9♣\n" + "C10: 10♣\n" + "C11: J♣\n" + "C12: Q♣\n"
            + "C13: K♣\n" + "C14: A♦\n" + "C15: 2♦\n" + "C16: 3♦\n"
            + "C17: 4♦\n" + "C18:\n" + "C19: 6♦\n" + "C20: 7♦\n"
            + "C21: 8♦\n" + "C22: 9♦\n" + "C23: 10♦\n" + "C24: J♦\n"
            + "C25: Q♦\n" + "C26: K♦\n" + "C27: A♥\n" + "C28: 2♥\n"
            + "C29:\n" + "C30: 4♥\n" + "C31: 5♥\n" + "C32: 6♥\n"
            + "C33: 7♥, 6♠, 5♦, 4♣, 3♥, 2♣\n" + "C34: 8♥\n" + "C35: 9♥\n" + "C36: 10♥\n"
            + "C37: J♥\n" + "C38: Q♥\n" + "C39: K♥\n" + "C40: A♠\n"
            + "C41: 2♠\n" + "C42: 3♠\n" + "C43: 4♠\n" + "C44: 5♠\n"
            + "C45:\n" + "C46: 7♠\n"
            + "C47: 8♠\n"
            + "C48: 9♠\n" + "C49: 10♠\n" + "C50: J♠\n" + "C51: Q♠\n" + "C52: K♠", f.getGameState());

    //move to empty pile
    f.move(CASCADE, 32, 0, CASCADE, 44);
    assertEquals("F1:\n" + "F2:\n" + "F3:\n" + "F4:\n"
            + "O1:\n" + "O2:\n" + "O3:\n" + "O4:\n"
            + "C1: A♣\n" + "C2:\n" + "C3: 3♣\n" + "C4:\n"
            + "C5: 5♣\n" + "C6: 6♣\n" + "C7: 7♣\n" + "C8: 8♣\n"
            + "C9: 9♣\n" + "C10: 10♣\n" + "C11: J♣\n" + "C12: Q♣\n"
            + "C13: K♣\n" + "C14: A♦\n" + "C15: 2♦\n" + "C16: 3♦\n"
            + "C17: 4♦\n" + "C18:\n" + "C19: 6♦\n" + "C20: 7♦\n"
            + "C21: 8♦\n" + "C22: 9♦\n" + "C23: 10♦\n" + "C24: J♦\n"
            + "C25: Q♦\n" + "C26: K♦\n" + "C27: A♥\n" + "C28: 2♥\n"
            + "C29:\n" + "C30: 4♥\n" + "C31: 5♥\n" + "C32: 6♥\n"
            + "C33:\n" + "C34: 8♥\n" + "C35: 9♥\n" + "C36: 10♥\n"
            + "C37: J♥\n" + "C38: Q♥\n" + "C39: K♥\n" + "C40: A♠\n"
            + "C41: 2♠\n" + "C42: 3♠\n" + "C43: 4♠\n" + "C44: 5♠\n"
            + "C45: 7♥, 6♠, 5♦, 4♣, 3♥, 2♣\n" + "C46: 7♠\n"
            + "C47: 8♠\n"
            + "C48: 9♠\n" + "C49: 10♠\n" + "C50: J♠\n" + "C51: Q♠\n" + "C52: K♠", f.getGameState());
  }

  //move multiple cards from cascade to the same pile
  @Test
  public void multiCardMoveToCascadeSamePileNotBuild() {
    //the list of cards to move does not form a build
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 1, 5, CASCADE, 1);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: cards do not form a build.", e.getMessage());

      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void multiCardMoveToCascadeSamePileNotBuildWithDest() {
    f.move(CASCADE, 7, 5, OPEN, 0);
    f.move(CASCADE, 7, 4, FOUNDATION, 0);
    f.move(CASCADE, 0, 6, OPEN, 1);
    f.move(CASCADE, 0, 5, FOUNDATION, 0);
    f.move(CASCADE, 1, 6, OPEN, 2);
    f.move(CASCADE, 1, 5, FOUNDATION, 0);
    f.move(CASCADE, 7, 3, CASCADE, 5);
    String stateBefore = f.getGameState();
    assertEquals("F1: A♠, 2♠, 3♠\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1: 9♠\n"
            + "O2: 10♠\n"
            + "O3: J♠\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠, 6♥\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦", stateBefore);
    try {
      f.move(CASCADE, 5, 5, CASCADE, 5);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: the card to bee added should have different color "
              + "with top card and its value must one less than top card.", e.getMessage());
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  @Test
  public void multiCardMoveToCascadeSamePileSuccess() {
    this.formBuild();
    f.move(CASCADE, 13, 0, CASCADE, 3);
    String stateBefore = f.getGameState();
    try {
      f.move(CASCADE, 3, 2, CASCADE, 3);
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    } catch (IllegalArgumentException e) {
      fail();
    }
  }

  //try to move multiple cards to an empty foundation pile
  @Test
  public void multiCardMoveToEmptyFoundationFail() {
    this.formBuild();
    f.move(CASCADE, 13, 0, CASCADE, 3);
    String stateBefore = f.getGameState();

    try {
      f.move(CASCADE, 3, 0, FOUNDATION, 0);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: you can only move one card "
              + "to your destination pile type at a time.", e.getMessage());
      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //try to move multiple cards to a non-empty foundation pile
  @Test
  public void multiCardMoveToNonEmptyFoundationFail() {
    this.formBuild();
    f.move(CASCADE, 0, 0, FOUNDATION, 1);
    String stateBefore = f.getGameState();

    try {
      f.move(CASCADE, 3, 0, FOUNDATION, 1);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: you can only move one card "
              + "to your destination pile type at a time.", e.getMessage());

      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //try to move multiple cards to an empty open pile
  @Test
  public void multiCardMoveToEmptyOpenFail() {
    this.formBuild();
    String stateBefore = f.getGameState();

    try {
      f.move(CASCADE, 3, 0, OPEN, 2);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: you can only move one card "
              + "to your destination pile type at a time.", e.getMessage());

      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  //try to move multiple cards to a non-empty open pile
  @Test
  public void multiCardMoveToNonEmptyOpenFail() {
    this.formBuild();
    f.move(CASCADE, 13, 0, CASCADE, 3);
    f.move(CASCADE, 0, 0, OPEN, 3);
    String stateBefore = f.getGameState();

    try {
      f.move(CASCADE, 3, 0, OPEN, 3);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Invalid move: you can only move one card "
              + "to your destination pile type at a time.", e.getMessage());

      String stateAfter = f.getGameState();
      assertEquals(stateBefore, stateAfter);
    }
  }

  private void formBuild() {
    f = FreecellMultiMoveModel.getBuilder().cascades(52).build();
    List d = f.getDeck();
    f.startGame(d, false);

    f.move(CASCADE, 28, 0, CASCADE, 3);
    f.move(CASCADE, 1, 0, CASCADE, 3);
  }
}