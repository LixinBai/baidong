package freecell.model;

public class Card {
  private final Suit suit;
  private final Value value;

  public Card(Suit suit, Value value) {
    this.suit = suit;
    this.value = value;
  }

  public Suit getSuit() {
    return suit;
  }

  public Value getValue() {
    return value;
  }

  /*public void setSuit(Suit suit) {
    this.suit = suit;
  }

  public void setValue(Value value) {
    this.value = value;
  }*/

  @Override
  public String toString() {
    return value.toString() + suit.toString();
  }

  public boolean differentColor(Card another) {
    return false;
  }

}
