package freecell.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class FreecellModel implements FreecellOperations<Card> {
  private Pile[] open;
  private Pile[] cascade;
  private Pile[] foundation;
  private boolean gameStart;

  private FreecellModel(int cascades, int opens) {
    open = new OpenPile[opens];
    cascade = new CascadePile[cascades];
    foundation = new FoundationPile[4];
    reset();
  }

  @Override
  public List<Card> getDeck() {
    List<Card> deck = new LinkedList<>();
    Suit[] s = {Suit.Club, Suit.Diamond, Suit.Heart, Suit.Spade};
    Value[] v = {Value.A, Value.Two, Value.Three, Value.Four, Value.Five, Value.Six,
            Value.Seven, Value.Eight, Value.Nine, Value.Ten, Value.J, Value.Q, Value.K};
    for (Suit suit : s) {
      for (Value value : v) {
        deck.add(new Card(suit, value));
      }
    }
    return deck;
  }

  private void reset() {
    gameStart = false;
    for (int i = 0; i < open.length; i++) {
      open[i] = new OpenPile();
    }
    for (int i = 0; i < cascade.length; i++) {
      cascade[i] = new CascadePile();
    }
    for (int i = 0; i < foundation.length; i++) {
      foundation[i] = new FoundationPile();
    }
  }

  @Override
  public void startGame(List<Card> deck, boolean shuffle) throws IllegalArgumentException {
    reset();
    if (!validDeck(deck)) {
      throw new IllegalArgumentException("The deck is invalid");
    }
    gameStart = true;
    List d = new LinkedList(deck);
    if (shuffle) {
      Collections.shuffle(d);
    }
    distribute(d);
  }

  private boolean validDeck(List<Card> deck) {
    HashSet<Card> deckHash = new HashSet<>(deck);
    return deckHash.size() == 52;
  }

  private void distribute(List<Card> deck) {
    while (deck.size() > 0) {
      for (int i = 0; i < cascade.length; i++) {
        if (deck.size() > 0) {
          CascadePile c = (CascadePile) cascade[i];
          cascade[i] = c.distribute(deck.get(0));
          deck.remove(0);
        } else {
          break;
        }
      }
    }
  }

  @Override
  public void move(PileType source, int pileNumber, int cardIndex,
                   PileType destination, int destPileNumber)
          throws IllegalArgumentException, IllegalStateException {
    if (isGameOver()) {
      throw new IllegalStateException("Invalid move: Game is over.");
    }
    if (!gameStart) {
      throw new IllegalStateException("Invalid move: Game has not started yet.");
    }
    Pile from = getPile(source, pileNumber);
    Pile to = getPile(destination, destPileNumber);
    if (from != to) {
      to.addCard(from.findCard(cardIndex));
      from.removeCard(cardIndex);
    }
  }

  private Pile getPile(PileType type, int number) throws IllegalStateException {
    switch (type) {
      case OPEN:
        if (number >= open.length) {
          throw new IllegalStateException("Open pile " + number + "does not exist.");
        }
        return open[number];
      case CASCADE:
        if (number >= cascade.length) {
          throw new IllegalStateException("Cascade pile " + number + "does not exist.");
        }
        return cascade[number];
      case FOUNDATION:
        if (number >= foundation.length) {
          throw new IllegalStateException("Foundation pile " + number + "does not exist.");
        }
        return foundation[number];
      default:
        return null;
    }
  }

  @Override
  public boolean isGameOver() {
    if (!gameStart) {
      return false;
    }
    for (Pile p :
            foundation) {
      if (p.getSize() != 13) {
        return false;
      }
    }
    return true;
    /**boolean result = true;
     for (Pile p : foundation) {
     Card c = p.findCard(p.getSize() - 1);
     result = result && (c.getValue() == Value.K);
     }
     return result;
     */
  }

  @Override
  public String getGameState() {
    if (!gameStart) {
      return "";
    }
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < foundation.length; i++) {
      s.append("F");
      s.append(i + 1);
      s.append(":");
      s.append(foundation[i].toString());
      s.append("\n");
    }
    for (int i = 0; i < open.length; i++) {
      s.append("O");
      s.append(i + 1);
      s.append(":");
      s.append(open[i].toString());
      s.append("\n");
    }
    for (int i = 0; i < cascade.length; i++) {
      s.append("C");
      s.append(i + 1);
      s.append(":");
      s.append(cascade[i].toString());
      if (i < cascade.length - 1) {
        s.append("\n");
      }
    }
    return s.toString();
  }

  public static FreecellBuilder getBuilder() {
    return new FreecellBuilder();
  }

  public static class FreecellBuilder implements FreecellOperationsBuilder {
    private int cascades;
    private int opens;

    public FreecellBuilder() {
      //assign default values to all the fields as above
      cascades = 8;
      opens = 4;
    }

    @Override
    public FreecellOperationsBuilder cascades(int c) throws IllegalArgumentException {
      if (c < 4) {
        throw new IllegalArgumentException("xxxx");
      }
      this.cascades = c;
      return this;
    }

    @Override
    public FreecellOperationsBuilder opens(int o) throws IllegalArgumentException {
      if (o < 1) {
        throw new IllegalArgumentException("xxxx");
      }
      this.opens = o;
      return this;
    }

    @Override
    public FreecellOperations build() {

      return new FreecellModel(cascades, opens);
    }
  }

}
