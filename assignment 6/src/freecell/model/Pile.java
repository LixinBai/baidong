package freecell.model;

public interface Pile {

  void addCard(Card c) throws IllegalStateException;

  void removeCard(int index);

  Card findCard(int index) throws IllegalStateException;

  int getSize();

}
