import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static freecell.model.PileType.CASCADE;
import static freecell.model.PileType.OPEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import freecell.model.CascadePile;
import freecell.model.FreecellOperations;
import freecell.model.FreecellModel;
import freecell.model.Card;
import freecell.model.OpenPile;
import freecell.model.PileType;
import freecell.model.Suit;
import freecell.model.Value;


public class FreecellModelTest {
  private FreecellOperations f;

  @Before
  public void setUp() {
    f = FreecellModel.getBuilder().build();
  }

  private void nonshuffledGame() {
    List<Card> d = f.getDeck();
    f.startGame(d, false);
    System.out.println(f.getGameState());
    f.startGame(d, false);
    System.out.println(f.getGameState());
  }

  @Test
  public void getDeck() {
  }

  @Test
  public void startGameNullDeck() {
    //getState
    //is game over
  }

  @Test
  public void startGameInvalidDeck() {
    List<Card> d1 = f.getDeck();
    d1.remove(0);
    try {
      f.startGame(d1, false);
      fail();
    } catch (IllegalArgumentException e) {
      e.getMessage();

    }

    List<Card> d2 = f.getDeck();
    d2.add(new Card(Suit.Heart, Value.A));
    try {
      f.startGame(d2, false);
      fail();
    } catch (IllegalArgumentException e) {
      e.getMessage();
    }

    //getState
    //is game over
  }

  @Test
  public void startGameShuffle() {
    //do not mutate deck
  }

  @Test
  public void startGameNotShuffle() {
    //do not mutate deck
  }

  // start game after game start

  @Test
  public void moveCascadeToOpen() {
    this.nonshuffledGame();
    f.move(CASCADE, 0, 6, OPEN, 3);
    String state = f.getGameState();
    System.out.println(state);
  }

  @Test(expected = IllegalStateException.class)
  public void moveCascadeToSameOpenTwice() {
    this.nonshuffledGame();
    f.move(CASCADE, 0, 6, OPEN, 0);
    String state = f.getGameState();
    System.out.println(state);

    f.move(CASCADE, 0, 5, OPEN, 0);
  }

  @Test(expected = IllegalStateException.class)
  public void moveCascadeToCascadeSameColor() {
    this.nonshuffledGame();
    f.move(CASCADE, 0, 6, CASCADE, 1);
    String state = f.getGameState();
    System.out.println(state);
  }

  @Test
  public void moveCascadeToCascadeDifferentColorWrongValue() {
    //
  }

  @Test
  public void moveCascadeToCascade() {
    //
  }

  @Test
  public void moveCascadeToItself() {
    this.nonshuffledGame();
    f.move(CASCADE, 1, 6, CASCADE, 1);
    String state = f.getGameState();
    assertEquals("F1:\n"
            + "F2:\n"
            + "F3:\n"
            + "F4:\n"
            + "O1:\n"
            + "O2:\n"
            + "O3:\n"
            + "O4:\n"
            + "C1: A♣, 9♣, 4♦, Q♦, 7♥, 2♠, 10♠\n"
            + "C2: 2♣, 10♣, 5♦, K♦, 8♥, 3♠, J♠\n"
            + "C3: 3♣, J♣, 6♦, A♥, 9♥, 4♠, Q♠\n"
            + "C4: 4♣, Q♣, 7♦, 2♥, 10♥, 5♠, K♠\n"
            + "C5: 5♣, K♣, 8♦, 3♥, J♥, 6♠\n"
            + "C6: 6♣, A♦, 9♦, 4♥, Q♥, 7♠\n"
            + "C7: 7♣, 2♦, 10♦, 5♥, K♥, 8♠\n"
            + "C8: 8♣, 3♦, J♦, 6♥, A♠, 9♠", state);
  }

  @Test
  public void moveOpenToItself() {
    //
  }

  @Test
  public void moveOpenToCascade() {
    //
  }

  @Test
  public void moveOpenToCascadeFail() {
    //
  }

  //move before start game/after invalid start game

  @Test
  public void isGameOver() {
    f = FreecellModel.getBuilder().cascades(52).build();
    List d = f.getDeck();
    f.startGame(d, false);
    //System.out.println(f.getGameState());
    for (int i = 0; i < 4; i++) {
      for (int j = i * 13; j < (i + 1) * 13; j++) {
        f.move(PileType.CASCADE, j, 0, PileType.FOUNDATION, i);
      }
    }
    System.out.println(f.getGameState());
    System.out.println(f.isGameOver());
    // is over
    // not over
    // before start game/after invalid start game
  }

  @Test
  public void getGameState() {

  }


}