package virtualinvestment.calendar;

import java.io.Serializable;
import java.util.Calendar;

/**
 * This is the interface of my calender.
 */
public interface IMyCalendar extends Serializable, Cloneable, Comparable<Calendar> {

  /**
   * Determines whether the calendar represents a time when the stock exchange is open.
   *
   * @return null if the exchange is open, otherwise return the reason why the exchange is close.
   */
  String isClose();

  /**
   * Determines whether the calendar is before another object.
   *
   * @param o another object
   * @return true if the calendar is before another object, false otherwise
   */
  boolean before(Object o);

  /**
   * Determines whether the calendar is after another object.
   *
   * @param o another object
   * @return true if the calendar is after another object, false otherwise
   */
  boolean after(Object o);

  /**
   * Adds the specified amount of time to the given calendar field, based on the calendar's rules.
   *
   * @param field  the filed to be added
   * @param amount the amount to add
   */
  void add(int field, int amount);
}
