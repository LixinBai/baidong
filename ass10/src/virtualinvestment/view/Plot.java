package virtualinvestment.view;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * This is a class which provides operations to plot a portfolio.
 */
class Plot {
  private Map<String, Double> map;
  private String pName;
  private JFreeChart chart;

  /**
   * Initialize a plot class according data of a portfolio.
   *
   * @param map   the time-value data set
   * @param pName name of the portfolio to plot
   */
  Plot(Map<String, Double> map, String pName) {
    this.map = map;
    this.pName = pName;
  }

  ChartFrame getChartFrame() throws IllegalArgumentException {
    getChart();
    ChartFrame mChartFrame = new ChartFrame("Port", chart);
    mChartFrame.pack();
    return mChartFrame;
  }

  void saveChartAsFile() throws IOException {
    getChart();
    File file = new File(pName + ".png");
    ChartUtilities.saveChartAsPNG(file, chart, 400, 300);//把报表保存为文件
  }

  private void getChart() {
    CategoryDataset mDataset = getDataset();
    chart = ChartFactory.createLineChart("Totoal Value of " + pName,
            "date", "amount of dollar", mDataset,
            PlotOrientation.VERTICAL, true, true, false);
    CategoryPlot mPlot = (CategoryPlot) chart.getPlot();
    mPlot.setBackgroundPaint(Color.WHITE);
    mPlot.setRangeGridlinePaint(Color.BLUE);
    mPlot.setOutlinePaint(Color.RED);

    // Y Axis
    NumberAxis vn = (NumberAxis) mPlot.getRangeAxis();
    DecimalFormat df = new DecimalFormat("#0.00");
    vn.setNumberFormatOverride(df);
    vn.setUpperMargin(0.1);
    vn.setLowerMargin(0.1);
    vn.setAutoRangeMinimumSize(0.01);
    LineAndShapeRenderer lasp = (LineAndShapeRenderer) mPlot.getRenderer();
    lasp.setBaseShapesVisible(true);
    lasp.setDrawOutlines(true);
    lasp.setUseFillPaint(true);
    lasp.setBaseFillPaint(Color.BLUE);

    // X Axis
    CategoryAxis domainAxis = mPlot.getDomainAxis();
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
    domainAxis.setCategoryMargin(0.5);
    domainAxis.setLowerMargin(0.01);
    domainAxis.setUpperMargin(0.06);
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
  }

  private CategoryDataset getDataset() {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (Map.Entry<String, Double> entry : map.entrySet()) {
      String key = entry.getKey();
      dataset.addValue(entry.getValue(), pName, key.substring(2, key.length() - 3));
    }
    return dataset;
  }
}