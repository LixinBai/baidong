package virtualinvestment.view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BorderFactory;

/**
 * This is a strategy operation panel of the gui view.
 */
public class StrategyPanel extends JPanel {
  JButton viewAllStrategy;
  private Map<String, JButton> buttonMap;

  StrategyPanel() {
    super();

    this.setLayout(new GridLayout(2, 3, 0, 20));
    this.setBorder(BorderFactory.createTitledBorder("strategy"));
    this.setVisible(true);

    buttonMap = new LinkedHashMap<>();

    JButton createStrategy = new JButton("create");
    buttonMap.put("createStrategy", createStrategy);

    JButton saveStrategy = new JButton("save");
    buttonMap.put("saveStrategy", saveStrategy);

    JButton retrieveStrategy = new JButton("retrieve");
    buttonMap.put("retrieveStrategy", retrieveStrategy);

    JButton applyStrategy = new JButton("apply");
    buttonMap.put("applyStrategy", applyStrategy);

    JButton viewStrategy = new JButton("view");
    buttonMap.put("viewStrategy", viewStrategy);

    viewAllStrategy = new JButton("view all");
    buttonMap.put("viewAllStrategy", viewAllStrategy);


    for (Map.Entry<String, JButton> entry : buttonMap.entrySet()) {
      this.add(entry.getValue(), entry.getKey());
      entry.getValue().setActionCommand(entry.getKey());
    }
  }

  void addActionListener(ActionListener actionListener) {
    for (Map.Entry<String, JButton> entry : buttonMap.entrySet()) {
      entry.getValue().addActionListener(actionListener);
    }
  }

}
