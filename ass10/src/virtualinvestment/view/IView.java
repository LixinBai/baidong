package virtualinvestment.view;

import java.awt.event.ActionListener;
import java.util.Map;

/**
 * This interface represents all possible operations for a view of a virtual investment system.
 */
public interface IView extends ActionListener {

  /**
   * Set the prompt message.
   *
   * @param s the message to set
   */
  void setMessage(String s);

  /**
   * Add an action listener to the view.
   *
   * @param actionListener the action listener to add
   */
  void addActionListener(ActionListener actionListener);

  /**
   * Get the input.
   *
   * @return the input
   */
  String getInput();

  /**
   * Output a string which the model tell the view to.
   *
   * @param s the output string
   */
  void output(String s);

  /**
   * Clean all inputs of the view.
   */
  void cleanInput();

  /**
   * Reset to retake all inputs.
   */
  void resetInput();

  void plot(Map<String, Double> map, String pName);
}
