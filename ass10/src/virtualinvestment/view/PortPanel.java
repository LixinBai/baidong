package virtualinvestment.view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BorderFactory;

/**
 * This is a portfolio operation panel of the gui view.
 */
class PortPanel extends JPanel {

  JButton viewAllPort;
  private Map<String, JButton> buttonMap;

  PortPanel() {
    super();

    this.setLayout(new GridLayout(4, 3, 0, 20));
    this.setBorder(BorderFactory.createTitledBorder("portfolio"));
    this.setVisible(true);

    buttonMap = new LinkedHashMap<>();

    JButton createPort = new JButton("create");
    buttonMap.put("createPort", createPort);

    JButton savePort = new JButton("save");
    buttonMap.put("savePort", savePort);

    JButton retrievePort = new JButton("retrieve");
    buttonMap.put("retrievePort", retrievePort);

    JButton addStock = new JButton("add stock");
    buttonMap.put("addStock", addStock);

    JButton viewPort = new JButton("view");
    buttonMap.put("viewPort", viewPort);

    viewAllPort = new JButton("view all");
    buttonMap.put("viewAllPort", viewAllPort);

    JButton buyStock = new JButton("buy stock");
    buttonMap.put("buyStock", buyStock);

    JButton invest = new JButton("invest");
    buttonMap.put("invest", invest);

    JButton getHistory = new JButton("history");
    buttonMap.put("getHistory", getHistory);

    JButton getBasis = new JButton("cost basis");
    buttonMap.put("getBasis", getBasis);

    JButton getValue = new JButton("total value");
    buttonMap.put("getValue", getValue);

    JButton plotPort = new JButton("plot");
    buttonMap.put("plotPort", plotPort);

    for (Map.Entry<String, JButton> entry : buttonMap.entrySet()) {
      this.add(entry.getValue(), entry.getKey());
      entry.getValue().setActionCommand(entry.getKey());
    }
  }

  void addActionListener(ActionListener actionListener) {
    for (Map.Entry<String, JButton> entry : buttonMap.entrySet()) {
      entry.getValue().addActionListener(actionListener);
    }
  }

}
