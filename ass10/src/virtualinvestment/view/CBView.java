package virtualinvestment.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * This is a console based view.
 */
public class CBView implements IView {
  private final Appendable ap;
  private Scanner scan;

  /**
   * Initialize a console based view according to the readable and appendable object.
   *
   * @param rd the readable object
   * @param ap the appendable object
   */
  public CBView(Readable rd, Appendable ap) {
    this.ap = ap;
    this.scan = new Scanner(rd);
  }

  @Override
  public void setMessage(String s) {
    output(s);
  }

  @Override
  public void addActionListener(ActionListener actionListener) {
    //This functionality is not implement in the console based view.
  }

  @Override
  public String getInput() throws NoSuchElementException {
    if (!scan.hasNextLine()) {
      throw new NoSuchElementException("Miss input.\n");
    }
    return scan.nextLine();
  }

  @Override
  public void output(String s) {
    try {
      ap.append(s);
    } catch (IOException e) {
      e.getMessage();
    }
  }

  @Override
  public void cleanInput() {
    //This functionality is not implement in the console based view.
  }

  @Override
  public void resetInput() {
    //This functionality is not implement in the console based view.
  }

  @Override
  public void plot(Map<String, Double> map, String pName) {
    try {
      new Plot(map, pName).saveChartAsFile();
      output("Chart is saved to the root path.\n");
    } catch (IOException e) {
      output("Creating file failed.\n");
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    //This functionality is not implement in the console based view.
  }

}
