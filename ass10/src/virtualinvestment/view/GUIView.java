package virtualinvestment.view;


import org.jfree.chart.ChartFrame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BorderFactory;



/**
 * This is a graphical user interface view.
 */
public class GUIView extends JFrame implements IView {
  private JLabel message;
  private OperationPanel operationPanel;
  private CardPanel cardPanel;

  /**
   * Initialize a graphical user interface view.
   */
  public GUIView() {
    super();
    this.setTitle("Invest!");
    this.setLayout(new BorderLayout());
    this.setLocation(300, 150);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);


    JPanel messagePanel = new JPanel();
    messagePanel.setVisible(true);
    messagePanel.setPreferredSize(new Dimension(650, 500));

    message = new JLabel("");
    messagePanel.add(message);

    JScrollPane messageScrollPanel = new JScrollPane(messagePanel);
    messageScrollPanel.setPreferredSize(new Dimension(650, 120));
    messageScrollPanel.setBorder(BorderFactory.createTitledBorder("result"));
    messageScrollPanel.setBackground(messagePanel.getBackground());
    this.add(messageScrollPanel, BorderLayout.NORTH);

    operationPanel = new OperationPanel();
    operationPanel.setVisible(true);
    this.add(operationPanel, BorderLayout.WEST);

    cardPanel = new CardPanel();
    cardPanel.setVisible(true);
    this.add(cardPanel, BorderLayout.EAST);

    JPanel executePanel = new JPanel();
    executePanel.setVisible(true);
    this.add(executePanel, BorderLayout.SOUTH);

    JButton backButton = new JButton("back");
    backButton.setActionCommand("empty");
    executePanel.add(backButton);

    backButton.addActionListener(this);
    operationPanel.addActionListener(this);


    this.pack();
  }

  @Override
  public void setMessage(String s) {
    //This functionality is not implement in the graphical view.
  }

  @Override
  public void addActionListener(ActionListener actionListener) {
    cardPanel.addActionListener(actionListener);
    operationPanel.portPanel.viewAllPort.addActionListener(actionListener);
    operationPanel.strategyPanel.viewAllStrategy.addActionListener(actionListener);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    cardPanel.show(e.getActionCommand());
  }

  @Override
  public String getInput() throws NoSuchElementException {
    return cardPanel.getInput();
  }

  @Override
  public void output(String s) {
    message.setText("<html>" + s.replace("\n", "<br>") + "</html>");
  }

  @Override
  public void cleanInput() {
    cardPanel.cleanText();
  }

  @Override
  public void resetInput() {
    cardPanel.resetInput();
  }

  @Override
  public void plot(Map<String, Double> map, String pName) {
    ChartFrame chartFrame = new Plot(map, pName).getChartFrame();
    chartFrame.setVisible(true);
  }

}
