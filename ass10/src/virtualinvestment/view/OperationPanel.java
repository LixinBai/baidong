package virtualinvestment.view;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;


/**
 * This is a operation panel of the gui view.
 */
class OperationPanel extends JPanel {
  PortPanel portPanel;
  StrategyPanel strategyPanel;
  private JButton importStock;

  OperationPanel() {
    super();

    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.setVisible(true);

    JPanel stockPanel = new JPanel();
    stockPanel.setPreferredSize(new Dimension(350, 50));
    stockPanel.setBorder(BorderFactory.createTitledBorder("stock"));
    stockPanel.setVisible(true);
    this.add(stockPanel);

    importStock = new JButton("import");
    importStock.setActionCommand("importStock");
    importStock.setPreferredSize(new Dimension(100, 40));
    stockPanel.add(importStock);

    portPanel = new PortPanel();
    portPanel.setPreferredSize(new Dimension(350, 240));
    portPanel.setVisible(true);
    this.add(portPanel);

    strategyPanel = new StrategyPanel();
    strategyPanel.setPreferredSize(new Dimension(350, 100));
    strategyPanel.setVisible(true);
    this.add(strategyPanel);
  }

  void addActionListener(ActionListener actionListene) {
    portPanel.addActionListener(actionListene);
    strategyPanel.addActionListener(actionListene);
    importStock.addActionListener(actionListene);
  }

}
