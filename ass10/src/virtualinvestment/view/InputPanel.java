package virtualinvestment.view;


import java.awt.event.ActionListener;
import java.util.NoSuchElementException;
import java.util.Scanner;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;

/**
 * This is a input panel of the gui view.
 */
public class InputPanel extends JPanel {
  private JComboBox<String> comboBox;
  private JTextField[] texts;
  private int num;
  private JButton confirm;
  private Scanner scan;

  InputPanel(String[] str) {
    super();
    configure(str);
  }

  InputPanel(String name, String[] option, String[] str) {
    super();
    JLabel label = new JLabel(name);
    this.add(label);
    comboBox = new JComboBox<>();

    for (String s : option) {
      comboBox.addItem(s);
    }
    this.add(comboBox);

    configure(str);
  }

  private void configure(String[] str) {
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.setPreferredSize(new Dimension(330, 440));
    this.setVisible(true);
    num = str.length;
    texts = new JTextField[num];

    for (int i = 0; i < num; i++) {
      texts[i] = new JTextField();
      texts[i].setBorder(BorderFactory.createTitledBorder(str[i]));
      this.add(texts[i]);
    }

    JPanel spacePanel = new JPanel();
    int height = 440 - num * 45;
    if (comboBox != null) {
      height = height - 40;
    }
    spacePanel.setPreferredSize(new Dimension(330, height));

    this.add(spacePanel);
    spacePanel.setVisible(true);

    confirm = new JButton("confirm");
    spacePanel.add(confirm, BorderLayout.CENTER);
  }

  public String getInput() throws NoSuchElementException {
    return scan.nextLine();
  }

  void resetInput() {
    StringBuilder str = new StringBuilder();
    if (comboBox != null) {
      str.append(comboBox.getSelectedItem()).append("\n");
    }
    for (int i = 0; i < num; i++) {
      String in = texts[i].getText();
      if (in.equals("")) {
        throw new NoSuchElementException();
      }
      str.append(in).append("\n");
    }
    scan = new Scanner(str.toString());
  }

  void cleanText() {
    scan = null;
    for (JTextField t : texts) {
      t.setText("");
    }
  }

  void setActionCommand(String command) {
    confirm.setActionCommand(command);
  }

  void addActionListener(ActionListener actionListener) {
    confirm.addActionListener(actionListener);
  }

}
