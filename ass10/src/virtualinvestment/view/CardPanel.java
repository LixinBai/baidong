package virtualinvestment.view;

import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.BorderFactory;

/**
 * This is a card panel of the gui view.
 */
public class CardPanel extends JPanel {
  private CardLayout cards = new CardLayout();
  private Map<String, InputPanel> panelMap;

  private String panel;

  CardPanel() {
    super();
    this.setLayout(cards);
    this.setBorder(BorderFactory.createTitledBorder("input area"));

    panelMap = new LinkedHashMap<>();

    JPanel empty = new JPanel();
    this.add(empty, "empty");

    InputPanel importStock = new InputPanel("API", new String[]{"Alpha Vantage"},
            new String[]{"stock name"});
    panelMap.put("importStock", importStock);

    InputPanel createPort = new InputPanel(new String[]{"portfolio name", "stocks to add "
            + "(separated by \",\") or \" \" if empty"});
    panelMap.put("createPort", createPort);

    InputPanel savePort = new InputPanel(new String[]{"portfolio name"});
    panelMap.put("savePort", savePort);

    InputPanel retrievePort = new InputPanel(new String[]{"portfolio name"});
    panelMap.put("retrievePort", retrievePort);

    InputPanel addStock = new InputPanel(new String[]{"portfolio name", "stock name"});
    panelMap.put("addStock", addStock);

    InputPanel viewPort = new InputPanel(new String[]{"portfolio name"});
    panelMap.put("viewPort", viewPort);

    InputPanel getHistory = new InputPanel(new String[]{"portfolio name", "time(yyyy-mm-dd-hh)"});
    panelMap.put("getHistory", getHistory);

    InputPanel buyStock = new InputPanel(new String[]{"time to buy(yyyy-mm-dd-hh)",
                                                      "portfolio name",
                                                      "stock name",
                                                      "shares to buy",
                                                      "commission fee"});
    panelMap.put("buyStock", buyStock);

    InputPanel invest = new InputPanel(new String[]{"portfolio name", "time(yyyy-mm-dd-hh)",
                                                    "amount of dollar",
                                                    "\"equal\" or weights of stocks "
                                                            + "(separated by \",\")",
                                                    "commission fee"});
    panelMap.put("invest", invest);

    InputPanel getBasis = new InputPanel(new String[]{"portfolio name", "time(yyyy-mm-dd-hh)"});
    panelMap.put("getBasis", getBasis);

    InputPanel getValue = new InputPanel(new String[]{"portfolio name", "time(yyyy-mm-dd-hh)"});
    panelMap.put("getValue", getValue);

    InputPanel plotPort = new InputPanel("interval",
            new String[]{"day", "month", "year"},
            new String[]{"portfolio name", "start date(yyyy-mm-dd)",
                         "end date(yyyy-mm-dd)", "interval"});
    panelMap.put("plotPort", plotPort);

    InputPanel createStrategy = new InputPanel("type", new String[]{"dollar-cost averaging"},
            new String[]{"strategy name", "start date(yyyy-mm-dd): ",
                         "end date(yyyy-mm-dd or ongoing)", "interval days",
                         "amount of dollar to invest", "stocks to invest (separated by \",\")",
                         "\"equal\" or weights of stocks\n (separated by \",\")",
                         "commission fee"});
    panelMap.put("createStrategy", createStrategy);

    InputPanel saveStrategy = new InputPanel(new String[]{"strategy name"});
    panelMap.put("saveStrategy", saveStrategy);

    InputPanel retrieveStrategy = new InputPanel(new String[]{"strategy name"});
    panelMap.put("retrieveStrategy", retrieveStrategy);

    InputPanel viewStrategy = new InputPanel(new String[]{"strategy name"});
    panelMap.put("viewStrategy", viewStrategy);

    InputPanel applyStrategy = new InputPanel(new String[]{"strategy name", "portfolioName"});
    panelMap.put("applyStrategy", applyStrategy);

    for (Map.Entry<String, InputPanel> entry : panelMap.entrySet()) {
      this.add(entry.getValue(), entry.getKey());
    }

    for (Map.Entry<String, InputPanel> entry : panelMap.entrySet()) {
      entry.getValue().setActionCommand(entry.getKey());
    }
  }

  void show(String s) {
    if (panelMap.containsKey(s)) {
      cards.show(this, s);
      panel = s;
    } else {
      cards.show(this, "empty");
    }
  }

  String getInput() {
    return panelMap.get(panel).getInput();
  }

  void resetInput() {
    panelMap.get(panel).resetInput();
  }

  void cleanText() {
    panelMap.get(panel).cleanText();
  }

  void addActionListener(ActionListener actionListener) {
    for (Map.Entry<String, InputPanel> entry : panelMap.entrySet()) {
      entry.getValue().addActionListener(actionListener);
    }
  }

}
