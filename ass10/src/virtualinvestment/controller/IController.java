package virtualinvestment.controller;

import virtualinvestment.view.IView;

/**
 * This is the interface for the virtual investment controller. An implementation will work with the
 * virtual investment model to provide an virtual investment program.
 */
public interface IController {

  /**
   * Start a virtual investment program.
   */
  void start();

  /**
   * Set the view of the controller.
   *
   * @param view the view
   * @throws IllegalArgumentException if the view is null
   */
  void setView(IView view) throws IllegalArgumentException;
}
