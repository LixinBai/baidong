package virtualinvestment.controller;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This represent a graphical controller.
 */
public class GUIController extends AbstractController {

  /**
   * Initialize the graphical controller with given model.
   *
   * @param model the model to control
   * @throws IllegalArgumentException if and only if the model is null.
   */
  public GUIController(VirtualInvestment model) throws IllegalArgumentException {
    super(model);
  }

  @Override
  public void setView(IView view) throws IllegalArgumentException {
    if (view == null) {
      throw new IllegalArgumentException("View not found.\n");///
    }
    this.view = view;
  }

  @Override
  public void start() {
    model.reset();
    ButtonListener buttonListener = new ButtonListener(model, view);
    buttonListener.setButtonClickedActionMap(knownCommands);
    view.addActionListener(buttonListener);
  }
}
