package virtualinvestment.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import virtualinvestment.controller.command.AddStock;
import virtualinvestment.controller.command.ApplyStrategy;
import virtualinvestment.controller.command.BuyStock;
import virtualinvestment.controller.command.CreatePort;
import virtualinvestment.controller.command.CreateStrategy;
import virtualinvestment.controller.command.GetBasis;
import virtualinvestment.controller.command.GetHistory;
import virtualinvestment.controller.command.GetValue;
import virtualinvestment.controller.command.ImportStock;
import virtualinvestment.controller.command.Invest;
import virtualinvestment.controller.command.PlotPort;
import virtualinvestment.controller.command.RetrievePort;
import virtualinvestment.controller.command.RetrieveStrategy;
import virtualinvestment.controller.command.SavePort;
import virtualinvestment.controller.command.SaveStrategy;
import virtualinvestment.controller.command.ViewAllPort;
import virtualinvestment.controller.command.ViewAllStrategy;
import virtualinvestment.controller.command.ViewPort;
import virtualinvestment.controller.command.ViewStrategy;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This represent an abstract controller.
 */
public abstract class AbstractController implements IController {
  protected VirtualInvestment model;
  protected IView view;
  Map<String, Function<IView, Command>> knownCommands;

  /**
   * Initialize an abstract controller with the given model.
   *
   * @param model the model to control
   */
  AbstractController(VirtualInvestment model) throws IllegalArgumentException {
    if (model == null) {
      throw new IllegalArgumentException("Model not found.\n");
    }
    this.model = model;
    model.reset();
    configureCommandMap();
  }

  @Override
  public void setView(IView view) {
    if (view == null) {
      throw new IllegalArgumentException("View not found.\n");///
    }
    this.view = view;
  }

  private void configureCommandMap() {
    knownCommands = new HashMap<>();
    knownCommands.put("importStock", s -> new ImportStock(view));
    knownCommands.put("createPort", s -> new CreatePort(view));
    knownCommands.put("savePort", s -> new SavePort(view));
    knownCommands.put("retrievePort", s -> new RetrievePort(view));
    knownCommands.put("viewPort", s -> new ViewPort(view));
    knownCommands.put("viewAllPort", s -> new ViewAllPort(view));
    knownCommands.put("addStock", s -> new AddStock(view));
    knownCommands.put("buyStock", s -> new BuyStock(view));
    knownCommands.put("invest", s -> new Invest(view));
    knownCommands.put("getHistory", s -> new GetHistory(view));
    knownCommands.put("getBasis", s -> new GetBasis(view));
    knownCommands.put("getValue", s -> new GetValue(view));
    knownCommands.put("plotPort", s -> new PlotPort(view));
    knownCommands.put("createStrategy", s -> new CreateStrategy(view));
    knownCommands.put("saveStrategy", s -> new SaveStrategy(view));
    knownCommands.put("retrieveStrategy", s -> new RetrieveStrategy(view));
    knownCommands.put("viewStrategy", s -> new ViewStrategy(view));
    knownCommands.put("viewAllStrategy", s -> new ViewAllStrategy(view));
    knownCommands.put("applyStrategy", s -> new ApplyStrategy(view));
  }
}
