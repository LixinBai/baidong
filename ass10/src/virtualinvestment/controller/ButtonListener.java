package virtualinvestment.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Function;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This represent a button listener to perform according to the action event.
 */
public class ButtonListener implements ActionListener {
  private Map<String, Function<IView, Command>> knownCommands;
  private VirtualInvestment model;
  private IView view;

  /**
   * Initialize a button listener according to the given model and view.
   *
   * @param model the model
   * @param view  the view
   */
  ButtonListener(VirtualInvestment model, IView view) {
    this.model = model;
    this.view = view;
  }

  /**
   * Set the map for button events.
   *
   * @param map the map for button events.
   */
  void setButtonClickedActionMap(Map<String, Function<IView, Command>> map) {
    knownCommands = map;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (knownCommands.containsKey(e.getActionCommand())) {
      try {
        Function<IView, Command> cmd = knownCommands.get(e.getActionCommand());
        Command c = cmd.apply(view);
        c.start(model);
      } catch (IllegalArgumentException | IllegalStateException ex) {
        view.output(ex.getMessage());
      } catch (NoSuchElementException ex1) {
        view.output("Operation failed: Miss input.\n");
      }
    }
  }
}
