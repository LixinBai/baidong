package virtualinvestment.controller;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;

/**
 * This interface represent a command of a controller.
 */
public interface Command {

  /**
   * Execute the command.
   *
   * @param m the virtual investment model.
   * @throws NoSuchElementException   if it fail to get the input
   * @throws IllegalArgumentException if the input is illegal
   * @throws IllegalStateException    if the model fail to execute the command
   */
  void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException;
}
