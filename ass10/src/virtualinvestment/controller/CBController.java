package virtualinvestment.controller;

import java.util.NoSuchElementException;
import java.util.function.Function;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This represent a console based controller.
 */
public class CBController extends AbstractController {

  /**
   * Initialize the Console Based controller with given model.
   *
   * @param model the model to control
   * @throws IllegalArgumentException if and only if the model is null.
   */
  public CBController(VirtualInvestment model) throws IllegalArgumentException {
    super(model);
  }

  @Override
  public void start() {
    while (true) {
      try {
        model.reset();
        String in = view.getInput();
        if (in.equalsIgnoreCase("quit")) {
          view.output("Already quit the program.\n");
          return;
        } else if (in.equalsIgnoreCase("help")) {
          view.output("Known commands: importStock, createPort, savePort, retrievePort, "
                  + "viewPort, viewAllPort, addStock, buyStock, invest, getHistory, getBasis, "
                  + "getValue plotPort, createStrategy, saveStrategy, retrieveStrategy, "
                  + "viewStrategy, viewAllStrategy, applyStrategy, help.\n");
          continue;
        }
        Function<IView, Command> cmd = knownCommands.getOrDefault(in, null);
        if (cmd == null) {
          view.output("Command not found.\n");
        } else {
          Command c = cmd.apply(view);
          c.start(model);
        }
      } catch (NoSuchElementException e) {
        break;
      } catch (IllegalArgumentException | IllegalStateException e1) {
        view.output(e1.getMessage());
      }
    }
  }

}
