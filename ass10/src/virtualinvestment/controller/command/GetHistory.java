package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.view.IView;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the get purchase history command.
 */
public class GetHistory extends AbstractCommand {

  /**
   * Initialize a get purchase history command.
   *
   * @param v the view
   */
  public GetHistory(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    IMyCalendar time;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("time to get purchase history(yyyy-mm-dd-hh): ");
    time = new MyCalendar(view.getInput());
    if (time.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Time should be earlier than current "
              + "time.\n");
    }
    view.output(m.getPurchaseHistory(pName, time));
    view.cleanInput();
  }
}
