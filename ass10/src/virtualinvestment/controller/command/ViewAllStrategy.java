package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the view all strategy command.
 */
public class ViewAllStrategy extends AbstractCommand {

  /**
   * Initialize a view all strategy command.
   *
   * @param v the view
   */
  public ViewAllStrategy(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String all = m.viewAllStrategy();
    if (all.equals("")) {
      all = "There is no strategy.\n";
    }
    view.output(all);
  }
}
