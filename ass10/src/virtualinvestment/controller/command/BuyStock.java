package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.view.IView;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the buy stock command.
 */
public class BuyStock extends AbstractCommand {

  /**
   * Initialize a buy stock command.
   *
   * @param v the view
   */
  public BuyStock(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    String sName;
    double shares;
    IMyCalendar time;
    double fee;
    view.resetInput();
    view.setMessage("time to buy(yyyy-mm-dd-hh): ");
    time = new MyCalendar(view.getInput());
    if (time.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Time should be earlier than current "
              + "time.\n");
    }
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("stock name: ");
    sName = view.getInput();
    view.setMessage("shares to buy: ");
    shares = getDouble("number of shares", view.getInput(), false);
    view.setMessage("commission fee: ");
    fee = getDouble("commission fee", view.getInput(), true);
    m.buyStock(pName, sName, shares, fee, time);
    view.output("Buy successfully.\n");
    view.cleanInput();
  }

}
