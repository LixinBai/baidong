package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the view all portfolio command.
 */
public class ViewAllPort extends AbstractCommand {

  /**
   * Initialize a view all portfolio command.
   *
   * @param v the view
   */
  public ViewAllPort(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String all = m.viewAllPortfolio();
    if (all.equals("")) {
      all = "There is no portfolio.\n";
    }
    view.output(all);
  }
}
