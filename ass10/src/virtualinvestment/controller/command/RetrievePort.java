package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the retrieve portfolio command.
 */
public class RetrievePort extends AbstractCommand {

  /**
   * Initialize a retrieve portfolio command.
   *
   * @param v the view
   */
  public RetrievePort(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String pName;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    m.retrievePortfolio(pName);
    view.output("Portfolio " + pName + " retrieved successfully.\n");
    view.cleanInput();
  }
}
