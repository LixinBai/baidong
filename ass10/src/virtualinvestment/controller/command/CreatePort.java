package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the create port command.
 */
public class CreatePort extends AbstractCommand {

  /**
   * Initialize a create port command.
   *
   * @param v the view
   */
  public CreatePort(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    String[] stocks;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("stocks to add (separated by \",\"), or \" \" if empty: ");
    String sign = view.getInput();
    if (sign.equals(" ")) {
      m.createPortfolio(pName);
    } else {
      stocks = sign.split(",");
      m.createPortfolio(pName, stocks);
    }
    view.output("Portfolio " + pName + " created successfully.\n");
    view.cleanInput();
  }

}