package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.view.IView;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the get cost basis command.
 */
public class GetBasis extends AbstractCommand {

  /**
   * Initialize a get cost basis command.
   *
   * @param v the view
   */
  public GetBasis(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    IMyCalendar time;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("time to get cost basis(yyyy-mm-dd-hh): ");
    time = new MyCalendar(view.getInput());
    if (time.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Time should be earlier than current "
              + "time.\n");
    }
    double costBasis = m.getCostBasis(pName, time);
    view.output("The cost basis is: " + String.format("%.2f", costBasis) + ".\n");
    view.cleanInput();
  }
}
