package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

import static virtualinvestment.model.datasource.DataSourceType.ALPHAVANTAGE;

/**
 * This class represents the import stock command.
 */
public class ImportStock extends AbstractCommand {

  /**
   * Initialize an import stock command.
   * @param v the view
   */
  public ImportStock(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String api;
    String sName;
    view.resetInput();
    view.setMessage("API(Alpha Vantage): ");
    api = view.getInput();
    if (!api.equalsIgnoreCase("Alpha Vantage")) {
      throw new IllegalArgumentException("Operation failed: Unsupported API.\n");
    }
    view.setMessage("stock name: ");
    sName = view.getInput();
    m.addDataFromInternet(ALPHAVANTAGE, sName);
    view.output("Import stock " + sName + " successfully.\n");
    view.cleanInput();
  }
}
