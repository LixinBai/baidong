package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.view.IView;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the get total value command.
 */
public class GetValue extends AbstractCommand {

  /**
   * Initialize a get total value command.
   *
   * @param v the view
   */
  public GetValue(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    IMyCalendar time;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("time to get total value(yyyy-mm-dd-hh): ");
    time = new MyCalendar(view.getInput());
    if (time.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Time should be earlier than current "
              + "time.\n");
    }
    double totalValue = m.getTotalValue(pName, time);
    view.output("The total value is: " + String.format("%.2f", totalValue) + ".\n");
    view.cleanInput();
  }
}
