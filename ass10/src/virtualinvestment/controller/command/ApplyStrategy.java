package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the apply strategy command.
 */
public class ApplyStrategy extends AbstractCommand {

  /**
   * Initialize a apply strategy command.
   * @param view the view
   */
  public ApplyStrategy(IView view) {
    super(view);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String strName;
    String pName;
    view.resetInput();
    view.setMessage("strategy name: ");
    strName = view.getInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    m.applyStrategy(strName, pName);
    view.output("Apply strategy " + strName + " to portfolio " + pName + " successfully.\n");
    view.cleanInput();
  }
}
