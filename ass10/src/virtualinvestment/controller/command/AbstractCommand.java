package virtualinvestment.controller.command;

import virtualinvestment.controller.Command;
import virtualinvestment.view.IView;

/**
 * This is an abstract command of a IV controller which provides several helper methods.
 */
public abstract class AbstractCommand implements Command {
  protected final IView view;

  /**
   * Initialize a command of a IV controller.
   *
   * @param view the view
   */
  public AbstractCommand(IView view) {

    this.view = view;
  }

  static int getInt(String name, String num, boolean equalToZero)
          throws IllegalArgumentException {
    try {
      int number = Integer.valueOf(num);
      if (equalToZero && number < 0) {
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be no-negative.\n");
      }
      if (!equalToZero && number <= 0) {
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be positive.\n");
      }

      return number;
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Operation failed: Invalid " + name + ".\n");
    }
  }

  static double getDouble(String name, String num, boolean equalToZero)
          throws IllegalArgumentException {
    try {
      double number = Double.parseDouble(num);
      if (equalToZero && number < 0) { // true:can equal to 0
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be no-negative.\n");
      }
      if (!equalToZero && number <= 0) { // false: can not equal to 0
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be positive.\n");
      }
      return number;
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Operation failed: Invalid " + name + ".\n");
    }
  }

}
