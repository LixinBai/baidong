package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the view portfolio command.
 */
public class ViewPort extends AbstractCommand {

  /**
   * Initialize a view portfolio command.
   *
   * @param v the view
   */
  public ViewPort(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    String stocks = m.viewPortfolio(pName);
    if (stocks.equals("")) {
      view.output("This portfolio is empty.\n");
    } else {
      view.output("Portfolio " + pName + " contains: " + stocks + ".\n");
    }
    view.cleanInput();
  }
}
