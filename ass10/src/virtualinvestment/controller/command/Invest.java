package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.view.IView;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the invest command.
 */
public class Invest extends AbstractCommand {

  /**
   * Initialize a invest command.
   *
   * @param v the view
   */
  public Invest(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    IMyCalendar time;
    double amount;
    Map<String, Double> amountMap;
    double fee;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    String stocks = m.viewPortfolio(pName);
    if (stocks.equals("")) {
      throw new IllegalStateException("Operation failed: Portfolio to invest is empty.\n");
    }
    String[] stock = stocks.split(" ");
    view.setMessage("time to invest(yyyy-mm-dd-hh): ");
    time = new MyCalendar(view.getInput());
    if (time.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Time should be earlier than current "
              + "time.\n");
    }
    view.setMessage("amount of dollar to invest: ");
    amount = getDouble("amount of dollar", view.getInput(), false);
    view.setMessage("\"equal\" or weights of " + stocks + "(separated by \",\", add up to 1): ");
    amountMap = getWeights(stock, view.getInput(), amount);
    view.setMessage("commission fee: ");
    fee = getDouble("commission fee", view.getInput(), true);
    m.invest(pName, time, fee, amountMap);
    view.output("Invest successfully.\n");
    view.cleanInput();
  }


  protected Map<String, Double> getWeights(String[] stock, String weights, double amount) throws
          NoSuchElementException, IllegalStateException, IllegalArgumentException {

    String [] weightStr;
    double [] weight;
    if (weights.equals("equal")) {
      return equal(stock, amount);
    } else {
      Map<String, Double> amountMap = new LinkedHashMap<>();
      int num = stock.length;
      weightStr = weights.split(",");
      if (weightStr.length != stock.length) {
        throw new IllegalArgumentException("Operation failed: "
                + "Number of weights should equals to number of stocks.\n");
      }
      weight = new double[num];
      double total = 0;
      for (int i = 0; i < num; i++) {
        weight[i] = getDouble("weight of stock", weightStr[i], true);
        total = total + weight[i];
      }
      if (total != 1) {
        throw new IllegalArgumentException("Operation failed: Total weights should add up to 1.\n");
      }
      for (int i = 0; i < num; i++) {
        amountMap.put(stock[i], weight[i] * amount);
      }
      return amountMap;
    }
  }

  private Map<String, Double> equal(String[] stock, double amount) {
    int num = stock.length;
    Map<String, Double> amountMap = new LinkedHashMap<>();
    for (int i = 0; i < num; i++) {
      amountMap.put(stock[i], amount / (double) num);
    }
    return amountMap;
  }

}
