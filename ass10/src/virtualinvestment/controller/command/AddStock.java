package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the add stock command.
 */
public class AddStock extends AbstractCommand {

  /**
   * Initialize an add Stock command.
   *
   * @param v the view
   */
  public AddStock(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String pName;
    String sName;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("stock name: ");
    sName = view.getInput();
    m.addStock(pName, sName);
    view.output("Add stock " + sName + " successfully.\n");
    view.cleanInput();
  }
}
