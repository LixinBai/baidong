package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.PlotType;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the plot portfolio command.
 */
public class PlotPort extends AbstractCommand {

  /**
   * Initialize a plot portfolio command.
   *
   * @param v the view
   */
  public PlotPort(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String pName;
    IMyCalendar start;
    IMyCalendar end;
    PlotType type;
    int interval;
    view.resetInput();
    view.setMessage("type of interval(year/month/day): ");
    String sign = view.getInput();
    switch (sign) {
      case "year":
        type = PlotType.YEAR;
        break;
      case "month":
        type = PlotType.MONTH;
        break;
      case "day":
        type = PlotType.DAY;
        break;
      default:
        throw new IllegalArgumentException("Operation failed: Invalid interval type.\n");
    }
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    view.setMessage("start date(yyyy-mm-dd): ");
    start = new MyCalendar(view.getInput() + "-23");
    if (start.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Start date should be earlier than "
              + "current date.\n");
    }
    view.setMessage("end date(yyyy-mm-dd): ");
    end = new MyCalendar(view.getInput() + "-23");
    if (end.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: End date should be earlier than "
              + "current date.\n");
    }
    if (!end.after(start)) {
      throw new IllegalArgumentException("Operation failed: End date should be later than start "
              + "date.\n");
    }
    view.setMessage("interval: ");
    interval = getInt("interval", view.getInput(), false);
    Map<String, Double> map = m.plot(pName, start, end, type, interval);
    if (map.size() == 1) {
      throw new IllegalStateException("Operation failed: Interval is too long.\n");
    }
    view.plot(map, pName);
    view.cleanInput();
  }
}
