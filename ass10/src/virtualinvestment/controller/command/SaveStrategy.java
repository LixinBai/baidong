package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the save strategy command.
 */
public class SaveStrategy extends AbstractCommand {

  /**
   * Initialize a save strategy command.
   * @param v the view
   */
  public SaveStrategy(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {

    String strName;
    view.resetInput();
    view.setMessage("strategy name: ");
    strName = view.getInput();
    m.saveStrategy(strName);
    view.output("Strategy " + strName + " saved successfully.\n");
    view.cleanInput();
  }
}
