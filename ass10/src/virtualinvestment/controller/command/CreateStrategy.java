package virtualinvestment.controller.command;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.NoSuchElementException;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.view.IView;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the create strategy command.
 */
public class CreateStrategy extends Invest {

  /**
   * Initialize a create strategy command.
   *
   * @param v the view
   */
  public CreateStrategy(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException,
          IllegalArgumentException, IllegalStateException {
    String type;
    String strName;
    IMyCalendar start;
    IMyCalendar end;
    int interval;
    double amount;
    Map<String, Double> amountMap;
    double fee;
    view.resetInput();
    view.setMessage("strategy type(dollar-cost averaging): ");
    type = view.getInput();
    if (!type.equalsIgnoreCase("dollar-cost averaging")) {
      throw new IllegalArgumentException("Operation failed: Unsupported type.\n");
    }
    view.setMessage("strategy name: ");
    strName = view.getInput();
    view.setMessage("start date(yyyy-mm-dd): ");
    start = new MyCalendar(view.getInput() + "-16");
    if (start.after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Start date should be earlier than "
              + "current date.\n");
    }
    view.setMessage("end date(yyyy-mm-dd or ongoing): ");
    String sign = view.getInput();
    if (!sign.equals("ongoing")) {
      end = new MyCalendar(sign + "-16");
      if (!end.after(start)) {
        throw new IllegalArgumentException("Operation failed: End date should be later than start "
                + "date.\n");
      }
    } else {
      end = null;
    }
    view.setMessage("interval days: ");
    interval = getInt("interval days", view.getInput(), false);
    view.setMessage("amount of dollar to invest: ");
    amount = getDouble("amount of dollar", view.getInput(), false);
    view.setMessage("stocks to invest (separated by \",\"): ");
    String[] stock = view.getInput().split(",");
    view.setMessage("\"equal\" or weights of above stocks (separated by \",\", add up to 1): ");
    amountMap = getWeights(stock, view.getInput(), amount);
    view.setMessage("commission fee: ");
    fee = getDouble("commission fee", view.getInput(), true);
    m.createDollarCostAverage(strName, start, end, interval, fee, amountMap);
    view.output("Create a " + type + " successfully.\n");
    view.cleanInput();
  }
}
