package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;

import virtualinvestment.view.IView;

/**
 * This class represents the retrieve strategy command.
 */
public class RetrieveStrategy extends AbstractCommand {

  /**
   * Initialize a retrieve strategy command.
   * @param v the view
   */
  public RetrieveStrategy(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String strName;
    view.resetInput();
    view.setMessage("strategy name: ");
    strName = view.getInput();
    m.retrieveStrategy(strName);
    view.output("Strategy " + strName + " retrieved successfully.\n");
    view.cleanInput();
  }
}
