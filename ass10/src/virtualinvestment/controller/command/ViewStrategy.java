package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the view strategy command.
 */
public class ViewStrategy extends AbstractCommand {

  /**
   * Initialize a view strategy command.
   *
   * @param v the view
   */
  public ViewStrategy(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m)
          throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
    String strName;
    view.resetInput();
    view.setMessage("strategy name: ");
    strName = view.getInput();
    String strategy = m.viewStrategy(strName);
    if (strategy.equals("")) {
      view.setMessage("This strategy is empty\n");
    } else {
      view.output(strategy + "\n");
    }
    view.cleanInput();
  }
}
