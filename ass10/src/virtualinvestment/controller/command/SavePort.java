package virtualinvestment.controller.command;

import java.util.NoSuchElementException;

import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.IView;

/**
 * This class represents the save portfolio command.
 */
public class SavePort extends AbstractCommand {

  /**
   * Initialize a save portfolio command.
   * @param v the view
   */
  public SavePort(IView v) {
    super(v);
  }

  @Override
  public void start(VirtualInvestment m) throws NoSuchElementException, IllegalArgumentException,
          IllegalStateException {
    String pName;
    view.resetInput();
    view.setMessage("portfolio name: ");
    pName = view.getInput();
    m.savePortfolio(pName);
    view.output("Portfolio " + pName + " saved successfully.\n");
    view.cleanInput();
  }
}
