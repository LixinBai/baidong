package virtualinvestment;

import java.io.InputStreamReader;
import java.util.Scanner;

import virtualinvestment.controller.CBController;
import virtualinvestment.controller.GUIController;
import virtualinvestment.controller.IController;
import virtualinvestment.model.InvestmentModel;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.CBView;
import virtualinvestment.view.GUIView;
import virtualinvestment.view.IView;

import static java.lang.System.exit;

/**
 * This class is to start a virtual investment program by a graphical view.
 */
public class Play {

  /**
   * The main method to start a GUI program.
   */
  public static void main(String[] args) {
    VirtualInvestment model = new InvestmentModel();
    IController controller = null;
    IView view = null;
    Readable in = new InputStreamReader(System.in);
    System.out.println("Please input the type of view (console or gui): ");
    Scanner scan = new Scanner(System.in);
    switch (scan.next()) {
      case "console":
        controller = new CBController(model);
        view = new CBView(in, System.out);
        System.out.println("Known commands: importStock, createPort, savePort, retrievePort, "
                + "viewPort, viewAllPort, addStock, buyStock, invest, getHistory, getBasis, "
                + "plotPort, createStrategy, saveStrategy, retrieveStrategy, viewStrategy, "
                + "getValue viewAllStrategy, applyStrategy, help.\n"
                + "Input help to get this message again.\n");
        break;
      case "gui":
        controller = new GUIController(model);
        view = new GUIView();
        break;
      default:
        exit(0);
    }
    controller.setView(view);
    controller.start();
  }
}
