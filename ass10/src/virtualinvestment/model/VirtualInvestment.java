package virtualinvestment.model;

import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.model.datasource.DataSourceType;

/**
 * This interface represents all possible operations for a virtual investment system.
 */
public interface VirtualInvestment {

  /**
   * Allow users to fetch information from internet and create a new stock with given name.
   * If the price data is already stored in file, the system will find that file and load data
   * in it.
   *
   * @param name name of the stock
   * @throws IllegalStateException if the stock is already exist or the alphavantage API has either
   *                               changed or no longer works or there is no price found on the
   *                               Internet or in the file.
   */
  void addDataFromInternet(DataSourceType api, String name) throws IllegalStateException;

  /**
   * Create a new portfolio.
   *
   * @param pName name of the portfolio
   * @throws IllegalStateException if the portfolio name is already exist
   */
  void createPortfolio(String pName) throws IllegalStateException;


  /**
   * Create a portfolio with specific composition of stocks.
   * If the stock does not exist in the system, it will be automatically imported.
   *
   * @param pName  name of the portfolio
   * @param sNames names of stocks
   * @throws IllegalStateException if the portfolio name is already exist
   */
  void createPortfolio(String pName, String[] sNames) throws IllegalStateException;

  /**
   * Add a stock to a portfolio. If the stock does not exist in the system, it will be
   * automatically imported.
   *
   * @param pName name of the portfolio
   * @param sName name of the stock
   * @throws IllegalStateException if the portfolio or stock name does not exist
   */
  void addStock(String pName, String sName) throws IllegalStateException;

  /**
   * Get compositions of the given portfolio at a specific time. The compositions will be printed in
   * dictionary order. This method will automatically use the closest previous price as the value of
   * the stock.
   *
   * @param pName name of the portfolio
   * @param time the time to check composition
   * @return composition of given portfolio
   * @throws IllegalStateException if the portfolio name does not exist
   */
  String getPurchaseHistory(String pName, IMyCalendar time) throws IllegalStateException;

  /**
   * Buy shares of some stock in a portfolio worth a certain amount at a certain date. Users must
   * chose a portfolio to put the stock in.
   *
   * @param pName  name of the portfolio
   * @param sName  name of the stock
   * @param shares amount of share as a double
   * @param fee    amount of commission fee
   * @param time   the time user want to buy
   * @throws IllegalStateException if the portfolio or stock does not exist, users are trying to buy
   *                               stocks at a rest time, or the
   *                               stock does not exist in given portfolio
   */
  void buyStock(String pName, String sName, double shares, double fee, IMyCalendar time)
          throws IllegalStateException;///////int -> double, add a commission fee

  /**
   * Invest a fixed amount into an existing portfolio containing multiple stocks, using a specified
   * weight for each stock in the portfolio.
   *
   * @param pName   name of the portfolio
   * @param time   time of investment
   * @param fee     commission fee for each transaction
   * @param amounts weights for each stocks
   * @throws IllegalStateException if given portfolio does not exist, stock market is closed at
   *                               given date, or price data is missed for some of the stocks at
   *                               given date
   */
  void invest(String pName, IMyCalendar time, double fee, Map<String,Double> amounts)
          throws IllegalStateException;

  /**
   * Create a dollar cost average strategy.
   * @param strategyName name of the strategy
   * @param start start time
   * @param end end time
   * @param interval investment frequency
   * @param fee commission fee
   * @param weights weights for each stock
   * @throws IllegalStateException if any stock in this strategy does not exist in this system,
   *                               strategy ia already exist in system or saved
   */
  void createDollarCostAverage(String strategyName, IMyCalendar start, IMyCalendar end,
                               int interval, double fee, Map<String,Double> weights)
          throws IllegalStateException;

  /**
   * Retrieve a strategy from files.
   * @param strName name of the strategy
   * @throws IllegalStateException if the strategy is already in this system, or the strategy
   *                               is never saved
   */
  void retrieveStrategy(String strName)throws IllegalStateException;

  /**
   * Apply a strategy to a portfolio.
   * @param strName name of the strategy
   * @param pName name of the portfolio
   * @throws IllegalStateException if the strategy or portfolio does not exist
   */
  void applyStrategy(String strName, String pName)throws IllegalStateException;

  /**
   * Save the given strategy for future usage.
   * @param strName name of the strategy
   * @throws IllegalStateException if the strategy does not exist in this system
   */
  void saveStrategy(String strName)throws IllegalStateException;

  /**
   * Get all the strategy names.
   * @return all strategy names
   */
  String viewAllStrategy();

  /**
   * Get the detail of a strategy.
   * @param strName name of the strategy
   * @return detail of a strategy
   * @throws IllegalStateException if the strategy does not exist
   */
  String viewStrategy(String strName)throws IllegalStateException;

  /**
   * Save a portfolio.
   * @param pName name of the portfolio
   * @throws IllegalStateException if the portfolio does not exist in this system
   */
  void savePortfolio(String pName)throws IllegalStateException;

  /**
   * Retrieve a portfolio.
   * @param pName name of the portfolio
   * @throws IllegalStateException the portfolio is already in the system,
   *                               or the portfolio is never saved
   */
  void retrievePortfolio(String pName)throws IllegalStateException;

  /**
   * Get all the stock names in given portfolio.
   *
   * @param pName name of the portfolio
   * @return all stock names
   * @throws IllegalStateException if the portfolio name does not exist
   */
  String viewPortfolio(String pName) throws IllegalStateException;

  /**
   * get all portfolios with stocks in them.
   * @return all stock names in given portfolio
   */
  String viewAllPortfolio();

  /**
   * Get cost basis of a portfolio at a specific time. The system will calculate the costs before
   * that given time.
   *
   * @param pName name of the portfolio
   * @param time  time before which user want to get cost basis
   * @return cost basis of given portfolio before given time
   * @throws IllegalStateException if the portfolio name does not exist, or some price data at time
   *                               strategies should apply are missed
   */
  double getCostBasis(String pName, IMyCalendar time) throws IllegalStateException;

  /**
   * Get total value of a portfolio at a specific time.
   *
   * @param pName name of the portfolio
   * @param time  time at which user want to get total value
   * @return total value of given portfolio at given time
   * @throws IllegalStateException if the portfolio name does not exist, or some price data at time
   *                               strategies should apply are missed
   */
  double getTotalValue(String pName, IMyCalendar time) throws IllegalStateException;

  /**
   * Plot the performance of a portfolio over time. the x-coordinator is time of observation,
   * and y-coordinator is the total value of the portfolio at given time.
   * @param pName name of the portfolio
   * @param start start time
   * @param end end time
   * @param type type of observation
   * @param interval frequency of observation
   * @return a map representing data on the plot
   */
  Map<String, Double> plot(String pName, IMyCalendar start, IMyCalendar end, PlotType type,
                           int interval);

  /**
   * Clean all the stock data in this system. This is used to refresh data.
   */
  void reset();

}
