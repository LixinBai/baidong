package virtualinvestment.model.datasource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import virtualinvestment.model.Stock;

/**
 * This class represents all possible operations on a alpha vantage API data source.
 */
public class AlphaVantage extends AbstractDataSource {

  /**
   * Constructor of alpha vantage API.
   *
   * @param name name of the stock user want to fetch
   */
  public AlphaVantage(String name) {
    super(name);
  }

  /**
   * Get data from internet, and save it to local file.
   *
   * @return a map of time and corresponding price data
   */
  public HashMap<String, Stock> getData() {
    HashMap<String, Stock> stocks = new LinkedHashMap<>();
    String apiKey = "ALEDV8MB6B5NYIGQ";//IHM5FSYKD3V4O31Y
    String stockSymbol = name; //ticker symbol for Google
    URL url = null;

    try {
      url = new URL("https://www.alphavantage"
              + ".co/query?function=TIME_SERIES_DAILY"
              + "&outputsize=full"
              + "&symbol"
              + "=" + stockSymbol + "&apikey=" + apiKey + "&datatype=csv");
    } catch (MalformedURLException e) {
      throw new RuntimeException("the alphavantage API has either changed or "
              + "no longer works");
    }

    InputStream in = null;
    try {
      in = url.openStream();
      Stock stock = new Stock(name);
      Scanner sc = new Scanner(in);
      String head = sc.next();
      Pattern judge = Pattern.compile("^timestamp,open,high,low,close,volume$");
      Matcher m = judge.matcher(head);
      if (!m.find()) {
        sc.close();
        throw new IllegalStateException("Operation failed: No price found for "
                + stockSymbol + ".\n");
      }
      File newFile = new File("data/stocks/" + name + ".txt");
      //write to file
      BufferedWriter writer = new BufferedWriter(new FileWriter(newFile));
      while (sc.hasNext()) {
        String line = sc.next();
        //System.out.println(line);
        writer.write(line + "\n");
        super.addDataHelper(line, stock);
      }
      sc.close();
      writer.close();
      stocks.put(name, stock);

    } catch (IOException e) {
      File f = new File("data/stocks/" + name + ".txt");
      f.delete();
      throw new IllegalStateException("Operation failed: No price data found for "
              + stockSymbol + ".\n");
    }
    return stocks;

  }
}
