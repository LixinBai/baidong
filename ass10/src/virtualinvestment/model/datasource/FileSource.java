package virtualinvestment.model.datasource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import virtualinvestment.model.Stock;

/**
 * This class represents all possible operations on a file data source.
 */
public class FileSource extends AbstractDataSource {
  /**
   * Constructor of the data source.
   *
   * @param name name of the stock the user want to get data from
   */
  public FileSource(String name) {
    super(name);
  }

  @Override
  public HashMap<String, Stock> getData() {
    HashMap<String, Stock> stocks = new LinkedHashMap<>();
    File file = new File("data/stocks/" + name + ".txt");
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      Stock stock = new Stock(name);
      for (String line; (line = br.readLine()) != null; ) {
        // process the line.
        addDataHelper(line, stock);
      }
      br.close();
      stocks.put(name, stock);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalStateException("Operation failed: File not found.\n");
    } catch (IOException e) {
      //e.printStackTrace();
      throw new IllegalStateException("Operation failed: I/O Exception.\n");
    }
    return stocks;
  }
}
