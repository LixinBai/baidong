package virtualinvestment.model.datasource;

/**
 * This class represents all data source types.
 */
public enum DataSourceType {
  ALPHAVANTAGE, FILE,
}
