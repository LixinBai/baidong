package virtualinvestment.model.datasource;

import java.util.HashMap;

import virtualinvestment.model.Stock;

/**
 * This class represents all operations of a data source where the system can fetch price data.
 */
public interface DataSource {
  /**
   * Get all price data as a map.
   *
   * @return map of time ad price
   */
  HashMap<String, Stock> getData();
}
