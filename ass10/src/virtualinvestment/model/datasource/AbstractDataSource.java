package virtualinvestment.model.datasource;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.Stock;

/**
 * Ths class represents all common operations of a data source.
 */
public abstract class AbstractDataSource implements DataSource {
  /**
   * Constructor of a data source.
   */
  protected String name;

  public AbstractDataSource(String name) {
    this.name = name;
  }


  /**
   * A helper function that puts price data to a stock object.
   *
   * @param line  time of the price
   * @param stock price of ths stock
   */
  protected void addDataHelper(String line, Stock stock) {
    String[] entry = line.split(",");
    try {
      IMyCalendar closeTime = new MyCalendar(entry[0] + "-16");
      stock.addPrice(closeTime, Double.parseDouble(entry[4]));
    } catch (IllegalArgumentException e) {
      //ignore this entry
    }
    try {
      IMyCalendar openTime = new MyCalendar(entry[0] + "-9");
      stock.addPrice(openTime, Double.parseDouble(entry[1]));
    } catch (IllegalArgumentException e) {
      //ignore this entry
    }
  }
}
