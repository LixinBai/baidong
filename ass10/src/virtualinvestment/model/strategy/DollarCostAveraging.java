package virtualinvestment.model.strategy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.Property;
import virtualinvestment.model.Stock;
import virtualinvestment.model.datasource.AlphaVantage;

import static java.util.Calendar.DATE;

/**
 * This class represents a dollar cost average strategy.
 */
public class DollarCostAveraging extends AbstractStrategy {
  private StrategyType type;
  private int interval;

  /**
   * Initialize a dollar cost average strategy.
   *
   * @param start    time to start
   * @param end      time to end
   * @param amounts  amount of dollars to invest
   * @param fee      commission fee
   * @param interval interval days of each investment
   */
  public DollarCostAveraging(String strategyName, IMyCalendar start, IMyCalendar end,
                             HashMap<Stock, Double> amounts, double fee, int interval) {
    super(strategyName, start, end, amounts, fee);
    this.type = StrategyType.DOLLARCOSTAVE;
    this.interval = interval;
  }

  /**
   * Get details of the strategy, including its start and end time, interval, commission fee, and
   * all stocks.
   *
   * @return details of the strategy
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("From: ").append(start.toString()).append("  ");
    sb.append("To: ");
    if (end == null) {
      sb.append("  ongoing ");
    } else {
      sb.append(end.toString());
    }
    sb.append("  ");
    sb.append("Interval: ").append(interval).append("  ");
    sb.append("Commission Fee: ").append(fee).append("\n");
    for (Map.Entry<Stock, Double> entry : amounts.entrySet()) {
      sb.append("Stock: ").append(entry.getKey().getName()).append("  ");
      sb.append("Amount of Money: ").append(String.format("%.2f", entry.getValue()));
    }
    return sb.toString();
  }


  @Override
  public List<Property> apply(IMyCalendar time) {
    List<Property> result = new LinkedList<>();
    if (time.before(start)) {
      return result;
    }
    if (end == null) {
      //calculate properties from start to given time
      result = applyHelper(start, time);
    } else {
      if (time.after(end)) {
        //calculate properties from start to end
        result = applyHelper(start, end);
      } else {
        //calculate properties from start to given time
        result = applyHelper(start, time);
      }
    }
    return result;
  }

  @Override
  public void save() {
    File newFile = new File("data/strategies/" + name + ".txt");
    if (newFile.exists()) {
      newFile.delete();
    }
    try {
      if (newFile.createNewFile()) {
        //write to file
        BufferedWriter writer = new BufferedWriter(new FileWriter("data/strategies/"
                + name + ".txt"));
        writer.write(type.toString() + "\n");
        writer.write(start.toString() + "\n");
        if (end == null) {
          writer.write("ongoing\n");
        } else {
          writer.write(end.toString() + "\n");
        }
        writer.write(interval + "\n");
        writer.write(fee + "\n");
        writer.write(amounts.size() + "\n");
        for (Map.Entry<Stock, Double> entry : amounts.entrySet()) {
          writer.write(entry.getKey().getName() + "," + entry.getValue() + "\n");
        }
        writer.close();
      } else {
        throw new IllegalStateException("Operation failed: File creation failed.\n");
      }
    } catch (IOException e) {
      throw new IllegalStateException("Operation failed: File creation failed.\n");
    }
  }

  private List<Property> applyHelper(IMyCalendar start, IMyCalendar end) {
    List<Property> result = new LinkedList<>();
    //find times
    List<IMyCalendar> times = new LinkedList<>();
    IMyCalendar temp = new MyCalendar(start.toString());
    while (temp.before(end) || temp.toString().equals(end.toString())) {
      times.add(new MyCalendar(temp.toString()));
      temp.add(DATE, interval);
    }
    String prev = "";
    List<IMyCalendar> newTimes = new LinkedList<>();
    for (IMyCalendar m : times) {
      if (m.isClose() != null) {
        //next available day
        while (m.isClose() != null) {
          m.add(DATE, 1);
        }
      }
      if (!m.toString().equals(prev)) {
        //times.remove(m);
        newTimes.add(new MyCalendar(m.toString()));
      }
      prev = m.toString();
    }

    //for each stocks, buy it at each time
    for (Map.Entry<Stock, Double> entry : amounts.entrySet()) {
      Stock s = entry.getKey();
      for (IMyCalendar t : newTimes) {
        double share = s.getShare(t, entry.getValue());
        if (share != 0.0) {
          Property p = new Property(t, s, share, fee);
          result.add(p);
        }
      }
    }

    return result;
  }

  /**
   * Get a strategy from file.
   *
   * @param file file that stores the strategy
   * @return a strategy object
   */
  static Strategy getStrategy(File file) {
    Strategy strategy = null;
    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      br.readLine();

      String strStart = br.readLine();
      IMyCalendar start = new MyCalendar(strStart);

      String strEnd = br.readLine();
      IMyCalendar end;
      if (strEnd.equals("ongoing")) {
        end = null;
      } else {
        end = new MyCalendar(strEnd);
      }
      int interval = Integer.parseInt(br.readLine());

      double fee = Double.parseDouble(br.readLine());

      int amounts = Integer.parseInt(br.readLine());

      HashMap<Stock, Double> map = new LinkedHashMap<>();
      for (int i = 0; i < amounts; i++) {
        String[] entry = br.readLine().split(",");
        Stock stock = new AlphaVantage(entry[0]).getData().get(entry[0]);
        map.put(stock, Double.parseDouble(entry[1]));
        String name = file.getName();
        strategy = new DollarCostAveraging(name.substring(0, name.length() - 4), start, end, map,
                fee, interval);
      }

      br.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalStateException("Operation failed: File not found.\n");
    } catch (IOException e) {
      throw new IllegalStateException("Operation failed: I/O Exception.\n");
    }
    return strategy;
  }


}
