package virtualinvestment.model.strategy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.model.Property;

/**
 * This class represents all possible operations on a strategy.
 */
public interface Strategy {


  /**
   * Get all purchase history after applying a strategy within given time.
   *
   * @param time end time of the observation
   * @return all purchase history
   */
  List<Property> apply(IMyCalendar time);

  /**
   * Get all the stock names in this strategy.
   *
   * @return all stock names.
   */
  String getStocks();

  /**
   * Save the strategy to local file.
   */
  void save();

  /**
   * Get all the details of a strategy.
   */
  String toString();

  /**
   * Get name of the strategy.
   *
   * @return name of the strategy
   */
  String getName();

  /**
   * Get the start time of the strategy.
   *
   * @return start time
   */
  IMyCalendar getStartTime();

  /**
   * Retrieve a strategy from file.
   *
   * @param file file that stores the strategy
   * @return a strategy object
   */
  static Strategy retrieve(File file) {
    Strategy strategy = null;
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      String type = br.readLine();
      br.close();
      try {
        if (type.equals("DOLLARCOSTAVE")) {
          strategy = DollarCostAveraging.getStrategy(file);
        } else {
          throw new IllegalStateException("Operation failed: Strategy does not exist.\n");
        }
      } catch (IllegalArgumentException e) {
        throw new IllegalStateException(" Operation failed: Strategy type does not exist\n");
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalStateException("Operation failed: File not found.\n");
    } catch (IOException e) {
      throw new IllegalStateException("Operation failed: I/O Exception.\n");
    }
    return strategy;
  }
}