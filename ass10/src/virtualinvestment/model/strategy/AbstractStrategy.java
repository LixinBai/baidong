package virtualinvestment.model.strategy;

import java.util.HashMap;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.model.Stock;

/**
 * This is an abstract strategy of virtual investment model.
 */
public abstract class AbstractStrategy implements Strategy {
  protected String name;
  protected IMyCalendar start;
  protected IMyCalendar end;
  protected HashMap<Stock, Double> amounts;
  protected double fee;

  /**
   * Initialize an abstract strategy.
   *
   * @param start   time to start
   * @param end     time to end
   * @param amounts amoun of dollar to invest
   * @param fee     commission fee
   */
  AbstractStrategy(String name, IMyCalendar start, IMyCalendar end,
                   HashMap<Stock, Double> amounts, double fee) {
    this.name = name;
    this.start = start;
    this.end = end;
    this.amounts = amounts;
    this.fee = fee;
  }

  /**
   * Get stocks in this strategy.
   * @return stocks in this strategy as a string
   */
  public String getStocks() {
    StringBuilder sb = new StringBuilder();
    for (Stock stock : amounts.keySet()) {
      sb.append(stock.getName()).append(" ");
    }
    return sb.toString();
  }

  public String getName() {
    return this.name;
  }

  public IMyCalendar getStartTime() {
    return this.start;
  }

}
