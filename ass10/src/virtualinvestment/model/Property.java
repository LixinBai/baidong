package virtualinvestment.model;

import virtualinvestment.calendar.IMyCalendar;

/**
 * This cass represents a property in a portfolio. A property consists of stock, its buying time and
 * amount of shares.
 */
public class Property {
  private IMyCalendar time;
  private Stock stock;
  private double share;
  private double commissionFee;

  /**
   * Initialize a portfolio according to given properties.
   * @param time buying time
   * @param stock stock name
   * @param share shares
   * @param commissionFee commission fee
   */
  public Property(IMyCalendar time, Stock stock, double share, double commissionFee) {
    this.time = time;
    this.stock = stock;
    this.share = share;
    this.commissionFee = commissionFee;
  }

  IMyCalendar getTime() {
    return time;
  }

  Stock getStock() {
    return stock;
  }

  double getShare() {
    return share;
  }

  double getCommissionFee() {
    return commissionFee;
  }

  @Override
  public String toString() {
    return time.toString() + "," + stock.getName() + "," + share + "," + commissionFee;
  }

}
