package virtualinvestment.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.datasource.AlphaVantage;
import virtualinvestment.model.strategy.Strategy;

/**
 * This class represents a portfolio in a virtual investment system.
 */
class Portfolio {
  private Map<String, Stock> stocks;
  private List<Property> properties;
  private List<Strategy> plans;

  /**
   * A constructor for a portfolio. Initially a portfolio is empty.
   */
  Portfolio() {
    this.stocks = new LinkedHashMap<>();
    this.properties = new LinkedList<>();
    this.plans = new LinkedList<>();
  }

  /**
   * A private constructor as a helper function for {@link #retrieve(File, Map)}.
   *
   * @param stocks     all stocks in a portfolio
   * @param properties all purchase history
   * @param plans      all strategies in this portfolio
   */
  private Portfolio(Map<String, Stock> stocks, List<Property> properties, List<Strategy> plans) {
    this.stocks = stocks;
    this.properties = properties;
    this.plans = plans;
  }

  /**
   * Add a stock to this portfolio.
   *
   * @param stock stock to add
   * @throws IllegalStateException if the stock is already exist in this portfolio
   */
  void addStock(Stock stock) throws IllegalStateException {
    if (stocks.containsKey(stock.getName())) {
      throw new IllegalStateException("Operation failed: Stock already exists.\n");
    }
    stocks.put(stock.getName(), stock);
  }

  /**
   * Get all stock names in this portfolio.
   *
   * @return all stock names
   */
  String getStocks() {
    StringBuilder sb = new StringBuilder();
    for (Map.Entry<String, Stock> entry : stocks.entrySet()) {
      sb.append(entry.getKey());
      sb.append(" ");
    }
    return sb.toString();
  }

  /**
   * Buy some stocks to given portfolio.
   *
   * @param time  buying time
   * @param stock stock name
   * @param share amount of stock
   * @throws IllegalStateException if the stock is not in this portfolio
   */
  void buyStock(IMyCalendar time, Stock stock, double share, double commissionFee)
          throws IllegalStateException {
    if (!stocks.containsKey(stock.getName())) {
      throw new IllegalStateException("Operation failed: Stock is not in this portfolio.\n");
    }

    Property p = new Property(time, stock, share, commissionFee);
    properties.add(p);
  }

  /**
   * Get cost basis of this portfolio before given time.
   *
   * @param time time with which to find cost basis
   * @return cost basis of this portfolio
   */
  double getCostBasis(IMyCalendar time) {
    double plain = properties.stream().filter(p -> !p.getTime().after(time))
            .map(c -> c.getStock().getValue(c.getTime()) * c.getShare() + c.getCommissionFee())
            .reduce(0.0, (a, b) -> a + b);
    List<Property> planProperties = new LinkedList<>();
    for (Strategy strategy : plans) {
      planProperties.addAll(strategy.apply(time));
    }
    double plan = planProperties.stream().map(c -> c.getStock().getValue(c.getTime()) * c.getShare()
            + c.getCommissionFee()).reduce(0.0, (a, b) -> a + b);
    return plain + plan;
  }

  /**
   * Get total value of this portfolio.
   *
   * @param time time with which to find total value
   * @return total value of this portfolio
   */
  double getTotalValue(IMyCalendar time) {
    //add dollarCostAveraging properties first
    double plain = properties.stream().filter(p -> !p.getTime().after(time))
            .map(c -> c.getStock().getValue(time) * c.getShare())
            .reduce(0.0, (a, b) -> a + b);
    //+ planProperties.
    List<Property> planProperties = new LinkedList<>();
    for (Strategy strategy : plans) {
      planProperties.addAll(strategy.apply(time));
    }
    double plan = planProperties.stream().map(c -> c.getStock().getValue(time) * c.getShare())
            .reduce(0.0, (a, b) -> a + b);
    return plain + plan;
  }

  /**
   * Invest fixed amount of money for each stocks in this portfolio.
   *
   * @param amounts       map of stocks and amount of money to invest
   * @param time          time to invest
   * @param commissionFee commission fee of this investment
   * @throws IllegalStateException if the investment time is before a certain stock exist
   */
  void invest(Map<Stock, Double> amounts, IMyCalendar time, double commissionFee)
          throws IllegalStateException {
    //buy stocks
    List<Property> invest = new LinkedList<>();
    for (Map.Entry<Stock, Double> entry : amounts.entrySet()) {
      Stock s = entry.getKey();
      double share = s.getShare(time, entry.getValue());
      if (share != 0) {
        Property p = new Property(time, s, share, commissionFee);
        invest.add(p);
      }
    }
    properties.addAll(invest);
  }

  /**
   * Add a strategy to this portfolio.
   *
   * @param strategy strategy to add
   * @throws IllegalStateException if some of the stocks does not exist in this portfolio, or the
   *                               start time of the strategy is before the stock exist.
   */
  void addStrategy(Strategy strategy) throws IllegalStateException {
    //nake sure the stocks match
    String s = strategy.getStocks();
    String[] strategyStocks = s.split(" ");
    for (String name : strategyStocks) {
      if (!stocks.containsKey(name)) {
        throw new IllegalStateException("Operation failed: Stock "
                + name + " does not exist in portfolio you chose");
      }
      if (!stocks.get(name).ableTobuy(strategy.getStartTime())) {
        throw new IllegalStateException("Operation failed: Any start time of a strategy cannot "
                + "before the stock exist.");
      }
    }
    plans.add(strategy);
  }


  /**
   * Get composition of this portfolio.
   *
   * @return composition as a formatted string
   */
  String toString(IMyCalendar time) {
    List<Property> filter = properties.stream().filter(c -> !c.getTime().after(time))
            .collect(Collectors.toList());

    //+ planPropertie.
    for (Strategy strategy : plans) {
      filter.addAll(strategy.apply(time));
    }

    SortedMap<String, Double> result = new TreeMap<>();
    for (Property p : filter) {
      String stock = p.getStock().getName();
      String cost = Double.toString(p.getStock().getValue(p.getTime()));
      String key = "Stock Name: " + stock + "  Unit Price: " + cost;
      if (result.containsKey(key)) {
        //merge
        result.compute(key, (k, v) -> v += p.getShare());
      } else {
        result.put(key, p.getShare());
      }
    }
    StringBuilder sb = new StringBuilder();
    for (Map.Entry<String, Double> entry : result.entrySet()) {
      sb.append(entry.getKey()).append("  Shares: ").append(String.format("%.2f", entry.getValue()))
              .append("\n");
    }
    if (sb.toString().equals("")) {
      return "This portfolio is empty.\n";
    }
    return sb.toString();
  }

  void save(String name) {
    File newFile = new File("data/portfolios/" + name + ".txt");
    if (newFile.exists()) {
      newFile.delete();
    }
    try {
      if (newFile.createNewFile()) {
        //write to file
        BufferedWriter writer = new BufferedWriter(new FileWriter("data/portfolios/"
                + name + ".txt"));
        writer.write(stocks.size() + "\n");
        for (Map.Entry<String, Stock> entry : stocks.entrySet()) {
          writer.write(entry.getKey() + "\n");
        }

        writer.write(properties.size() + "\n");
        for (Property p : properties) {
          writer.write(p.toString() + "\n");
        }

        writer.write(plans.size() + "\n");
        for (Strategy strategy : plans) {
          strategy.save();
          writer.write(strategy.getName() + "\n");
        }

        writer.close();
      } else {
        throw new IllegalStateException("File creation failed.");
      }
    } catch (IOException e) {
      throw new IllegalStateException("File creation failed.");
    }
  }

  /**
   * Retrieve a portfolio from a file.
   * @param file the data file
   * @param stockPool map of stocks imported in the program
   * @return the portfolio retrieved
   */
  static Portfolio retrieve(File file, Map<String, Stock> stockPool) {
    Map<String, Stock> stocksInPort = new LinkedHashMap<>();
    List<Property> properties = new LinkedList<>();
    List<Strategy> strategies = new LinkedList<>();

    try {
      BufferedReader br = new BufferedReader(new FileReader(file));

      int numOfStock = Integer.parseInt(br.readLine());

      for (int i = 0; i < numOfStock; i++) {
        String name = br.readLine();
        Map<String, Stock> newStock = new LinkedHashMap<>();
        if (stockPool.containsKey(name)) {
          newStock.put(name, stockPool.get(name));
        } else {
          newStock = new AlphaVantage(name).getData();
          stockPool.putAll(newStock);
        }
        stocksInPort.putAll(newStock);
      }


      int numOfProperty = Integer.parseInt(br.readLine());

      for (int j = 0; j < numOfProperty; j++) {
        //new property, add to list
        String[] property = br.readLine().split(",");

        //time
        IMyCalendar time = new MyCalendar(property[0]);
        //stock
        Stock stock = stocksInPort.get(property[1]);
        //share
        double share = Double.parseDouble(property[2]);
        //fee
        double fee = Double.parseDouble(property[3]);
        properties.add(new Property(time, stock, share, fee));
      }

      int numOfStrategy = Integer.parseInt(br.readLine());

      for (int k = 0; k < numOfStrategy; k++) {
        Strategy strategy = Strategy.retrieve(new File("data/strategies/"
                + br.readLine() + ".txt"));
        strategies.add(strategy);
      }
      br.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalStateException("Operation failed: File not found.\n");
    } catch (IOException e) {
      //e.printStackTrace();
      throw new IllegalStateException("Operation failed: I/O Exception.\n");
    }
    return new Portfolio(stocksInPort, properties, strategies);
  }

}