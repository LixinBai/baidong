package virtualinvestment.model;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.calendar.MyCalendar;

/**
 * This class represent a single stock in a virtual investment system.
 */
public class Stock {
  private String name;
  private Map<String, Double> price;

  /**
   * A constructor for a stock.
   *
   * @param name name of the stock
   */
  public Stock(String name) {
    this.name = name;
    price = new LinkedHashMap<>();
  }

  /**
   * Add a new entry to price table of this stock.
   *
   * @param time time of the price to be added
   * @param p    price of given time
   * @throws IllegalArgumentException if there is already a price for given time
   */
  public void addPrice(IMyCalendar time, double p) throws IllegalArgumentException {
    if (price.containsKey(time.toString())) {
      throw new IllegalArgumentException("Operation failed: This time already has price");
    }
    price.put(time.toString(), p);
  }

  /**
   * Determine if the stock can be bought at the given time. A stock cannot be bought before it has
   * prices.
   *
   * @param time purchase time
   * @return whether the stock can be bought
   */
  boolean ableTobuy(IMyCalendar time) {
    this.getValue(time);
    return true;
  }

  /**
   * Get proper value of a stock ar given time.
   *
   * @param time time with which to find a value
   * @return value at given time
   */
  double getValue(IMyCalendar time) throws IllegalStateException {
    ////delete this part
    if (price.isEmpty()) {
      throw new IllegalStateException("Operation failed: No price found for this stock.\n");
    }
    Iterator<Map.Entry<String, Double>> iterator = price.entrySet().iterator();
    String target = null;
    while (iterator.hasNext()) {
      Map.Entry<String, Double> current = iterator.next();
      if (!(new MyCalendar(current.getKey()).after(time))) {
        target = current.getKey();
        break;
      }
    }
    if (target == null) {
      throw new IllegalStateException("Operation failed: "
              + "No price available before the stock exist.\n");
    }
    return price.get(target);
  }

  /**
   * Get the name of this stock.
   *
   * @return name of this stock
   */
  public String getName() {
    return this.name;
  }

  /**
   * Get shares of this stock.
   * @param time purchase time
   * @param amount amount of dollar
   * @return the number of shares
   */
  public double getShare(IMyCalendar time, double amount) {
    double p = this.getValue(time);
    return amount / p;
  }
}
