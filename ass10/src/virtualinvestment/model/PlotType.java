package virtualinvestment.model;

/**
 * This class represents all possible observation frequency of a plot.
 */
public enum PlotType {
  YEAR, MONTH, DAY,
}
