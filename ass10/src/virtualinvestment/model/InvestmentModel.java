
package virtualinvestment.model;

import java.io.File;
import java.util.HashMap;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.datasource.AlphaVantage;
import virtualinvestment.model.datasource.DataSource;
import virtualinvestment.model.datasource.DataSourceType;
import virtualinvestment.model.datasource.FileSource;
import virtualinvestment.model.strategy.DollarCostAveraging;
import virtualinvestment.model.strategy.Strategy;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;


/**
 * This is an implementation of virtual investment system.
 */
public class InvestmentModel implements VirtualInvestment {
  private Map<String, Stock> stocks;
  private Map<String, Portfolio> portfolios;
  private Map<String, Strategy> strategies;

  /**
   * A constructor for investment system. initially the system contains no stocks and portfolios.
   */
  public InvestmentModel() {
    stocks = new LinkedHashMap<>();
    portfolios = new LinkedHashMap<>();
    strategies = new LinkedHashMap<>();
  }

  @Override
  public void addDataFromInternet(DataSourceType api, String name) throws IllegalStateException {
    if (api == DataSourceType.ALPHAVANTAGE) {
      this.alphaVantageHelper(name);
    } else {
      throw new IllegalStateException("Operation failed: API " + api + " not supported.\n");
    }
  }

  private void alphaVantageHelper(String name) {
    if (stocks.containsKey(name)) {
      throw new IllegalStateException("Operation failed: Stock name is already exist.\n");
    }
    File file = new File("data/stocks/" + name + ".txt");
    DataSource data;
    if (file.exists()) {
      data = new FileSource(name);
    } else {
      data = new AlphaVantage(name);
    }
    HashMap<String, Stock> newData = data.getData();
    stocks.putAll(newData);
  }

  @Override
  public void createPortfolio(String pName) throws IllegalStateException {
    if (portfolios.containsKey(pName)) {
      throw new IllegalStateException("Creation failed: Portfolio name already exists.\n");
    }
    if (isSaved(pName, "data/portfolios")) {
      throw new IllegalStateException("Operation failed: You have already saved " + pName + ".\n");
    }
    Portfolio port = new Portfolio();
    portfolios.put(pName, port);
  }

  @Override
  // new constructor
  public void createPortfolio(String pName, String[] sNames) throws IllegalStateException {
    if (portfolios.containsKey(pName)) {
      throw new IllegalStateException("Creation failed: Portfolio name already exists.\n");
    }
    if (isSaved(pName, "data/portfolios")) {
      throw new IllegalStateException("Operation failed: You have already saved " + pName + ".\n");
    }
    Portfolio port = new Portfolio();
    for (String s : sNames) {
      if (!stocks.containsKey(s)) {
        this.alphaVantageHelper(s);
      }
      port.addStock(stocks.get(s));
    }

    portfolios.put(pName, port);
  }

  private boolean isSaved(String name, String dir) {
    File file = new File(dir);
    if (file.isDirectory()) {
      //judge if there is a strategy with given name
      for (File f : file.listFiles()) {
        if (f.getName() == name + ".txt") {
          return true;
        }
      }
    } else {
      try {
        file.mkdirs();
        return false;
      } catch (SecurityException e) {
        throw new IllegalStateException(" Operation failed: Security problem.\n");
      }
    }
    return false;
  }

  @Override
  //new
  public void addStock(String pName, String sName) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not found.\n");
    }
    Portfolio port = portfolios.get(pName);
    if (!stocks.containsKey(sName)) {
      this.alphaVantageHelper(sName);
    }
    port.addStock(stocks.get(sName));
  }

  @Override
  public String getPurchaseHistory(String pName, IMyCalendar time) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not found.\n");
    }
    return portfolios.get(pName).toString(time);
  }

  @Override
  public void buyStock(String pName, String sName, double shares, double fee, IMyCalendar time)
          throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Portfolio p = portfolios.get(pName);
    if (!stocks.containsKey(sName)) {
      throw new IllegalStateException("Operation failed: Stock does not exist.\n");
    }
    Stock s = stocks.get(sName);
    String close = time.isClose();
    if (close != null) {
      throw new IllegalStateException("Operation failed: This time is " + close + ".\n");
    }
    if (s.ableTobuy(time)) {
      p.buyStock(time, s, shares, fee);
    }
  }

  @Override
  public void invest(String pName, IMyCalendar time, double fee, Map<String, Double> amounts)
          throws IllegalStateException {
    //portfolio should exist
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    //time to buy should not reset
    String close = time.isClose();
    if (close != null) {
      throw new IllegalStateException("Operation failed: This time is " + close + ".\n");
    }
    Portfolio port = portfolios.get(pName);
    HashMap<Stock, Double> newAmounts = addStrategyHelper(amounts, time);
    port.invest(newAmounts, time, fee);
  }

  @Override
  public void createDollarCostAverage(String strategyName, IMyCalendar start, IMyCalendar end,
                                      int interval, double fee, Map<String, Double> weights)
          throws IllegalStateException {
    if (strategies.containsKey(strategyName)) {
      throw new IllegalStateException("Operation failed: Strategy " + strategyName
              + " is already exist in the system.\n");
    }

    if (this.isSaved(strategyName, "data/strategies")) {
      throw new IllegalStateException("Operation failed: You have already saved "
              + strategyName + ".\n");
    }
    HashMap<Stock, Double> newAmounts = addStrategyHelper(weights, start);
    Strategy strategy = new DollarCostAveraging(strategyName, start, end, newAmounts,
            fee, interval);
    strategies.put(strategyName, strategy);
  }

  @Override
  public void retrieveStrategy(String strName) throws IllegalStateException {
    if (strategies.containsKey(strName)) {
      throw new IllegalStateException("Operation failed: Strategy is already in this system.\n");
    } else {
      File file = new File("data/strategies/" + strName + ".txt");
      if (file.exists()) {
        Strategy strategy = Strategy.retrieve(file);
        strategies.put(strName, strategy);
      } else {
        throw new IllegalStateException("Operation failed: Strategy " + strName
                + " does not exist\n.");
      }
    }
  }

  @Override
  public void applyStrategy(String strName, String pName) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    if (!strategies.containsKey(strName)) {
      throw new IllegalStateException("Operation failed: Strategy name does not exist.\n");
    }
    Strategy strategy = strategies.get(strName);
    Portfolio port = portfolios.get(pName);

    //make sure stocks match
    port.addStrategy(strategy);
  }

  @Override
  public void saveStrategy(String strName) throws IllegalStateException {
    if (!strategies.containsKey(strName)) {
      throw new IllegalStateException("Operation failed: Strategy "
              + strName + " does not exist.\n");
    }
    strategies.get(strName).save();
  }

  @Override
  public String viewStrategy(String strName) throws IllegalStateException {
    if (!strategies.containsKey(strName)) {
      throw new IllegalStateException("Operation failed: Strategy does not found.\n");
    }

    return strategies.get(strName).toString();
  }

  @Override
  public String viewAllStrategy() {
    StringBuilder sb = new StringBuilder();
    for (Map.Entry<String, Strategy> entry : strategies.entrySet()) {
      sb.append(entry.getKey()).append(":").append(entry.getValue().toString()).append("\n");
    }
    return sb.toString();
  }

  private HashMap<Stock, Double> addStrategyHelper(Map<String, Double> amounts, IMyCalendar start)
          throws IllegalStateException {
    HashMap<Stock, Double> result = new LinkedHashMap<>();

    Iterator<Map.Entry<String, Double>> entry = amounts.entrySet().iterator();
    while (entry.hasNext()) {
      Map.Entry<String, Double> i = entry.next();
      //make sure all stocks exist
      if (stocks.containsKey(i.getKey())) {
        Stock s = stocks.get(i.getKey());
        if (!s.ableTobuy(start)) {
          throw new IllegalStateException("Operation failed: "
                  + "Time of this strategy is before existence of some stocks.\n");
        }
        result.put(s, i.getValue());
      } else {
        throw new IllegalStateException("Operation failed: Stock "
                + i.getKey() + " does not exist.\n");
      }
    }
    return result;
  }

  @Override
  public void savePortfolio(String pName) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio " + pName
              + " does not exist.\n");
    }
    portfolios.get(pName).save(pName);
  }

  @Override
  public void retrievePortfolio(String pName) throws IllegalStateException {
    if (portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio is already in this system.\n");
    } else {
      File file = new File("data/portfolios/" + pName + ".txt");
      if (file.exists()) {
        //retrieve from file
        Portfolio portfolio = Portfolio.retrieve(file, stocks);
        portfolios.put(pName, portfolio);
      } else {
        throw new IllegalStateException("Operation failed: Portfolio " + pName
                + " never saved.\n");
      }
    }
  }

  @Override
  public String viewPortfolio(String pName) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not found.\n");
    }

    return portfolios.get(pName).getStocks();
  }

  @Override
  public String viewAllPortfolio() {
    StringBuilder sb = new StringBuilder();
    for (Map.Entry<String, Portfolio> entry : portfolios.entrySet()) {
      sb.append(entry.getKey()).append(":").append(entry.getValue().getStocks()).append("\n");
    }
    return sb.toString();
  }

  @Override
  public double getCostBasis(String pName, IMyCalendar time) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Portfolio port = portfolios.get(pName);
    return port.getCostBasis(time);
  }

  @Override
  public double getTotalValue(String pName, IMyCalendar time) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Portfolio port = portfolios.get(pName);
    return port.getTotalValue(time);
  }

  @Override
  public Map<String, Double> plot(String pName, IMyCalendar start, IMyCalendar end,
                                  PlotType type, int interval) {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Map<String, Double> result = new LinkedHashMap<>();
    IMyCalendar temp = new MyCalendar(start.toString());
    while (!temp.after(end)) {
      result.put(temp.toString(), this.getTotalValue(pName, temp));
      switch (type) {
        case YEAR:
          temp.add(YEAR, interval);
          break;
        case MONTH:
          temp.add(MONTH, interval);
          break;
        case DAY:
          temp.add(DATE, interval);
          break;
        default:
          throw new IllegalStateException("Operation failed: Invalid plot type.");
      }
    }
    return result;
  }

  @Override
  public void reset() {
    //remove all data
    File dir = new File("data/stocks");
    if (dir.isDirectory()) {
      File[] files = dir.listFiles();
      if (files != null) {
        for (File file : files) {
          file.delete();
        }
      }
    } else {
      try {
        dir.mkdirs();
      } catch (SecurityException e) {
        throw new IllegalStateException(" Operation failed: Security problem.\n");
      }
    }

  }
}
