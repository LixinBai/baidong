import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import virtualinvestment.calendar.MyCalendar;
import virtualinvestment.model.InvestmentModel;
import virtualinvestment.model.PlotType;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.model.datasource.DataSourceType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VirtualInvestmentTest {

  private VirtualInvestment investment;

  /**
   * Initialize some fields of tests.
   */
  @Before
  public void setUp() {
    investment = new InvestmentModel();

    investment.createPortfolio("Internet");
    investment.createPortfolio("File");
    investment.createPortfolio("Manual");

    investment.addDataFromInternet(DataSourceType.ALPHAVANTAGE, "GOOG");
    investment.addDataFromInternet(DataSourceType.ALPHAVANTAGE, "VZ");
  }

  @Test
  public void addAndBuyStockFromInternet() {
    investment.addStock("Internet", "GOOG");
    investment.buyStock("Internet", "GOOG", 10, 0,
            new MyCalendar("2018-12-4-16"));
    String result = investment.getPurchaseHistory("Internet",
            new MyCalendar("2018-12-4-16"));
    assertEquals("Stock Name: GOOG  Unit Price: 1050.82  Shares: 10.00\n", result);

  }

  @Test
  public void addFromInternetDuplicateStock() {
    //already exist a stock with given name
    try {
      investment.addDataFromInternet(DataSourceType.ALPHAVANTAGE, "GOOG");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock name is already exist.\n", e.getMessage());
    }
  }

  @Test
  public void createPortfolio() {
    //success add a portfolio
    investment.createPortfolio("p1");
    try {
      String name = investment.viewAllPortfolio();
      assertEquals("Internet:\n" + "File:\n" + "Manual:\n" + "p1:\n", name);
    } catch (IllegalStateException e) {
      fail();
    }
  }

  @Test
  public void createPortfolioDuplicateName() {
    //already exist a portfolio with given name
    try {
      investment.createPortfolio("File");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Creation failed: Portfolio name already exists.\n", e.getMessage());
    }
  }

  @Test
  public void createPortfolioWithNames() {
    //use createPortfolio(String pName, String[] sNames)
    String[] names = {"GOOG", "VZ"};
    investment.createPortfolio("Names", names);
    assertEquals("GOOG VZ ", investment.viewPortfolio("Names"));
  }

  @Test
  public void createPortfolioNonExistStock() {
    //the stock to add does not exist
    try {
      String[] names = {"GOOG", "NONEXIST"};
      investment.createPortfolio("Names", names);
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: No price found for NONEXIST.\n", e.getMessage());
    }
  }

  @Test
  public void createPortfolioImportNew() {
    try {
      String[] names = {"GOOG", "AAPL"};
      investment.createPortfolio("Names", names);
      assertEquals("GOOG AAPL ", investment.viewPortfolio("Names"));
    } catch (IllegalStateException e) {
      fail();
    }
  }

  @Test
  public void addStock() {
    //add stock success
    String before = investment.viewPortfolio("File");
    assertEquals("", before);

    investment.addStock("File", "GOOG");
    String after = investment.viewPortfolio("File");
    assertNotEquals(before, after);
    assertEquals("GOOG ", after);
  }

  @Test
  public void addStockDuplicateName() {
    //the stock is already exist in portfolio
    investment.addStock("File", "GOOG");
    String before = investment.viewPortfolio("File");
    try {
      investment.addStock("File", "GOOG");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock already exists.\n", e.getMessage());
      String after = investment.viewPortfolio("File");
      assertEquals(before, after);
    }
  }

  @Test
  public void addStockNonExistName() {
    //non existent portfolio name
    try {
      investment.addStock("NonExist", "GOOG");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Portfolio name does not found.\n", e.getMessage());
    }

    //non existent stock name
    try {
      investment.addStock("File", "NonExist");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: No price found for NonExist.\n", e.getMessage());
    }
  }

  @Test
  public void getPurchaseHistoryOfEmptyPortfolio() {
    //empty portfolio
    assertEquals("This portfolio is empty.\n",
            investment.getPurchaseHistory("File", new MyCalendar("2016-05-25-12")));
  }

  @Test
  public void getPurchaseHistoryBoughtSameTime() {
    //different stocks in a portfolio are bought at the same time, they are listed separately
    investment.addStock("File", "VZ");
    investment.addStock("File", "GOOG");
    investment.buyStock("File", "GOOG", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "VZ", 200, 0,
            new MyCalendar("2016-06-24-12"));
    String result = investment.getPurchaseHistory("File",
            new MyCalendar("2016-06-27-12"));
    assertEquals("Stock Name: GOOG  Unit Price: 675.17  Shares: 17.00\n"
            + "Stock Name: VZ  Unit Price: 54.47  Shares: 200.00\n", result);
  }

  @Test
  public void getPurchaseHistoryBoughtDifferentTime() {
    //user buy the same stock with different cost basis, they can be listed separately
    investment.addStock("File", "GOOG");
    investment.buyStock("File", "GOOG", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "GOOG", 17, 0,
            new MyCalendar("2018-10-2-16"));
    //all history
    String result1 = investment.getPurchaseHistory("File",
            new MyCalendar("2018-10-2-16"));
    assertEquals("Stock Name: GOOG  Unit Price: 1200.11  Shares: 17.00\n"
            + "Stock Name: GOOG  Unit Price: 675.17  Shares: 17.00\n", result1);

    //part of history
    String result2 = investment.getPurchaseHistory("File",
            new MyCalendar("2018-8-2-12"));
    assertEquals("Stock Name: GOOG  Unit Price: 675.17  Shares: 17.00\n", result2);
  }

  @Test
  public void getPurchaseHistorySameCostBasisMerge() {
    investment.addStock("File", "GOOG");
    investment.buyStock("File", "GOOG", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "GOOG", 13, 0,
            new MyCalendar("2016-06-24-09"));
    String result = investment.getPurchaseHistory("File",
            new MyCalendar("2016-06-24-17"));
    assertEquals("Stock Name: GOOG  Unit Price: 675.17  Shares: 30.00\n", result);
  }

  @Test
  public void buyStock() {
    investment.addStock("File", "GOOG");
    investment.buyStock("File", "GOOG", 17, 0,
            new MyCalendar("2016-06-24-09"));
    String result1 = investment.getPurchaseHistory("File",
            new MyCalendar("2016-06-25-09"));
    assertEquals("Stock Name: GOOG  Unit Price: 675.17  Shares: 17.00\n", result1);

    investment.buyStock("File", "GOOG", 110, 0,
            new MyCalendar("2016-07-13-16"));
    String result2 = investment.getPurchaseHistory("File",
            new MyCalendar("2016-08-24-09"));
    assertEquals("Stock Name: GOOG  Unit Price: 675.17  Shares: 17.00\n"
            + "Stock Name: GOOG  Unit Price: 716.98  Shares: 110.00\n", result2);
  }

  @Test
  public void buyStockWithCommissionFee() {
    investment.addStock("File", "GOOG");
    investment.buyStock("File", "GOOG", 17, 4,
            new MyCalendar("2014-06-24-09"));

    String result1 = investment.getPurchaseHistory("File",
            new MyCalendar("2016-06-25-09"));
    assertEquals("Stock Name: GOOG  Unit Price: 565.19  Shares: 17.00\n", result1);

    double cost = investment.getCostBasis("File", new MyCalendar("2016-06-25-09"));
    assertEquals(17 * 565.19 + 4, cost, 0.01);
    double value = investment.getTotalValue("File", new MyCalendar("2014-06-25-09"));
    assertEquals(565.26 * 17, value, 0.01);
  }

  @Test
  public void buyNonExistPortfolio() {
    //given portfolio name does not exist
    try {
      investment.buyStock("nonExistPortfolio", "GOOG", 15, 0,
              new MyCalendar("2016-05-25-12"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Portfolio name does not exist.\n", e.getMessage());
    }
  }

  @Test
  public void buyNonExistStock() {
    //given stock does not exist
    try {
      investment.buyStock("Internet", "MNL", 15, 0,
              new MyCalendar("2016-05-25-12"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock does not exist.\n", e.getMessage());
    }
  }

  @Test
  public void buyStockNotBelongToPort() {
    //the stock does not belong to given portfolio
    try {
      investment.buyStock("Internet", "GOOG", 15, 0,
              new MyCalendar("2016-05-25-16"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock is not in this portfolio.\n", e.getMessage());
    }
  }

  @Test
  public void buyCloseTime() {
    //buying time is weekend
    try {
      investment.addStock("File", "GOOG");
      investment.buyStock("File", "GOOG", 2, 0,
              new MyCalendar("2018-11-11-16"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: This time is weekend.\n", e.getMessage());
    }

    //buying time is after-hours
    try {
      investment.buyStock("File", "GOOG", 2, 0,
              new MyCalendar("2018-11-1-1"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: This time is before 9 am.\n", e.getMessage());
    }

    //buying time is before this stock exist
    try {
      investment.buyStock("File", "GOOG", 2, 0,
              new MyCalendar("1999-11-1-09"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: No price available before the stock exist.\n",
              e.getMessage());
    }
  }

  @Test
  public void investNonExistPort() {
    //portfolio does not exist
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 100.0);
    try {
      investment.invest("NonExist",
              new MyCalendar("2014-4-1-09"), 5.0, weights);
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Portfolio name does not exist.\n", e.getMessage());
    }
  }

  @Test
  public void investClose() {
    // stock market is reset at invest time
    investment.addStock("Internet", "GOOG");
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 100.0);
    try {
      investment.invest("Internet",
              new MyCalendar("2014-4-1-18"), 5.0, weights);
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: This time is after 4 pm.\n", e.getMessage());
    }
  }

  @Test
  public void investGetClosestPrice() {
    //price data missed at buying time
    investment.addStock("Internet", "GOOG");
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 100.0);
    try {
      investment.invest("Internet",
              new MyCalendar("2014-4-1-12"), 5.0, weights);
      assertEquals("Stock Name: GOOG  Unit Price: 558.71  Shares: "
                      + String.format("%.2f", 100 / 558.71) + "\n",
              investment.getPurchaseHistory("Internet",
                      new MyCalendar("2014-4-1-16")));
    } catch (IllegalStateException e) {
      fail();
    }
  }

  @Test
  public void dollarCostAveraging() {
    //apply strategy
    //investment.addDataFromFile("VZ", new File("ANOTHER.txt"));
    investment.addStock("Internet", "GOOG");
    investment.addStock("Internet", "VZ");
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 40.0);
    weights.put("VZ", 60.0);
    investment.createDollarCostAverage("Internet", new MyCalendar("2014-4-1-16"),
             new MyCalendar("2014-4-6-18"), 2, 5.0, weights);
    investment.applyStrategy("Internet", "Internet");
    //choose next available business day
    //first day
    String result1 = investment.getPurchaseHistory("Internet",
            new MyCalendar("2014-4-1-16"));
    assertEquals("Stock Name: GOOG  Unit Price: " + 567.16
            + "  Shares: " + String.format("%.2f", 100 * 0.4 / 567.16) + "\n"
            + "Stock Name: VZ  Unit Price: " + 47.75
            + "  Shares: " + String.format("%.2f", 100 * 0.6 / 47.75) + "\n", result1);
    double cost1 = investment.getCostBasis("Internet", new MyCalendar("2014-4-1-16"));
    assertEquals(100 + 10, cost1, 0.01);
    double value1 = investment.getTotalValue("Internet",
            new MyCalendar("2014-4-1-16"));
    assertEquals((100 * 0.4 / 567.16) * 567.16 + (100 * 0.6 / 47.75) * 47.75,
            value1, 0.01);

    //before buy
    String result2 = investment.getPurchaseHistory("Internet",
            new MyCalendar("2014-4-1-09"));
    assertEquals("This portfolio is empty.\n", result2);
    double cost2 = investment.getCostBasis("Internet", new MyCalendar("2014-4-1-09"));
    assertEquals(0.0, cost2, 0.01);
    double value2 = investment.getTotalValue("Internet",
            new MyCalendar("2014-4-1-09"));
    assertEquals(0.0, value2, 0.01);

    //after end
    //getPurchaseHistory
    String result3 = investment.getPurchaseHistory("Internet",
            new MyCalendar("2014-4-7-09"));
    assertEquals("Stock Name: GOOG  Unit Price: " + 538.15
            + "  Shares: " + String.format("%.2f", 100 * 0.4 / 538.15) + "\n"
            + "Stock Name: GOOG  Unit Price: " + 567.16
            + "  Shares: " + String.format("%.2f", 100 * 0.4 / 567.16) + "\n"
            + "Stock Name: GOOG  Unit Price: " + 569.74
            + "  Shares: " + String.format("%.2f", 100 * 0.4 / 569.74) + "\n"
            + "Stock Name: VZ  Unit Price: " + 47.75
            + "  Shares: " + String.format("%.2f", 100 * 0.6 / 47.75) + "\n"
            + "Stock Name: VZ  Unit Price: " + 48.12
            + "  Shares: " + String.format("%.2f", 100 * 2 * 0.6 / 48.12) + "\n", result3);
    //getCostBasis
    double cost3 = investment.getCostBasis("Internet", new MyCalendar("2014-4-7-09"));
    assertEquals(100.0 * 3 + 5.0 * 6, cost3, 0.01);
    //getTotalValue
    double value3 = investment.getTotalValue("Internet",
            new MyCalendar("2014-4-7-09"));
    assertEquals(((100 * 0.4 / 538.15) + (100 * 0.4 / 567.16)
            + (100 * 0.4 / 569.74)) * 540.74 + ((100 * 0.6 / 47.75)
            + (100 * 2 * 0.6 / 48.12)) * 47.7, value3, 0.01);
  }

  @Test
  public void addStockApplyAnotherDCA() {
    //apply a strategy
    //investment.addDataFromFile("VZ", new File("ANOTHER.txt"));
    investment.addStock("Internet", "GOOG");
    investment.addStock("Internet", "VZ");
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 60.0);
    weights.put("VZ", 40.0);
    investment.createDollarCostAverage("Internet", new MyCalendar("2014-4-1-16"),
            new MyCalendar("2018-4-1-09"), 2, 5.0, weights);
    investment.applyStrategy("Internet", "Internet");

    //then add a new stock to port
    //investment.addDataFromFile("ANOTHER", new File("ANOTHER.txt"));
    investment.addStock("Internet", "AAPL");

    //apply a new strategy
    //double[] anotherWeights = {0.1, 0.2, 0.7};
    Map<String, Double> anotherWeights = new TreeMap<>();
    anotherWeights.put("GOOG", 20.0);
    anotherWeights.put("VZ", 70.0);
    anotherWeights.put("AAPL", 10.0);
    investment.createDollarCostAverage("Internet1", new MyCalendar("2014-4-1-16"),
            new MyCalendar("2018-4-1-09"), 3, 5.0, anotherWeights);
    investment.applyStrategy("Internet1", "Internet");

    //get purchase history
    String result = investment.getPurchaseHistory("Internet",
            new MyCalendar("2014-4-7-09"));
    assertEquals("Stock Name: AAPL  Unit Price: 531.82  Shares: "
            + String.format("%.2f", 10 / 531.82) + "\n"
            + "Stock Name: AAPL  Unit Price: 541.65  Shares: "
            + String.format("%.2f", 10 / 541.65) + "\n"
            + "Stock Name: GOOG  Unit Price: 538.15  Shares: "
            + String.format("%.2f", 60 / 538.15) + "\n"
            + "Stock Name: GOOG  Unit Price: 543.14  Shares: "
            + String.format("%.2f", 20 / 543.14) + "\n"
            + "Stock Name: GOOG  Unit Price: 567.16  Shares: "
            + String.format("%.2f", (60 / 567.16 + 20 / 567.16)) + "\n"
            + "Stock Name: GOOG  Unit Price: 569.74  Shares: "
            + String.format("%.2f", 60 / 569.74) + "\n"
            + "Stock Name: VZ  Unit Price: 47.75  Shares: "
            + String.format("%.2f", (40 + 70) / 47.75) + "\n"
            + "Stock Name: VZ  Unit Price: 48.04  Shares: "
            + String.format("%.2f", 70 / 48.04) + "\n"
            + "Stock Name: VZ  Unit Price: 48.12  Shares: "
            + String.format("%.2f", (40 + 40) / 48.12) + "\n", result);
    //getCostBasis
    double cost1 = investment.getCostBasis("Internet", new MyCalendar("2014-4-7-09"));
    assertEquals(100.0 * 5 + 5.0 * (2 * 3 + 3 * 2), cost1, 0.01);
    //getTotalValue
    double value1 = investment.getTotalValue("Internet",
            new MyCalendar("2014-4-7-09"));
    double expect = ((10 / 531.82) + (10 / 541.65)) * 528.02
            + ((60 / 538.15) + (20 / 543.14) + (60 / 567.16 + 20 / 567.16) + (60 / 569.74)) * 540.74
            + (((40 + 70) / 47.75) + (70 / 48.04) + (40 + 40) / 48.12) * 47.7;
    assertEquals(expect, value1, 0.01);
  }

  @Test
  public void comboInvestment() {
    //combination of investment
    //investment.addDataFromFile("VZ", new File("ANOTHER.txt"));
    investment.addStock("Internet", "VZ");
    investment.addStock("Internet", "GOOG");

    //buyStock
    investment.buyStock("Internet", "GOOG", 10, 4,
            new MyCalendar("2014-05-14-11"));

    //invest
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 40.0);
    weights.put("VZ", 60.0);
    investment.invest("Internet", new MyCalendar("2014-04-09-16"), 2.1, weights);

    //strategy
    investment.createDollarCostAverage("Internet",
            new MyCalendar("2014-04-04-16"),
            new MyCalendar("2014-04-08-9"), 1, 3.3, weights);
    investment.applyStrategy("Internet", "Internet");
    //viewPurchaseHistory
    String result = investment.getPurchaseHistory("Internet",
            new MyCalendar("2014-06-14-09"));
    assertEquals("Stock Name: GOOG  Unit Price: " + 533.0
            + "  Shares: " + String.format("%.2f", 10.00) + "\n"
            + "Stock Name: GOOG  Unit Price: " + 538.15
            + "  Shares: " + String.format("%.2f", 0.07) + "\n"
            + "Stock Name: GOOG  Unit Price: " + 543.14
            + "  Shares: " + String.format("%.2f", 0.07) + "\n"
            + "Stock Name: GOOG  Unit Price: " + 564.14
            + "  Shares: " + String.format("%.2f", 0.07) + "\n"
            + "Stock Name: VZ  Unit Price: " + 47.98
            + "  Shares: " + String.format("%.2f", 1.25) + "\n"
            + "Stock Name: VZ  Unit Price: " + 48.04
            + "  Shares: " + String.format("%.2f", 1.25) + "\n"
            + "Stock Name: VZ  Unit Price: " + 48.12
            + "  Shares: " + String.format("%.2f", 1.25) + "\n", result);
  }

  @Test
  public void createDollarCostAverage() {
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 40.0);
    weights.put("VZ", 60.0);
    investment.createDollarCostAverage("test", new MyCalendar("2015-12-25-3"),
            new MyCalendar("2016-12-25-12"), 2, 4.5, weights);
    String strategies = investment.viewAllStrategy();
    assertEquals("test:From: 2015-12-25-3  To: 2016-12-25-12  "
            + "Interval: 2  Commission Fee: 4.5\n"
            + "Stock: GOOG  Amount of Money: 40.0\n"
            + "Stock: VZ  Amount of Money: 60.0\n", strategies);
  }

  @Test
  public void stage1SaveStrategy() {
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 40.0);
    weights.put("VZ", 60.0);
    investment.createDollarCostAverage("myStrategy",
            new MyCalendar("2016-3-14-16"), new MyCalendar("2017-5-12-16"),
            3, 2, weights);
    investment.saveStrategy("myStrategy");
    File file = new File("data/strategies/myStrategy.txt");
    assertTrue(file.exists());
  }

  @Test
  public void stage2RetrieveStrategy() {
    investment.retrieveStrategy("myStrategy");
    String detail = investment.viewStrategy("myStrategy");
    assertEquals("From: 2016-3-14-16  To: 2017-5-12-16  Interval: 3  Commission Fee: 2.0\n"
                    + "Stock: GOOG  Amount of Money: 40.0\n"
                    + "Stock: VZ  Amount of Money: 60.0\n", detail);

    //apply it
    String[] names = {"GOOG", "VZ"};
    investment.createPortfolio("apply", names);
    investment.applyStrategy("myStrategy","apply");
    String result = investment.getPurchaseHistory("apply",
            new MyCalendar("2016-3-15-09"));
    assertEquals("Stock Name: GOOG  Unit Price: 730.49  Shares: "
            + String.format("%.2f", 40 / 730.49) + "\n"
            + "Stock Name: VZ  Unit Price: 52.54  Shares: "
            + String.format("%.2f", 60 / 52.54) + "\n", result);
  }

  @Test
  public void viewAllStrategy() {
    //empty
    String before = investment.viewAllStrategy();
    System.out.println(before);

    //add new
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 40.0);
    weights.put("VZ", 60.0);
    investment.createDollarCostAverage("test", new MyCalendar("2015-12-25-3"),
            new MyCalendar("2016-12-25-12"), 2, 4.5, weights);
    String strategies = investment.viewAllStrategy();
    assertEquals("test:From: 2015-12-25-3  To: 2016-12-25-12  "
                    + "Interval: 2  Commission Fee: 4.5\n"
                    + "Stock: GOOG  Amount of Money: 40.0\n"
                    + "Stock: VZ  Amount of Money: 60.0\n", strategies);
  }

  @Test
  public void viewStrategy() {
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 20.0);
    weights.put("VZ", 80.0);
    investment.createDollarCostAverage("test", new MyCalendar("2015-12-25-3"),
            new MyCalendar("2016-12-25-12"), 2, 4.5, weights);
    String strategies = investment.viewAllStrategy();
    assertEquals("test:From: 2015-12-25-3  To: 2016-12-25-12  "
            + "Interval: 2  Commission Fee: 4.5\n"
            + "Stock: GOOG  Amount of Money: 20.0\n"
            + "Stock: VZ  Amount of Money: 80.0\n", strategies);
  }

  @Test
  public void stage1SaveEmptyPort() {
    investment.createPortfolio("myPort");
    investment.savePortfolio("myPort");
    File file = new File("data/portfolios/myPort.txt");
    assertTrue(file.exists());
  }

  @Test
  public void stage2RetrieveEmptyPort() {
    investment.retrievePortfolio("myPort");
    String portfolios = investment.viewAllPortfolio();
    assertEquals("Internet:\n"
            + "File:\n"
            + "Manual:\n"
            + "myPort:\n", portfolios);
    String stocks = investment.viewPortfolio("myPort");
    assertEquals("", stocks);
    String history = investment.getPurchaseHistory("myPort",
            new MyCalendar("2016-12-13-09"));
    assertEquals("This portfolio is empty.\n", history);
  }

  @Test
  public void stage1SaveNonEmptyPortfolio() {
    investment.createPortfolio("nonEmpty");
    investment.addStock("nonEmpty", "GOOG");
    investment.addStock("nonEmpty", "VZ");
    investment.buyStock("nonEmpty", "GOOG", 13, 2.5,
            new MyCalendar("2018-11-29-13"));
    Map<String, Double> weights = new TreeMap<>();
    weights.put("GOOG", 40.0);
    weights.put("VZ", 60.0);
    investment.createDollarCostAverage("myStrategy1",
            new MyCalendar("2016-3-14-16"), new MyCalendar("2017-5-12-16"),
            3, 2, weights);
    investment.applyStrategy("myStrategy1", "nonEmpty");
    investment.savePortfolio("nonEmpty");
    File strategy = new File("data/strategies/myStrategy1.txt");
    File port = new File("data/portfolios/nonEmpty.txt");
    assertTrue(strategy.exists());
    assertTrue(port.exists());
  }

  @Test
  public void stage2RetrieveNonEmptyPortfolio() {
    investment.retrievePortfolio("nonEmpty");
    String stocks = investment.viewPortfolio("nonEmpty");
    assertEquals("GOOG VZ ", stocks);
  }

  @Test
  public void getCostBasis() {
    investment.addStock("File", "GOOG");
    investment.addStock("File", "VZ");
    investment.buyStock("File", "GOOG", 17, 2,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "VZ", 200, 3,
            new MyCalendar("2016-05-25-12"));
    investment.buyStock("File", "GOOG", 200, 5,
            new MyCalendar("2016-10-12-16"));
    // a time before buying
    double result1 = investment.getCostBasis("File",
            new MyCalendar("2016-04-24-08"));
    assertEquals(0.0, result1, 0);


    //time equal buying time
    double result2 = investment.getCostBasis("File",
            new MyCalendar("2016-06-24-09"));
    assertEquals(675.17 * 17 + 49.63 * 200 + 2 + 3, result2, 0);

    //time after buying time
    double result3 = investment.getCostBasis("File",
            new MyCalendar("2016-12-28-11"));
    assertEquals(675.1700 * 17 + 49.63 * 200 + 786.1400 * 200 + 2 + 3 + 5,
            result3, 0);

  }

  @Test
  public void getTotalValue() {
    investment.addStock("File", "GOOG");
    investment.buyStock("File", "GOOG", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "GOOG", 200, 0,
            new MyCalendar("2016-10-12-16"));

    //a time before buy
    double result1 = investment.getTotalValue("File", new MyCalendar("2016-05-24-07"));
    assertEquals(0.0, result1, 0);

    //time after to buying time
    double result2 = investment.getTotalValue("File", new MyCalendar("2016-06-24-09"));
    assertEquals(675.1700 * 17, result2, 0);

    //time after buying time
    double result3 = investment.getTotalValue("File", new MyCalendar("2016-10-13-16"));
    assertNotEquals(result2, result3);
    assertEquals(778.1900 * (200 + 17), result3, 0);
  }

  @Test
  public void stage3plot() {
    investment.retrievePortfolio("nonEmpty");
    Map<String, Double> map = investment.plot("nonEmpty",
            new MyCalendar("2018-11-29-14"), new MyCalendar("2018-12-25-14"),
            PlotType.DAY, 25);

    Map<String, Double> compare = new LinkedHashMap<>();
    compare.put("2018-11-29-14", 31305.55);
    compare.put("2018-12-24-14", 30581.19);
    assertEquals(compare.size(), map.size());
    for (Map.Entry<String, Double> entry : map.entrySet()) {
      assertEquals(compare.get(entry.getKey()), entry.getValue(), 0.01);
    }
  }

}