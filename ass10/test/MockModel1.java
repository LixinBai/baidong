import java.util.LinkedHashMap;
import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.model.PlotType;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.model.datasource.DataSourceType;

/**
 * This is a mock model to test the controller.
 */
public class MockModel1 implements VirtualInvestment {
  private final StringBuilder log;
  private final double uniqueDouble;

  /**
   * Create a mock model.
   *
   * @param log          the log to record operations
   * @param uniqueDouble the amount to return
   */
  public MockModel1(StringBuilder log, double uniqueDouble) {
    this.log = log;
    this.uniqueDouble = uniqueDouble;
  }


  @Override
  public void reset() {
    // be tested in model
  }

  @Override
  public void addDataFromInternet(DataSourceType api, String sName) throws IllegalStateException {
    log.append("Import stock").append(sName).append(" from ").append(api.toString()).append(".\n");
  }

  @Override
  public void createPortfolio(String pName) throws IllegalStateException {
    log.append("Create an empty portfolio named ").append(pName).append(".\n");
  }

  @Override
  public void createPortfolio(String pName, String[] sNames) throws IllegalStateException {
    log.append("Create a portfolio named ").append(pName).append(" with stock ");
    for (String s : sNames) {
      log.append(s).append(" ");
    }
    log.append("in it.\n");
  }


  @Override
  public void savePortfolio(String pName) {
    log.append("Save portfolio ").append(pName).append(".\n");
  }

  @Override
  public void retrievePortfolio(String pName) {
    log.append("Retrieve portfolio ").append(pName).append(".\n");
  }

  @Override
  public void addStock(String pName, String sName) throws IllegalStateException {
    log.append("Add stock ").append(sName).append(" to portfolio ").append(pName).append(".\n");
  }

  @Override
  public String viewPortfolio(String pName) throws IllegalStateException {
    log.append("View portfolio ").append(pName).append(".\n");
    return "stocks in portfolio " + pName;
  }

  @Override
  public String viewAllPortfolio() {
    log.append("View all portfolios.\n");
    return "all portfolios\n";
  }

  @Override
  public String getPurchaseHistory(String pName, IMyCalendar date) throws IllegalStateException {
    log.append("Get purchase history of portfolio ").append(pName).append(" at ")
            .append(date.toString()).append(".\n");
    return "Purchase history of " + pName + " at " + date.toString() + ".\n";
  }

  @Override
  public void buyStock(String pName, String sName, double shares, double fee, IMyCalendar date) {
    log.append("Buy ").append(String.format("%.2f", shares)).append(" shares of ").append(sName)
            .append(" in portfolio ").append(pName).append(" at ").append(date.toString())
            .append(" with commission fee of ").append(String.format("%.2f", fee)).append(".\n");
  }

  @Override
  public void invest(String pName, IMyCalendar time, double fee, Map<String, Double> weights)
          throws IllegalStateException {
    // be tested in mock model 3
  }

  @Override
  public void createDollarCostAverage(String strName, IMyCalendar start, IMyCalendar end,
                                      int interval, double fee, Map<String, Double> amounts) {
    log.append("Create a dollar-cost averaging strategy ").append(strName).append(" from ")
            .append(start.toString());
    if (end == null) {
      log.append(" and ongoing");
    } else {
      log.append(" to ").append(end.toString());
    }
    log.append(" with commission fee of ").append((String.format("%.2f", fee))).append(".\n");
    for (Map.Entry<String, Double> entry : amounts.entrySet()) {
      log.append(entry.getKey()).append(":")
              .append((String.format("%.2f", entry.getValue()))).append(" ");
    }
    log.append("\n");
  }

  @Override
  public void saveStrategy(String strName) {
    log.append("Save strategy ").append(strName).append(".\n");
  }

  @Override
  public void retrieveStrategy(String strName) {
    log.append("Retrieve strategy ").append(strName).append(".\n");
  }

  @Override
  public void applyStrategy(String strName, String pName) {
    log.append("Apply strategy ").append(strName).append(" to portfolio ")
            .append(pName).append(".\n");
  }

  @Override
  public String viewStrategy(String strName) {
    log.append("View strategy ").append(strName).append(".\n");
    return "Strategy " + strName;
  }

  @Override
  public String viewAllStrategy() {
    log.append("View all strategy.\n");
    return "all strategies\n";
  }

  @Override
  public double getCostBasis(String pName, IMyCalendar date) throws IllegalStateException {
    log.append("Get cost basis of portfolio ").append(pName).append(" at ")
            .append(date.toString()).append(".\n");
    return uniqueDouble;
  }

  @Override
  public double getTotalValue(String pName, IMyCalendar date) throws IllegalStateException {
    log.append("Get total value of portfolio ").append(pName).append(" at ")
            .append(date.toString()).append(".\n");
    return uniqueDouble;
  }

  @Override
  public Map<String, Double> plot(String pName, IMyCalendar start, IMyCalendar end, PlotType type,
                                  int interval) throws IllegalStateException {
    log.append("Plot portfolio ").append(pName).append(" from ").append(start.toString())
            .append(" to ").append(end.toString()).append(" every ").append(interval).append(" ")
            .append(type.toString()).append("s.\n");
    return new LinkedHashMap<>();
  }

}
