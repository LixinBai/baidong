import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;

import virtualinvestment.controller.CBController;
import virtualinvestment.controller.IController;
import virtualinvestment.model.InvestmentModel;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.view.CBView;
import virtualinvestment.view.IView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ControllerTest {
  private Reader in;
  private StringBuffer out;
  private StringBuilder log;
  private IController controller;

  /**
   * A helper method which will run a virtual investment model.
   *
   * @param mock the model to run
   */
  private void run(ModelName mock) {
    VirtualInvestment model;
    log = new StringBuilder();
    out = new StringBuffer();
    IView view = new CBView(in, out);
    switch (mock) {
      case ONE:
        model = new MockModel1(log, 12.23);
        break;
      case TWO:
        model = new MockModel2();
        break;
      case THREE:
        model = new MockModel3(log, 3, true);
        break;
      case FOUR:
        model = new MockModel3(log, 3, false);
        break;
      case TRUE:
        model = new InvestmentModel();
        break;
      default:
        model = null;
    }
    controller = new CBController(model);
    controller.setView(view);
    controller.start();
  }

  @Test
  public void testNoViewException() {
    try {
      controller = new CBController(new InvestmentModel());
      controller.setView(null);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("View not found.\n", e.getMessage());
    }
  }

  @Test
  public void testNoModelException() {
    try {
      in = new StringReader("");
      out = new StringBuffer();
      controller = new CBController(null);
      controller.start();
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Model not found.\n", e.getMessage());
    }
  }

  @Test
  public void testImportStock() {
    in = new StringReader("importStock\nAlpha Vantage\nAAPL");
    run(ModelName.ONE);
    assertEquals("Import stockAAPL from ALPHAVANTAGE.\n", log.toString());
    assertEquals("API(Alpha Vantage): stock name: Import stock AAPL successfully.\n",
            out.toString());
  }

  @Test
  public void testImportStockFail() {
    in = new StringReader("importStock\nAlpha Vantage\nAAPL");
    run(ModelName.TWO);
    assertEquals("API(Alpha Vantage): stock name: "
            + "Importing stock failed message.\n", out.toString());
  }

  @Test
  public void testSavePortfolio() {
    in = new StringReader("savePort\naPort");
    run(ModelName.ONE);
    assertEquals("Save portfolio aPort.\n", log.toString());
    assertEquals("portfolio name: Portfolio aPort saved successfully.\n",
            out.toString());
  }

  @Test
  public void testSavePortfolioFail() {
    in = new StringReader("savePort\naPort");
    run(ModelName.TWO);
    assertEquals("portfolio name: Saving portfolio failed message.\n", out.toString());
  }

  @Test
  public void testRetrievePortfolio() {
    in = new StringReader("retrievePort\naPort");
    run(ModelName.ONE);
    assertEquals("Retrieve portfolio aPort.\n", log.toString());
    assertEquals("portfolio name: Portfolio aPort retrieved successfully.\n",
            out.toString());
  }

  @Test
  public void testRetrievePortfolioFail() {
    in = new StringReader("retrievePort\naPort");
    run(ModelName.TWO);
    assertEquals("portfolio name: Retrieving nonempty portfolio failed message.\n",
            out.toString());
  }


  @Test
  public void testCreateEmptyPort() {
    in = new StringReader("createPort\naPort\n ");
    run(ModelName.ONE);
    assertEquals("Create an empty portfolio named aPort.\n", log.toString());
    assertEquals("portfolio name: stocks to add (separated by \",\"), or \" \" if empty: "
            + "Portfolio aPort created successfully.\n", out.toString());
  }

  @Test
  public void testCreateEmptyPortFail() {
    in = new StringReader("createPort\naPort\n ");
    run(ModelName.TWO);
    assertEquals("portfolio name: stocks to add (separated by \",\"), or \" \" if empty: "
                    + "Creating empty portfolio failed message.\n",
            out.toString());
  }

  @Test
  public void testCreateNonemptyPort() {
    in = new StringReader("createPort\naPort\nAAPL,GOOG");
    run(ModelName.ONE);
    assertEquals("Create a portfolio named aPort with stock AAPL GOOG in it.\n",
            log.toString());
    assertEquals("portfolio name: stocks to add (separated by \",\"), or \" \" if empty: "
            + "Portfolio aPort created successfully.\n", out.toString());
  }

  @Test
  public void testCreateNonemptyPortFail() {
    in = new StringReader("createPort\naPort\nAAPL");
    run(ModelName.TWO);
    assertEquals("portfolio name: stocks to add (separated by \",\"), or \" \" if empty: "
            + "Creating nonempty portfolio failed message.\n", out.toString());
  }

  @Test
  public void testAddStock() {
    in = new StringReader("addStock\naPort\nAAPL");
    run(ModelName.ONE);
    assertEquals("Add stock AAPL to portfolio aPort.\n", log.toString());
    assertEquals("portfolio name: stock name: Add stock AAPL successfully.\n",
            out.toString());
  }

  @Test
  public void testAddStockFailed() {
    in = new StringReader("addStock\naPort\nAAPL");
    run(ModelName.TWO);
    assertEquals("portfolio name: stock name: "
            + "Adding stock failed message.\n", out.toString());
  }

  @Test
  public void testViewPort() {
    in = new StringReader("viewPort\naPort");
    run(ModelName.ONE);
    assertEquals("View portfolio aPort.\n", log.toString());
    assertEquals("portfolio name: Portfolio "
            + "aPort contains: stocks in portfolio aPort.\n", out.toString());
  }

  @Test
  public void testViewEmptyPort() {
    in = new StringReader("viewPort\naPort");
    log = new StringBuilder();
    out = new StringBuffer();
    VirtualInvestment model = new MockModel3(log, 0, true);
    controller = new CBController(model);
    controller.setView(new CBView(in, out));
    controller.start();
    assertEquals("portfolio name: This portfolio is empty.\n", out.toString());
  }

  @Test
  public void testViewPortFail() {
    in = new StringReader("viewPort\naPort");
    run(ModelName.TWO);
    assertEquals("portfolio name: Viewing portfolio failed message.\n", out.toString());
  }

  @Test
  public void testViewAllPort() {
    in = new StringReader("viewAllPort");
    run(ModelName.ONE);
    assertEquals("View all portfolios.\n", log.toString());
    assertEquals("all portfolios\n", out.toString());
  }

  @Test
  public void testViewAllPortFail() {
    in = new StringReader("viewAllPort");
    run(ModelName.TWO);
    assertEquals("Viewing all portfolios failed message.\n", out.toString());
  }

  @Test
  public void testGetPurchaseHistory() {
    in = new StringReader("getHistory\naPort\n2018-10-1-9");
    run(ModelName.ONE);
    assertEquals("Get purchase history of portfolio aPort at 2018-10-1-9.\n",
            log.toString());
    assertEquals("portfolio name: time to get purchase history(yyyy-mm-dd-hh): "
            + "Purchase history of aPort at 2018-10-1-9.\n", out.toString());
  }

  @Test
  public void testGetPurchaseHistoryFail() {
    in = new StringReader("getHistory\naPort\n2018-10-1-9");
    run(ModelName.TWO);
    assertEquals("portfolio name: time to get purchase history(yyyy-mm-dd-hh): "
            + "Getting purchase history of portfolio failed message.\n", out.toString());
  }

  @Test
  public void testGetPurchaseHistoryBadTime() {
    Reader[] inputs = {new StringReader("getHistory\naPort\nbadInput"),
                       new StringReader("getHistory\naPort\n2099-1-1-9")};
    String common = "portfolio name: time to get purchase history(yyyy-mm-dd-hh): "
            + "Operation failed: ";
    String[] fail = {"Invalid time format.\n",
                     "Time should be earlier than current time.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testBuyStock() {
    in = new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\n100\n5");
    run(ModelName.ONE);
    assertEquals("Buy 100.00 shares of AAPL in portfolio aPort at 2018-10-1-14 "
            + "with commission fee of 5.00.\n", log.toString());
    assertEquals("time to buy(yyyy-mm-dd-hh): portfolio name: stock name: shares to buy: "
            + "commission fee: Buy successfully.\n", out.toString());
  }

  @Test
  public void testBuyStockFail() {
    in = new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\n100\n5");
    run(ModelName.TWO);
    assertEquals("time to buy(yyyy-mm-dd-hh): portfolio name: stock name: shares to buy: "
            + "commission fee: Buying stock failed message.\n", out.toString());
  }


  @Test
  public void testBuyStockBadTime() {
    Reader[] inputs = {new StringReader("buyStock\nbadInput"),
                       new StringReader("buyStock\n2099-1-1-14")};
    String common = "time to buy(yyyy-mm-dd-hh): ";
    String[] fail = {"Operation failed: Invalid time format.\n",
                     "Operation failed: Time should be earlier than current time.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testBuyStockBadShares() {
    Reader[] inputs = {new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\nbadInput"),
                       new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\n-1"),
                       new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\n0")};
    String common = "time to buy(yyyy-mm-dd-hh): portfolio name: stock name: "
            + "shares to buy: Operation failed: ";
    String[] fail = {"Invalid number of shares.\n",
                     "Number of shares should be positive.\n",
                     "Number of shares should be positive.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testBuyStockBadFee() {
    Reader[] inputs = {new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\n100\nbadInput"),
                       new StringReader("buyStock\n2018-10-1-14\naPort\nAAPL\n100\n-1")};
    String common = "time to buy(yyyy-mm-dd-hh): portfolio name: stock name: shares to buy: "
            + "commission fee: Operation failed: ";
    String[] fail = {"Invalid commission fee.\n",
                     "Commission fee should be no-negative.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testInvest() {
    in = new StringReader("invest\naPort\n2018-10-1-14\n1000\n0.5,0.5,0\n5");
    run(ModelName.THREE);
    assertEquals("Invest in portfolio aPort with amount of "
                    + "Stock1:500.00 Stock2:500.00 Stock3:0.00 "
                    + "with commission fee of 5.00.\n",
            log.toString());
    assertEquals("portfolio name: time to invest(yyyy-mm-dd-hh): "
            + "amount of dollar to invest: \"equal\" or weights of "
            + "Stock1 Stock2 Stock3 (separated by \",\", add up to 1): commission fee: "
            + "Invest successfully.\n", out.toString());
  }

  @Test
  public void testInvestEqualWeights() {
    in = new StringReader("invest\naPort\n2018-10-1-14\n1000\nequal\n0");
    run(ModelName.THREE);
    assertEquals("Invest in portfolio aPort with amount of "
                    + "Stock1:333.33 Stock2:333.33 Stock3:333.33 "
                    + "with commission fee of 0.00.\n",
            log.toString());
    assertEquals("portfolio name: time to invest(yyyy-mm-dd-hh): "
            + "amount of dollar to invest: \"equal\" or weights of "
            + "Stock1 Stock2 Stock3 (separated by \",\", add up to 1): commission fee: "
            + "Invest successfully.\n", out.toString());
  }

  @Test
  public void testInvestEmptyPort() {
    in = new StringReader("invest\naPort");
    out = new StringBuffer();
    controller = new CBController(new MockModel3(log, 0, true));
    controller.setView(new CBView(in, out));
    controller.start();
    assertEquals("portfolio name: "
            + "Operation failed: Portfolio to invest is empty.\n", out.toString());
  }

  @Test
  public void testInvestFail() {
    in = new StringReader("invest\naPort\n2018-10-1-14\n1000\n0.3,0.3,0.4\n5");
    run(ModelName.FOUR);
    assertEquals("portfolio name: time to invest(yyyy-mm-dd-hh): "
            + "amount of dollar to invest: \"equal\" or weights of "
            + "Stock1 Stock2 Stock3 (separated by \",\", add up to 1): commission fee: "
            + "Investing specified weights failed message.\n", out.toString());
  }

  @Test
  public void testInvestBadTime() {
    Reader[] inputs = {new StringReader("invest\naPort\nbadInput"),
                       new StringReader("invest\naPort\n2099-1-1-9")};
    String common = "portfolio name: time to invest(yyyy-mm-dd-hh): Operation failed: ";
    String[] fail = {"Invalid time format.\n",
                     "Time should be earlier than current time.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.THREE);
      assertEquals("", log.toString());
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testInvestBadAmount() {
    Reader[] inputs = {new StringReader("invest\naPort\n2018-10-1-14\nbadInput"),
                       new StringReader("invest\naPort\n2018-10-1-14\n0"),
                       new StringReader("invest\naPort\n2018-10-1-14\n-100")};
    String common = "portfolio name: time to invest(yyyy-mm-dd-hh): "
            + "amount of dollar to invest: Operation failed: ";
    String[] fail = {"Invalid amount of dollar.\n",
                     "Amount of dollar should be positive.\n",
                     "Amount of dollar should be positive.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testInvestBadWeight() {
    Reader[] inputs = {new StringReader("invest\naPort\n2018-10-1-14\n1000\nbadInput,0,0"),
                       new StringReader("invest\naPort\n2018-10-1-14\n1000\n-0.1,0,0"),
                       new StringReader("invest\naPort\n2018-10-1-14\n1000\n0.1,0.2,0.3"),
                       new StringReader("invest\naPort\n2018-10-1-14\n1000\n1"),
                       new StringReader("invest\naPort\n2018-10-1-14\n1000\n"
                               + "0.25,0.25,0.25,0.25")};
    String common = "portfolio name: time to invest(yyyy-mm-dd-hh): amount of dollar to invest: "
            + "\"equal\" or weights of Stock1 Stock2 Stock3 (separated by \",\", add up to 1): "
            + "Operation failed: ";
    String[] fail = {"Invalid weight of stock.\n",
                     "Weight of stock should be no-negative.\n",
                     "Total weights should add up to 1.\n",
                     "Number of weights should equals to number of stocks.\n",
                     "Number of weights should equals to number of stocks.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.THREE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testCreateStrategy() {
    in = new StringReader("createStrategy\ndollar-cost averaging\naStr\n2018-10-1\n2018-10-2\n"
            + "10\n5000\nAAPL,GOOG,VT\n0.2,0.3,0.5\n10\n");
    run(ModelName.ONE);
    assertEquals("Create a dollar-cost averaging strategy aStr from 2018-10-1-16 to "
            + "2018-10-2-16 with commission fee of 10.00.\n"
            + "AAPL:1000.00 GOOG:1500.00 VT:2500.00 \n", log.toString());
    assertEquals("strategy type(dollar-cost averaging): strategy name: "
            + "start date(yyyy-mm-dd): end date(yyyy-mm-dd or ongoing): interval days: "
            + "amount of dollar to invest: stocks to invest (separated by \",\"): "
            + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
            + "commission fee: Create a dollar-cost averaging successfully.\n", out.toString());
  }

  @Test
  public void testCreateStrategyEqualOngoing() {
    in = new StringReader("createStrategy\ndollar-cost averaging\naStr\n2018-10-1\nongoing\n"
            + "10\n5000\nAAPL,GOOG,VT\nequal\n10\n");
    run(ModelName.ONE);
    assertEquals("Create a dollar-cost averaging strategy aStr from 2018-10-1-16"
            + " and ongoing with commission fee of 10.00.\n"
            + "AAPL:1666.67 GOOG:1666.67 VT:1666.67 \n", log.toString());
    assertEquals("strategy type(dollar-cost averaging): strategy name: "
            + "start date(yyyy-mm-dd): end date(yyyy-mm-dd or ongoing): interval days: "
            + "amount of dollar to invest: stocks to invest (separated by \",\"): "
            + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
            + "commission fee: Create a dollar-cost averaging successfully.\n", out.toString());
  }

  @Test
  public void testCreateStrategyFail() {
    in = new StringReader("createStrategy\ndollar-cost averaging\naStr\n2018-10-1\nongoing\n"
            + "10\n5000\nAAPL,GOOG,VT\nequal\n10\n");
    run(ModelName.TWO);
    assertEquals("strategy type(dollar-cost averaging): strategy name: "
            + "start date(yyyy-mm-dd): end date(yyyy-mm-dd or ongoing): interval days: "
            + "amount of dollar to invest: stocks to invest (separated by \",\"): "
            + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
            + "commission fee: Creating strategy failed message.\n", out.toString());
  }

  @Test
  public void testCreateStrategyBadType() {
    in = new StringReader("createStrategy\nbadInput");
    run(ModelName.ONE);
    assertEquals("strategy type(dollar-cost averaging): "
            + "Operation failed: Unsupported type.\n", out.toString());
  }

  @Test
  public void testCreateStrategyBadTime() {
    Reader[] inputs = {new StringReader("createStrategy\ndollar-cost averaging\nstr\n"
                              + "badInput1"),
                       new StringReader("createStrategy\ndollar-cost averaging\nstr\n"
                              + "2099-1-1\n"),
                       new StringReader("createStrategy\ndollar-cost averaging\nstr\n"
                              + "2018-1-2\nbadInput"),
                       new StringReader("createStrategy\ndollar-cost averaging\nstr\n"
                              + "2018-1-2\n2018-1-1")};
    String common = "strategy type(dollar-cost averaging): strategy name: start date(yyyy-mm-dd): ";
    String[] fail = {"Operation failed: Invalid time format.\n",
                     "Operation failed: Start date should be earlier than current date.\n",
                     "end date(yyyy-mm-dd or ongoing): Operation failed: Invalid time format.\n",
                     "end date(yyyy-mm-dd or ongoing): Operation failed: "
                     + "End date should be later than start date.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testCreateStrategyBadInterval() {
    Reader[] inputs = {new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\nbadInput"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n0"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n-1"),};
    String common = "strategy type(dollar-cost averaging): strategy name: start date(yyyy-mm-dd): "
            + "end date(yyyy-mm-dd or ongoing): interval days: Operation failed: ";
    String[] fail = {"Invalid interval days.\n",
                     "Interval days should be positive.\n",
                     "Interval days should be positive.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testCreateStrategyBadAmount() {
    Reader[] inputs = {new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\nbadInput"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n0"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n-1"),};
    String common = "strategy type(dollar-cost averaging): strategy name: start date(yyyy-mm-dd): "
            + "end date(yyyy-mm-dd or ongoing): interval days: amount of dollar to invest: "
            + "Operation failed: ";
    String[] fail = {"Invalid amount of dollar.\n",
                     "Amount of dollar should be positive.\n",
                     "Amount of dollar should be positive.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testCreateStrategyBadWeight() {
    Reader[] inputs = {new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n1000\nAAPL\nbadInput"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n1000\nAAPL\n-0.1"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n1000\nAAPLE,VT\n0.2,0.3"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n1000\nAAPL\n0.5,0.5")};
    String common = "strategy type(dollar-cost averaging): strategy name: start date(yyyy-mm-dd): "
            + "end date(yyyy-mm-dd or ongoing): interval days: amount of dollar to invest: "
            + "stocks to invest (separated by \",\"): "
            + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
            + "Operation failed: ";
    String[] fail = {"Invalid weight of stock.\n",
                     "Weight of stock should be no-negative.\n",
                     "Total weights should add up to 1.\n",
                     "Number of weights should equals to number of stocks.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testCreateStrategyBadFee() {
    Reader[] inputs = {new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n1000\nAAPL\n1\nbadInput"),
                       new StringReader("createStrategy\ndollar-cost averaging\naStr\n"
                       + "2018-10-1\n2018-11-1\n5\n1000\nAAPL\n1\n-1")};
    String common = "strategy type(dollar-cost averaging): strategy name: start date(yyyy-mm-dd): "
            + "end date(yyyy-mm-dd or ongoing): interval days: amount of dollar to invest: "
            + "stocks to invest (separated by \",\"): "
            + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
            + "commission fee: Operation failed: ";
    String[] fail = {"Invalid commission fee.\n",
                     "Commission fee should be no-negative.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testSaveStrategy() {
    in = new StringReader("saveStrategy\naStr");
    run(ModelName.ONE);
    assertEquals("Save strategy aStr.\n", log.toString());
    assertEquals("strategy name: Strategy aStr saved successfully.\n",
            out.toString());
  }

  @Test
  public void testSaveStrategyFail() {
    in = new StringReader("saveStrategy\naStr");
    run(ModelName.TWO);
    assertEquals("strategy name: Saving strategy failed message.\n", out.toString());
  }

  @Test
  public void testRetrieveStrategy() {
    in = new StringReader("retrieveStrategy\naStr");
    run(ModelName.ONE);
    assertEquals("Retrieve strategy aStr.\n", log.toString());
    assertEquals("strategy name: Strategy aStr retrieved successfully.\n",
            out.toString());
  }

  @Test
  public void testRetrieveStrategyFail() {
    in = new StringReader("retrieveStrategy\naStr");
    run(ModelName.TWO);
    assertEquals("strategy name: Retrieving strategy failed message.\n", out.toString());
  }

  @Test
  public void testApplyStrategy() {
    in = new StringReader("applyStrategy\naStr\naPort");
    run(ModelName.ONE);
    assertEquals("Apply strategy aStr to portfolio aPort.\n", log.toString());
    assertEquals("strategy name: portfolio name: "
                    + "Apply strategy aStr to portfolio aPort successfully.\n",
            out.toString());
  }

  @Test
  public void testApplyStrategyFail() {
    in = new StringReader("applyStrategy\naStr\naPort");
    run(ModelName.TWO);
    assertEquals("strategy name: portfolio name: Applying strategy failed message.\n",
            out.toString());
  }

  @Test
  public void testViewStrategy() {
    in = new StringReader("viewStrategy\naStr");
    run(ModelName.ONE);
    assertEquals("View strategy aStr.\n", log.toString());
    assertEquals("strategy name: Strategy aStr\n",
            out.toString());
  }

  @Test
  public void testViewStrategyFail() {
    in = new StringReader("viewStrategy\naStr");
    run(ModelName.TWO);
    assertEquals("strategy name: Viewing strategy failed message.\n", out.toString());
  }

  @Test
  public void testViewAllStrategy() {
    in = new StringReader("viewAllStrategy\n");
    run(ModelName.ONE);
    assertEquals("View all strategy.\n", log.toString());
    assertEquals("all strategies\n",
            out.toString());
  }

  @Test
  public void testViewAllStrategyFail() {
    in = new StringReader("viewAllStrategy\n");
    run(ModelName.TWO);
    assertEquals("Viewing all strategy failed message.\n", out.toString());
  }

  @Test
  public void testgetBasis() {
    in = new StringReader("getBasis\naPort\n2018-10-1-14");
    run(ModelName.ONE);
    assertEquals("Get cost basis of portfolio aPort at 2018-10-1-14.\n",
            log.toString());
    assertEquals("portfolio name: time to get cost basis(yyyy-mm-dd-hh): "
            + "The cost basis is: 12.23.\n", out.toString());
  }

  @Test
  public void testPlot() {
    in = new StringReader("plotPort\nmonth\naPort\n2018-1-1\n2018-2-1\n1");
    run(ModelName.ONE);
    assertEquals("Plot portfolio aPort from 2018-1-1-23 to 2018-2-1-23 every 1 MONTHs.\n",
            log.toString());
    assertEquals("type of interval(year/month/day): portfolio name: "
                    + "start date(yyyy-mm-dd): end date(yyyy-mm-dd): interval: "
                    + "Chart is saved to the root path.\n",
            out.toString());
  }

  @Test
  public void testPlotFail() {
    in = new StringReader("plotPort\nmonth\naPort\n2018-1-1\n2018-2-1\n1");
    run(ModelName.TWO);
    assertEquals("type of interval(year/month/day): portfolio name: "
            + "start date(yyyy-mm-dd): end date(yyyy-mm-dd): interval: "
            + "Plotting failed message.\n", out.toString());
  }

  @Test
  public void testgetBasisFail() {
    in = new StringReader("getBasis\naPort\n2018-10-1-14");
    run(ModelName.TWO);
    assertEquals("portfolio name: time to get cost basis(yyyy-mm-dd-hh): "
            + "Getting cost basis failed message.\n", out.toString());
  }

  @Test
  public void testgetBasisBadTime() {
    Reader[] inputs = {new StringReader("getBasis\naPort\nbadInput"),
                       new StringReader("getBasis\naPort\n2099-1-1-9")};
    String common = "portfolio name: time to get cost basis(yyyy-mm-dd-hh): Operation failed: ";
    String[] fail = {"Invalid time format.\n",
                     "Time should be earlier than current time.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testgetValue() {
    in = new StringReader("getValue\naPort\n2018-10-1-14");
    run(ModelName.ONE);
    assertEquals("Get total value of portfolio aPort at 2018-10-1-14.\n",
            log.toString());
    assertEquals("portfolio name: time to get total value(yyyy-mm-dd-hh): "
            + "The total value is: 12.23.\n", out.toString());
  }

  @Test
  public void testgetValueFail() {
    in = new StringReader("getValue\naPort\n2018-10-1-14");
    run(ModelName.TWO);
    assertEquals("portfolio name: time to get total value(yyyy-mm-dd-hh): "
            + "Getting total value failed message.\n", out.toString());
  }

  @Test
  public void testTotalValueBadTime() {
    Reader[] inputs = {new StringReader("getValue\naPort\nbadInput"),
                       new StringReader("getValue\naPort\n2099-1-1-9")};
    String common = "portfolio name: time to get total value(yyyy-mm-dd-hh): Operation failed: ";
    String[] fail = {"Invalid time format.\n",
                     "Time should be earlier than current time.\n"};
    for (int i = 0; i < inputs.length; i++) {
      in = inputs[i];
      run(ModelName.ONE);
      assertEquals(common + fail[i], out.toString());
    }
  }

  @Test
  public void testQuit() {
    in = new StringReader("quit");
    run(ModelName.ONE);
    assertEquals("Already quit the program.\n", out.toString());
  }

  @Test
  public void testHelp() {
    in = new StringReader("help");
    run(ModelName.ONE);
    assertEquals("Known commands: importStock, createPort, savePort, retrievePort, "
                    + "viewPort, viewAllPort, addStock, buyStock, invest, getHistory, getBasis, "
                    + "getValue plotPort, createStrategy, saveStrategy, retrieveStrategy, "
                    + "viewStrategy, viewAllStrategy, applyStrategy, help.\n",
            out.toString());
  }

  @Test
  public void testCommandNotFound() {
    in = new StringReader("badCommand");
    run(ModelName.ONE);
    assertEquals("Command not found.\n", out.toString());
  }

  @Test
  public void testFullProcessWithMockModel() {
    in = new StringReader("importStock\nalpha vantage\nGOOG\n"
            + "createPort\naPort\n \n"
            + "addStock\naPort\nAAPL\n"
            + "savePort\naPort\n"
            + "retrievePort\naPort\n"
            + "buyStock\n2018-1-1-9\naPort\nAAPL\n100\n5\n"
            + "invest\naPort\n2018-2-1-9\n1000\nequal\n5\n"
            + "createStrategy\ndollar-cost averaging\naStr\n2018-10-1\n2018-10-2\n"
            + "10\n5000\nAAPL,GOOG\n0.4,0.6\n0\n"
            + "saveStrategy\naStr\n"
            + "retrieveStrategy\naStr\n"
            + "applyStrategy\naStr\naPort\n"
            + "getBasis\naPort\n2018-1-1-9\n"
            + "getValue\naPort\n2018-1-1-9\n"
            + "viewPort\naPort\n"
            + "viewAllPort\n"
            + "viewStrategy\naStr\n"
            + "viewAllStrategy\n"
            + "quit\n");
    run(ModelName.ONE);
    assertEquals("Import stockGOOG from ALPHAVANTAGE.\n"
            + "Create an empty portfolio named aPort.\n"
            + "Add stock AAPL to portfolio aPort.\n"
            + "Save portfolio aPort.\n"
            + "Retrieve portfolio aPort.\n"
            + "Buy 100.00 shares of AAPL in portfolio aPort at 2018-1-1-9 with commission "
            + "fee of 5.00.\n"
            + "View portfolio aPort.\n"
            + "Create a dollar-cost averaging strategy aStr from 2018-10-1-16 to 2018-10-2-16 "
            + "with commission fee of 0.00.\n"
            + "AAPL:2000.00 GOOG:3000.00 \n"
            + "Save strategy aStr.\n"
            + "Retrieve strategy aStr.\n"
            +  "Apply strategy aStr to portfolio aPort.\n"
            + "Get cost basis of portfolio aPort at 2018-1-1-9.\n"
            + "Get total value of portfolio aPort at 2018-1-1-9.\n"
            + "View portfolio aPort.\n"
            + "View all portfolios.\n"
            + "View strategy aStr.\n"
            + "View all strategy.\n", log.toString());
    assertEquals("API(Alpha Vantage): stock name: Import stock GOOG successfully.\n"
            + "portfolio name: stocks to add (separated by \",\"), or \" \" if empty: "
            + "Portfolio aPort created successfully.\n"
            + "portfolio name: stock name: Add stock AAPL successfully.\n"
            + "portfolio name: Portfolio aPort saved successfully.\n"
            + "portfolio name: Portfolio aPort retrieved successfully.\n"
            + "time to buy(yyyy-mm-dd-hh): portfolio name: stock name: shares to buy: "
            + "commission fee: Buy successfully.\n"
            + "portfolio name: time to invest(yyyy-mm-dd-hh): amount of dollar to invest: "
            + "\"equal\" or weights of stocks in portfolio aPort(separated by \",\", add up to 1): "
            + "commission fee: Invest successfully.\n"
            + "strategy type(dollar-cost averaging): strategy name: start date(yyyy-mm-dd): "
            + "end date(yyyy-mm-dd or ongoing): interval days: amount of dollar to invest: "
            + "stocks to invest (separated by \",\"): "
            + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
            + "commission fee: Create a dollar-cost averaging successfully.\n"
            + "strategy name: Strategy aStr saved successfully.\n"
            + "strategy name: Strategy aStr retrieved successfully.\n"
            + "strategy name: portfolio name: "
            + "Apply strategy aStr to portfolio aPort successfully.\n"
            + "portfolio name: time to get cost basis(yyyy-mm-dd-hh): The cost basis is: 12.23.\n"
            + "portfolio name: time to get total value(yyyy-mm-dd-hh): The total value is: 12.23.\n"
            + "portfolio name: Portfolio aPort contains: stocks in portfolio aPort.\n"
            + "all portfolios\n"
            + "strategy name: Strategy aStr\n"
            + "all strategies\n"
            + "Already quit the program.\n", out.toString());
  }

  @Test
  public void testFullProcessWithTrueModel() {
    in = new StringReader("importStock\nalpha vantage\nGOOG\n"
            + "createPort\naPort\nAAPL\n"
            + "addStock\naPort\nGOOG\n"
            + "buyStock\n2018-1-3-9\naPort\nAAPL\n100\n5\n"
            + "invest\naPort\n2018-2-1-9\n1000\nequal\n5\n"
            + "createStrategy\ndollar-cost averaging\naStr\n2018-10-1\n2018-10-2\n"
            + "10\n5000\nAAPL,GOOG\n0.4,0.6\n0\n"
            + "applyStrategy\naStr\naPort\n"
            + "getBasis\naPort\n2018-1-1-9\n"
            + "getValue\naPort\n2018-1-1-9\n"
            + "viewPort\naPort\n"
            + "viewAllPort\n"
            + "viewStrategy\naStr\n"
            + "viewAllStrategy\n"
            + "quit\n");
    run(ModelName.TRUE);
    assertEquals("API(Alpha Vantage): stock name: Import stock GOOG successfully.\n"
                    + "portfolio name: stocks to add (separated by \",\"), or \" \" if empty: "
                    + "Portfolio aPort created successfully.\n"
                    + "portfolio name: stock name: Add stock GOOG successfully.\n"
                    + "time to buy(yyyy-mm-dd-hh): portfolio name: stock name: shares to buy: "
                    + "commission fee: Buy successfully.\n"
                    + "portfolio name: time to invest(yyyy-mm-dd-hh): amount of dollar to invest: "
                    + "\"equal\" or weights of AAPL GOOG (separated by \",\", add up to 1): "
                    + "commission fee: Invest successfully.\n"
                    +  "strategy type(dollar-cost averaging): strategy name: "
                    + "start date(yyyy-mm-dd): "
                    + "end date(yyyy-mm-dd or ongoing): interval days: amount of dollar to invest: "
                    + "stocks to invest (separated by \",\"): "
                    + "\"equal\" or weights of above stocks (separated by \",\", add up to 1): "
                    + "commission fee: Create a dollar-cost averaging successfully.\n"
                    + "strategy name: portfolio name: "
                    + "Apply strategy aStr to portfolio aPort successfully.\n"
                    + "portfolio name: time to get cost basis(yyyy-mm-dd-hh): "
                    + "The cost basis is: 0.00.\n"
                    + "portfolio name: time to get total value(yyyy-mm-dd-hh): "
                    + "The total value is: 0.00.\n"
                    + "portfolio name: Portfolio aPort contains: AAPL GOOG .\n"
                    + "aPort:AAPL GOOG \n"
                    + "strategy name: From: 2018-10-1-16  To: 2018-10-2-16  "
                    + "Interval: 10  Commission Fee: 0.0\n"
                    + "Stock: AAPL  Amount of Money: 2000.00Stock: GOOG  "
                    + "Amount of Money: 3000.00\n"
                    + "aStr:From: 2018-10-1-16  To: 2018-10-2-16  Interval: 10  "
                    + "Commission Fee: 0.0\n"
                    + "Stock: AAPL  Amount of Money: 2000.00Stock: GOOG  Amount of Money: 3000.00\n"
                    + "Already quit the program.\n",
            out.toString());
  }

}