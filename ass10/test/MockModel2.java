import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;

import virtualinvestment.model.PlotType;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.model.datasource.DataSourceType;


/**
 * This is a mock model to test the controller.
 */
public class MockModel2 implements VirtualInvestment {


  @Override
  public void reset() {
    // be tested in model
  }

  @Override
  public void addDataFromInternet(DataSourceType api, String name) throws IllegalStateException {
    throw new IllegalStateException("Importing stock failed message.\n");
  }

  @Override
  public void createPortfolio(String pName) throws IllegalStateException {
    throw new IllegalStateException("Creating empty portfolio failed message.\n");
  }

  @Override
  public void createPortfolio(String pName, String[] sNames) throws IllegalStateException {
    throw new IllegalStateException("Creating nonempty portfolio failed message.\n");
  }

  @Override
  public void savePortfolio(String pName) {
    throw new IllegalStateException("Saving portfolio failed message.\n");
  }

  @Override
  public void retrievePortfolio(String prName) {
    throw new IllegalStateException("Retrieving nonempty portfolio failed message.\n");
  }

  @Override
  public void addStock(String pName, String sName) throws IllegalStateException {
    throw new IllegalStateException("Adding stock failed message.\n");
  }

  @Override
  public String viewPortfolio(String pName) throws IllegalStateException {
    throw new IllegalStateException("Viewing portfolio failed message.\n");
  }

  @Override
  public String viewAllPortfolio() {
    throw new IllegalStateException("Viewing all portfolios failed message.\n");
  }


  @Override
  public String getPurchaseHistory(String pName, IMyCalendar date) throws IllegalStateException {
    throw new IllegalStateException("Getting purchase history of portfolio failed message.\n");
  }

  @Override
  public void buyStock(String pName, String sName, double shares, double fee, IMyCalendar date)
          throws IllegalStateException {
    throw new IllegalStateException("Buying stock failed message.\n");
  }

  @Override
  public void invest(String pName, IMyCalendar time, double fee, Map<String, Double> weights)
          throws IllegalStateException {
    // be tested in mock model 3
  }

  @Override
  public void createDollarCostAverage(String strName, IMyCalendar start, IMyCalendar end,
                                      int interval, double fee, Map<String, Double> weights) {
    throw new IllegalStateException("Creating strategy failed message.\n");
  }

  @Override
  public void saveStrategy(String strName) {
    throw new IllegalStateException("Saving strategy failed message.\n");
  }

  @Override
  public void retrieveStrategy(String strName) {
    throw new IllegalStateException("Retrieving strategy failed message.\n");
  }

  @Override
  public void applyStrategy(String strName, String pName) {
    throw new IllegalStateException("Applying strategy failed message.\n");
  }

  @Override
  public String viewStrategy(String strName) {
    throw new IllegalStateException("Viewing strategy failed message.\n");
  }

  @Override
  public String viewAllStrategy() {
    throw new IllegalStateException("Viewing all strategy failed message.\n");
  }

  @Override
  public double getCostBasis(String pName, IMyCalendar date) throws IllegalStateException {
    throw new IllegalStateException("Getting cost basis failed message.\n");
  }

  @Override
  public double getTotalValue(String pName, IMyCalendar date) throws IllegalStateException {
    throw new IllegalStateException("Getting total value failed message.\n");
  }

  @Override
  public Map<String, Double> plot(String pName, IMyCalendar start, IMyCalendar end,
                                  PlotType type, int interval) {
    throw new IllegalStateException("Plotting failed message.\n");
  }

}
