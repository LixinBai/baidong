import java.util.Map;

import virtualinvestment.calendar.IMyCalendar;
import virtualinvestment.model.PlotType;
import virtualinvestment.model.VirtualInvestment;
import virtualinvestment.model.datasource.DataSourceType;

/**
 * This is a mock model to test the controller.
 */
public class MockModel3 implements VirtualInvestment {
  private final StringBuilder log;
  private final int uniqueInt;
  private final boolean success;

  /**
   * Create a mock model.
   *
   * @param log       the log to record operations
   * @param uniqueInt the amount to return
   * @param success   true if the operations succeed, false other wise
   */
  public MockModel3(StringBuilder log, int uniqueInt, boolean success) {
    this.log = log;
    this.uniqueInt = uniqueInt;
    this.success = success;
  }


  @Override
  public void reset() {
    // be tested in model
  }

  @Override
  public void addDataFromInternet(DataSourceType api, String name) throws IllegalStateException {
    // be tested in mock model 1 and 2
  }

  @Override
  public void createPortfolio(String pName) throws IllegalStateException {
    // be tested in mock model 1 and 2
  }

  @Override
  public void createPortfolio(String pName, String[] sNames) throws IllegalStateException {
    // be tested in mock model 1 and 2
  }

  @Override
  public void savePortfolio(String pName) {
    // be tested in mock model 1 and 2
  }

  @Override
  public void retrievePortfolio(String prName) {
    // be tested in mock model 1 and 2
  }

  @Override
  public void addStock(String pName, String sName) throws IllegalStateException {
    // be tested in mock model 1 and 2
  }

  @Override
  public String viewPortfolio(String pName) throws IllegalStateException {
    StringBuilder stocks = new StringBuilder();
    for (int i = 0; i < uniqueInt; i++) {
      stocks.append("Stock").append(i + 1).append(" ");
    }
    return stocks.toString();
  }

  @Override
  public String viewAllPortfolio() {
    // be tested in mock model 1 and 2
    return null;
  }


  @Override
  public String getPurchaseHistory(String pName, IMyCalendar time) throws IllegalStateException {
    // be tested in mock model 1 and 2
    return null;
  }

  @Override
  public void buyStock(String pName, String sName, double shares, double fee, IMyCalendar time)
          throws IllegalStateException {
    // be tested in mock model 1 and 2
  }

  @Override
  public void invest(String pName, IMyCalendar time, double fee, Map<String, Double> weights)
          throws IllegalStateException {
    if (success) {
      log.append("Invest in portfolio ").append(pName).append(" with amount of ");
      for (Map.Entry<String, Double> entry : weights.entrySet()) {
        log.append(entry.getKey()).append(":")
                .append(String.format("%.2f", entry.getValue())).append(" ");
      }
      log.append("with commission fee of ").append(String.format("%.2f", fee)).append(".\n");
    } else {
      throw new IllegalStateException("Investing specified weights failed message.\n");
    }
  }

  @Override
  public void createDollarCostAverage(String strName, IMyCalendar start, IMyCalendar end,
                                      int interval, double fee, Map<String, Double> weights) {
    // be tested in mock model 1 and 2
  }

  @Override
  public void applyStrategy(String strName, String pName) {
    // be tested in mock model 1 and 2
  }

  @Override
  public void saveStrategy(String strName) {
    // be tested in mock model 1 and 2
  }

  @Override
  public void retrieveStrategy(String strName) {
    // be tested in mock model 1 and 2
  }

  @Override
  public String viewStrategy(String strName) {
    // be tested in mock model 1 and 2
    return null;
  }

  @Override
  public String viewAllStrategy() {
    // be tested in mock model 1 and 2
    return null;
  }

  @Override
  public double getCostBasis(String pName, IMyCalendar time) throws IllegalStateException {
    // be tested in mock model 1 and 2
    return 0;
  }

  @Override
  public double getTotalValue(String pName, IMyCalendar time) throws IllegalStateException {
    // be tested in mock model 1 and 2
    return 0;
  }

  @Override
  public Map<String, Double> plot(String pName, IMyCalendar start, IMyCalendar end, PlotType type,
                                  int interval) throws IllegalStateException {
    // be tested in mock model 1 and 2
    return null;
  }

}
