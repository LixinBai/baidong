package vehicle;

/**
 * This class represents a regular vehicle with manual transmission. The transmission supports
 * exactly 5 gears. Its initial speed is 0, and initial gear is 1. Its speed changes 1 at one time.
 */
public class RegularManualTransmission implements ManualTransmission {
  private int gear;
  private int speed;
  private String status;
  public int[] l;
  public int[] h;

  /**
   * Construct a regular manual transmission with its gear intervals.
   *
   * @param l1 lower bound of gear 1
   * @param h1 higher bound of gear 1
   * @param l2 lower bound of gear 2
   * @param h2 higher bound of gear 2
   * @param l3 lower bound of gear 3
   * @param h3 higher bound of gear 3
   * @param l4 lower bound of gear 4
   * @param h4 higher bound of gear 4
   * @param l5 lower bound of gear 5
   * @param h5 higher bound of gear 5
   */
  public RegularManualTransmission(int l1, int h1, int l2, int h2, int l3, int h3, int l4,
                                   int h4, int l5, int h5) throws IllegalArgumentException {
    int[] low = new int[]{l1, l2, l3, l4, l5};
    int[] high = new int[]{h1, h2, h3, h4, h5};

    if (l1 != 0) {
      throw new IllegalArgumentException("Illegal parameters.");
    }

    for (int i = 0; i <= 4; i++) {
      if (low[i] > high[i]) {
        throw new IllegalArgumentException("Illegal parameters.");
      }
    }

    for (int i = 0; i <= 3; i++) {
      if (low[i] >= low[i + 1]) {
        throw new IllegalArgumentException("Illegal parameters.");
      }
    }

    for (int i = 0; i <= 3; i++) {
      if (high[i] < low[i + 1]) {
        throw new IllegalArgumentException("Illegal parameters.");
      }
    }

    for (int i = 0; i < 3; i++) {
      if (high[i] >= low[i + 2]) {
        throw new IllegalArgumentException("Illegal parameters.");
      }
    }
    this.l = low;
    this.h = high;
    this.gear = 1;
    this.speed = 0;
    this.status = null;
  }

  private RegularManualTransmission(int gear, int speed, String status, int[] l, int[] h) {
    this.gear = gear;
    this.speed = speed;
    this.status = status;
    this.l = l;
    this.h = h;
  }

  @Override
  public String getStatus() {
    return this.status;
  }

  @Override
  public int getSpeed() {
    return this.speed;
  }

  @Override
  public int getGear() {
    return this.gear;
  }

  @Override
  public ManualTransmission increaseSpeed() {
    int g = this.getGear();
    int s = this.getSpeed();
    String status = this.getStatus();

    if (g == 5) {
      if (l[4] <= s && s < h[4]) {
        s++;
        status = "OK: everything is OK.";
      } else if (s == h[g - 1]) {
        status = "Cannot increase speed. Reached maximum speed.";
      }
    } else {
      if (l[g - 1] <= s && s < (l[g] - 1)) {
        s++;
        status = "OK: everything is OK.";
      } else if ((l[g] - 1) <= s && s < h[g - 1]) {
        s++;
        status = "OK: you may increase the gear.";
      } else if (s == h[g - 1]) {
        status = "Cannot increase speed, increase gear first.";
      }
    }
    ManualTransmission mt = new RegularManualTransmission(g, s, status, l, h);

    return mt;
  }

  @Override
  public ManualTransmission decreaseSpeed() {
    int g = this.getGear();
    int s = this.getSpeed();
    String status = this.getStatus();

    if (g == 1) {
      if (l[g - 1] < s && s <= h[g - 1]) {
        s--;
        status = "OK: everything is OK.";
      } else if (s == l[g - 1]) {
        status = "Cannot decrease speed. Reached minimum speed.";
      }
    } else {
      if (l[g - 1] < s && s <= (h[g - 2] + 1)) {
        s--;
        status = "OK: you may decrease the gear.";
      } else if ((h[g - 2] + 1) < s && s <= h[g - 1]) {
        s--;
        status = "OK: everything is OK.";
      } else if (s == l[g - 1]) {
        status = "Cannot decrease speed, decrease gear first.";
      }
    }

    ManualTransmission mt = new RegularManualTransmission(g, s, status, l, h);

    return mt;
  }

  @Override
  public ManualTransmission increaseGear() {
    int g = this.getGear();
    int s = this.getSpeed();
    String status = this.getStatus();

    if (g == 5) {
      status = "Cannot increase gear. Reached maximum gear.";
    } else {
      if (l[g - 1] <= s && s < l[g]) {
        status = "Cannot increase gear, increase speed first.";
      } else if (l[g] <= s && s <= h[g - 1]) {
        g++;
        status = "OK: everything is OK.";
      }
    }
    ManualTransmission mt = new RegularManualTransmission(g, s, status, l, h);

    return mt;
  }

  @Override
  public ManualTransmission decreaseGear() {
    int g = this.getGear();
    int s = this.getSpeed();
    String status = this.getStatus();

    if (g == 1) {
      status = "Cannot decrease gear. Reached minimum gear.";
    } else {
      if (l[g - 1] <= s && s <= h[g - 2]) {
        g--;
        status = "OK: everything is OK.";
      } else if (h[g - 2] < s && s <= h[g - 1]) {
        status = "Cannot decrease gear, decrease speed first.";
      }
    }

    ManualTransmission mt = new RegularManualTransmission(g, s, status, l, h);

    return mt;
  }
}
