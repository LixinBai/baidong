package vehicle;

/**
 * This interface represents a set of operations on any vehicle with manual transmission.
 */
public interface ManualTransmission {
  String getStatus();

  /**
   * Return the speed of this vehicle as an integer.
   *
   * @return the speed of this vehicle
   */
  int getSpeed();

  /**
   * Return the gear of this vehicle as an integer.
   *
   * @return the gear of this vehicle
   */
  int getGear();

  /**
   * Increase the speed by a fixed amount without changing gears.
   *
   * @return the resulting transmission object
   */
  ManualTransmission increaseSpeed();

  /**
   * Decrease the speed by a fixed amount without changing gears.
   *
   * @return the resulting transmission object
   */
  ManualTransmission decreaseSpeed();

  /**
   * Increase the gear by one without changing speed.
   *
   * @return the resulting transmission object
   */
  ManualTransmission increaseGear();

  /**
   * Decrease the gear by one without changing speed.
   *
   * @return the resulting transmission object
   */
  ManualTransmission decreaseGear();
}
