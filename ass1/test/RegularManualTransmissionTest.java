import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import vehicle.RegularManualTransmission;
import vehicle.ManualTransmission;

/**
 * This class tests various situations for a regular manual transmission.
 */
public class RegularManualTransmissionTest {
  private ManualTransmission test1;
  private ManualTransmission test2;
  private ManualTransmission test3;
  private ManualTransmission test4;

  /**
   * Initialize testers.
   */
  @Before
  public void setUp() {

    //Objects for tests.
    test1 = new RegularManualTransmission(0, 16, 13, 35, 21, 44, 39, 50, 50, 52);
    test2 = new RegularManualTransmission(0, 16, 13, 35, 21, 44, 39, 50, 50, 52);
    test3 = new RegularManualTransmission(0, 16, 13, 35, 21, 44, 39, 50, 50, 52);
    test4 = new RegularManualTransmission(0, 16, 13, 35, 21, 44, 39, 50, 50, 52);
  }

  /**
   * Test the whether the constructor can construct well-ordered and overlapping gear intervals.
   */
  @Test
  public void IllegalParams1() {
    ManualTransmission wrong1;
    ManualTransmission wrong2;
    ManualTransmission wrong3;
    ManualTransmission wrong4;
    ManualTransmission wrong5;
    ManualTransmission wrong6;
    //The lower speed of the first gear is not 0.
    boolean except1 = false;
    try {
      wrong1 = new RegularManualTransmission(1, 22, 21, 26, 24, 39, 31, 45, 40, 47);
    } catch (IllegalArgumentException e) {
      except1 = true;
    }
    assertTrue(except1);

    //The gear's lower bound exceeds its higher bound.
    boolean except2 = false;
    try {
      wrong2 = new RegularManualTransmission(0, 22, 26, 21, 24, 39, 31, 45, 40, 47);
    } catch (IllegalArgumentException e) {
      except2 = true;
    }
    assertTrue(except2);

    //The lower gear's lower speed is higher than higher gear's.
    boolean except3 = false;
    try {
      wrong3 = new RegularManualTransmission(0, 13, 13, 17, 12, 19, 21, 27, 26, 48);
    } catch (IllegalArgumentException e) {
      except3 = true;
    }
    assertTrue(except3);

    //Ranges are non-overlapping.
    boolean except4 = false;
    try {
      wrong4 = new RegularManualTransmission(0, 11, 13, 14, 14, 25, 19, 36, 33, 67);
    } catch (IllegalArgumentException e) {
      except4 = true;
    }
    assertTrue(except4);

    //Non-adjacent gears overlap.
    boolean except5 = false;
    try {
      wrong5 = new RegularManualTransmission(0, 4, 2, 5, 3, 6, 6, 7, 7, 8);
    } catch (IllegalArgumentException e) {
      except5 = true;
    }
    assertTrue(except5);

    //Input negative integers.
    boolean except6 = false;
    try {
      wrong6 = new RegularManualTransmission(0, -1, 0, 5, 13, 26, 29, 57, 47, 98);
    } catch (IllegalArgumentException e) {
      except6 = true;
    }
    assertTrue(except6);

  }

  /**
   * Test methods increase and decrease speed.
   */
  @Test
  public void singleIncreaseDecreaseSpeed() {
    assertEquals(0, test1.getSpeed());
    assertEquals(1, test1.getGear());
    assertEquals(null, test1.getStatus());

    test1 = test1.increaseSpeed();
    assertEquals(1, test1.getSpeed());
    assertEquals(1, test1.getGear());
    assertEquals("OK: everything is OK.", test1.getStatus());

    test1 = test1.decreaseSpeed();
    assertEquals(0, test1.getSpeed());
    assertEquals(1, test1.getGear());
    assertEquals("OK: everything is OK.", test1.getStatus());
  }

  /**
   * Test methods increase and decrease gear.
   */
  @Test
  public void singleIncreaseDecreaseGear() {
    //Single increase and decrease the gear.
    for (int i = 0; i < 15; i++) {
      test2 = test2.increaseSpeed();
    }
    assertEquals(15, test2.getSpeed());
    assertEquals(1, test2.getGear());
    assertEquals("OK: you may increase the gear.", test2.getStatus());
    test2 = test2.increaseGear();
    assertEquals(15, test2.getSpeed());
    assertEquals(2, test2.getGear());
    assertEquals("OK: everything is OK.", test2.getStatus());
    test2 = test2.decreaseGear();
    assertEquals(15, test2.getSpeed());
    assertEquals(1, test2.getGear());
    assertEquals("OK: everything is OK.", test2.getStatus());
  }

  /**
   * Situations for status OKs.
   */
  @Test
  public void okSituations() {
    //Multiple increase speed without gear change.
    for (int i = 0; i < 11; i++) {
      test3 = test3.increaseSpeed();
    }
    assertEquals(11, test3.getSpeed());
    assertEquals(1, test3.getGear());
    assertEquals("OK: everything is OK.", test3.getStatus());

    //Multiple decrease speed without gear change.
    for (int i = 0; i < 7; i++) {
      test3 = test3.decreaseSpeed();
    }
    assertEquals(4, test3.getSpeed());
    assertEquals(1, test3.getGear());
    assertEquals("OK: everything is OK.", test3.getStatus());

    //The speed was increased successfully, but it is now within the range of the next gear
    for (int i = 0; i < 10; i++) {
      test3 = test3.increaseSpeed();
    }
    assertEquals(14, test3.getSpeed());
    assertEquals(1, test3.getGear());
    assertEquals("OK: you may increase the gear.", test3.getStatus());

    //Increase the gear without changing the speed.
    test3 = test3.increaseGear();
    assertEquals(14, test3.getSpeed());
    assertEquals(2, test3.getGear());
    assertEquals("OK: everything is OK.", test3.getStatus());

    //The speed was decreased successfully, but it is now within the range of the previous gear.
    test3 = test3.decreaseSpeed();
    assertEquals(13, test3.getSpeed());
    assertEquals(2, test3.getGear());
    assertEquals("OK: you may decrease the gear.", test3.getStatus());

    //Decrease the gear without changing the speed.
    test3 = test3.decreaseGear();
    assertEquals(13, test3.getSpeed());
    assertEquals(1, test3.getGear());
    assertEquals("OK: everything is OK.", test3.getStatus());
  }

  /**
   * Situations for situations cannots.
   */
  @Test
  public void cannot() {

    //Cannot decrease gear. Reached minimum gear.
    test4 = test4.decreaseGear();
    assertEquals(0, test4.getSpeed());
    assertEquals(1, test4.getGear());
    assertEquals("Cannot decrease gear. Reached minimum gear.", test4.getStatus());

    //Cannot decrease speed. Reached minimum speed.
    test4 = test4.decreaseSpeed();
    assertEquals(0, test4.getSpeed());
    assertEquals(1, test4.getGear());
    assertEquals("Cannot decrease speed. Reached minimum speed.", test4.getStatus());

    //Multiple increase the gear.
    for (int i = 0; i < 3; i++) {
      test4 = test4.increaseGear();
    }
    assertEquals(0, test4.getSpeed());
    assertEquals(1, test4.getGear());
    assertEquals("Cannot increase gear, increase speed first.", test4.getStatus());

    //Cannot increase speed, increase gear first.
    for (int i = 0; i < 18; i++) {
      test4 = test4.increaseSpeed();
    }
    assertEquals(16, test4.getSpeed());
    assertEquals(1, test4.getGear());
    assertEquals("Cannot increase speed, increase gear first.", test4.getStatus());

    //Cannot increase gear, increase speed first.
    for (int i = 0; i < 3; i++) {
      test4 = test4.increaseGear();
    }
    assertEquals(16, test4.getSpeed());
    assertEquals(2, test4.getGear());
    assertEquals("Cannot increase gear, increase speed first.", test4.getStatus());

    for (int i = 0; i < 19; i++) {
      test4 = test4.increaseSpeed();
    }
    test4 = test4.increaseGear();
    for (int i = 0; i < 9; i++) {
      test4 = test4.increaseSpeed();
    }
    test4 = test4.increaseGear();
    for (int i = 0; i < 6; i++) {
      test4 = test4.increaseSpeed();
    }
    test4 = test4.increaseGear();
    for (int i = 0; i < 2; i++) {
      test4 = test4.increaseSpeed();
    }
    assertEquals(52, test4.getSpeed());
    assertEquals(5, test4.getGear());
    assertEquals("OK: everything is OK.", test4.getStatus());

    test4 = test4.increaseSpeed();
    assertEquals(52, test4.getSpeed());
    assertEquals(5, test4.getGear());
    assertEquals("Cannot increase speed. Reached maximum speed.", test4.getStatus());

    test4 = test4.increaseGear();
    assertEquals(52, test4.getSpeed());
    assertEquals(5, test4.getGear());
    assertEquals("Cannot increase gear. Reached maximum gear.", test4.getStatus());

    //Multiple decrease the gear.
    for (int i = 0; i < 3; i++) {
      test4 = test4.decreaseGear();
    }
    assertEquals(52, test4.getSpeed());
    assertEquals(5, test4.getGear());
    assertEquals("Cannot decrease gear, decrease speed first.", test4.getStatus());

    //Cannot decrease gear, decrease speed first.
    for (int i = 0; i < 2; i++) {
      test4 = test4.decreaseSpeed();
    }
    for (int i = 0; i < 3; i++) {
      test4 = test4.decreaseGear();
    }
    assertEquals(50, test4.getSpeed());
    assertEquals(4, test4.getGear());
    assertEquals("Cannot decrease gear, decrease speed first.", test4.getStatus());

    //Cannot decrease speed, decrease gear first.
    for (int i = 0; i < 13; i++) {
      test4 = test4.decreaseSpeed();
    }
    assertEquals(39, test4.getSpeed());
    assertEquals(4, test4.getGear());
    assertEquals("Cannot decrease speed, decrease gear first.", test4.getStatus());
  }
}