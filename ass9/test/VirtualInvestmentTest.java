import org.junit.Before;
import org.junit.Test;

import java.io.File;

import virtualinvestment.MyCalendar;
import virtualinvestment.model.InvestmentModel;
import virtualinvestment.model.VirtualInvestment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class VirtualInvestmentTest {
  private VirtualInvestment investment;

  /**
   * Initialize some fields of tests.
   */
  @Before
  public void setUp() {
    investment = new InvestmentModel();

    investment.createPortfolio("Internet");
    investment.createPortfolio("File");
    investment.createPortfolio("Manual");

    investment.addDataFromFile("APPLE", new File("APPLE.txt"));
    MyCalendar[] times = {new MyCalendar("2016-05-25-12")};
    double[] prices = {1923};
    investment.addDataByManual("MNL", times, prices);
  }

  @Test
  public void addAndBuyStockFromInternet() {
    investment.addDataFromInternet("GOOG");
    investment.addStock("Internet", "GOOG");
    investment.buyStock("Internet", "GOOG", 10, 0, new MyCalendar("2014-03-27-16"));
    String result = investment.getPurchaseHistory("Internet", new MyCalendar("2016-05-27-16"));
    assertEquals("Stock Name: GOOG  Cost Basis: 558.46  Shares: 10.00\n", result);

    //with commission fee
  }

  @Test
  public void addFromInternetDuplicateStock() {
    //already exist a stock with given name
    investment.addDataFromInternet("GOOG");
    try {
      investment.addDataFromInternet("GOOG");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock name is already exist.\n", e.getMessage());
    }
  }

  @Test
  public void addStockFromFile() {
    investment.addDataFromFile("ANOTHER", new File("APPLE.txt"));
    investment.addStock("File", "ANOTHER");
    investment.buyStock("File", "ANOTHER", 10, 0, new MyCalendar("2016-05-27-9"));
    String result = investment.getPurchaseHistory("File", new MyCalendar("2016-05-27-16"));
    assertEquals("Stock Name: ANOTHER  Cost Basis: 724.01  Shares: 10.00\n", result);

    //with commission fee
  }

  @Test
  public void addStockDuplicateStock() {
    //already exist a stock with given name
    try {
      investment.addDataFromFile("APPLE", new File("APPLE.txt"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock name is already exist.\n", e.getMessage());
    }
  }

  @Test
  public void addStockByManual() {
    MyCalendar[] times = {new MyCalendar("2016-06-15-12")};
    double[] prices = {1923};
    investment.addDataByManual("ANOTHER", times, prices);
    investment.addStock("Manual", "ANOTHER");
    try {
      investment.buyStock("Manual", "ANOTHER", 100, 0,
              new MyCalendar("2016-06-15-12"));
      assertEquals("Stock Name: ANOTHER  Cost Basis: 1923.0  Shares: 100.00\n",
              investment.getPurchaseHistory("Manual", new MyCalendar("2016-06-15-12")));
    } catch (IllegalStateException e) {
      fail();
    }

  }

  @Test
  public void addStockByManualDuplicateStock() {
    //already exist a stock with given name
    MyCalendar[] times = {new MyCalendar("2016-05-25-12")};
    double[] prices = {1923};
    try {
      investment.addStock("File", "MNL");
      investment.addDataByManual("MNL", times, prices);
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock name is already exist.\n", e.getMessage());
    }
  }

  @Test
  public void createPortfolio() {
    //success add a portfolio
    investment.createPortfolio("p1");
    try {
      String result = investment.getPurchaseHistory("p1", new MyCalendar("2016-05-25-12"));
      assertEquals("This portfolio is empty.\n", result);
    } catch (IllegalStateException e) {
      fail();
    }
  }

  @Test
  public void createPortfolioWithNames() {
    //use createPortfolio(String pName, String[] sNames)
  }

  @Test
  public void createPortfolioNonExistStock() {
    //the stock to add does not exist
  }

  @Test
  public void addStock() {
    //add stock success
  }

  @Test
  public void addStockDuplicateName() {
    //the stock is already exist in portfolio
  }

  @Test
  public void addStockNonExistName() {
    //non existent portfolio name

    //non existent stock name
  }


  @Test
  public void createPortfolioDuplicateName() {
    //already exist a portfolio with given name
    try {
      investment.createPortfolio("File");
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Creation failed: Portfolio name already exists.\n", e.getMessage());
    }
  }

  @Test
  public void getPurchaseHistoryOfEmptyPortfolio() {
    //empty portfolio
    assertEquals("This portfolio is empty.\n",
            investment.getPurchaseHistory("Manual", new MyCalendar("2016-05-25-12")));
  }

  @Test
  public void getPurchaseHistoryBoughtSameTime() {
    //different stocks in a portfolio are bought at the same time, they are listed separately
    investment.addStock("File", "APPLE");
    investment.addStock("File", "MNL");
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "MNL", 200, 0,
            new MyCalendar("2016-05-25-12"));
    String result = investment.getPurchaseHistory("File", new MyCalendar("2016-06-27-12"));
    assertEquals("Stock Name: APPLE  Cost Basis: 675.17  Shares: 17.00\n"
            + "Stock Name: MNL  Cost Basis: 1923.0  Shares: 200.00\n", result);
  }

  @Test
  public void getPurchaseHistoryNoData() {
    //no data found when apply a strategy
  }

  @Test
  public void getPurchaseHistoryBoughtDifferentTime() {
    //user buy the same stock with different cost basis, they can be listed separately
    investment.addStock("File", "APPLE");
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2018-10-2-16"));
    //al history
    String result1 = investment.getPurchaseHistory("File", new MyCalendar("2018-10-2-16"));
    assertEquals("Stock Name: APPLE  Cost Basis: 1200.11  Shares: 17.00\n"
            + "Stock Name: APPLE  Cost Basis: 675.17  Shares: 17.00\n", result1);

    //part of history
    String result2 = investment.getPurchaseHistory("File", new MyCalendar("2018-8-2-12"));
    assertEquals("Stock Name: APPLE  Cost Basis: 675.17  Shares: 17.00\n", result2);
  }

  @Test

  public void getPurchaseHistorySameCostBasisMerge() {
    investment.addStock("File", "APPLE");
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "APPLE", 13, 0,
            new MyCalendar("2016-06-24-09"));
    String result = investment.getPurchaseHistory("File", new MyCalendar("2016-06-24-17"));
    assertEquals("Stock Name: APPLE  Cost Basis: 675.17  Shares: 30.00\n", result);
  }

  @Test
  public void buyStock() {
    investment.addStock("File", "APPLE");
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2016-06-24-09"));
    String result1 = investment.getPurchaseHistory("File", new MyCalendar("2016-06-25-09"));
    assertEquals("Stock Name: APPLE  Cost Basis: 675.17  Shares: 17.00\n", result1);

    investment.buyStock("File", "APPLE", 110, 0,
            new MyCalendar("2016-07-13-16"));
    String result2 = investment.getPurchaseHistory("File", new MyCalendar("2016-08-24-09"));
    assertEquals("Stock Name: APPLE  Cost Basis: 675.17  Shares: 17.00\n"
            + "Stock Name: APPLE  Cost Basis: 716.98  Shares: 110.00\n", result2);
  }

  @Test
  public void buyNonExistPortfolio() {
    //given portfolio name does not exist
    try {
      investment.buyStock("nonExistPortfolio", "APPLE", 15, 0,
              new MyCalendar("2016-05-25-12"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Portfolio name does not exist.\n", e.getMessage());
    }
  }

  @Test
  public void buyNonExistStock() {
    //given stock does not exist
    try {
      investment.buyStock("Internet", "GOOG", 15, 0,
              new MyCalendar("2016-05-25-12"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Stock does not exist.\n", e.getMessage());
    }
  }

  @Test
  public void buyStockNotBelongToPort() {
    //the stock does not belong to given portfolio
  }


  @Test
  public void buyCloseTime() {
    //buying time is weekend
    try {
      investment.addStock("File", "APPLE");
      investment.buyStock("File", "APPLE", 2, 0,
              new MyCalendar("2018-11-11-16"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: This time is weekend.\n", e.getMessage());
    }

    //buying time is after-hours
    try {
      investment.buyStock("File", "APPLE", 2, 0,
              new MyCalendar("2018-11-1-1"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: This time is before 9 am.\n", e.getMessage());
    }

    //buying time is before this stock exist
    try {
      investment.buyStock("File", "APPLE", 2, 0,
              new MyCalendar("2013-11-1-09"));
      fail();
    } catch (IllegalStateException e) {
      assertEquals("Operation failed: Miss price data.\n", e.getMessage());
    }
  }

  @Test
  public void invest() {
    //invest successfully
    investment.addDataFromInternet("VZ");
    investment.addStock("Internet", "APPLE");
    investment.addStock("Internet", "VZ");
    //System.out.println(investment.getPurchaseHistory("Internet", new MyCalendar("2018-09-09-09")));
    double[] weights = {0.4, 0.6};
    investment.invest("Internet", new MyCalendar("2014-4-1-09"), 100, 5.0, weights);
    //System.out.println(investment.getPurchaseHistory("Internet", new MyCalendar("2018-09-09-09")));
    String result = investment.getPurchaseHistory("Internet", new MyCalendar("2018-09-09-09"));
    assertEquals("Stock Name: APPLE  Unit Price: " + 558.71 + "  Shares: " + String.format("%.2f", 100 * 0.4 / 558.71) + "\n" +
            "Stock Name: VZ  Unit Price: " + 47.54 + "  Shares: " + String.format("%.2f", 100 * 0.6 / 47.54) + "\n", result);

    //invest with 0 weight

    //invest with equal weights
  }

  @Test
  public void investNonExistPort() {
    //portfolio does not exist
  }

  @Test
  public void investClose() {
    // stock market is close at invest time
  }

  @Test
  public void investMissData() {
    //price data missed at buying time
  }

  @Test
  public void dollarCostAveraging() {
    //apply strategy
    investment.addDataFromInternet("VZ");
    investment.addStock("Internet", "APPLE");
    investment.addStock("Internet", "VZ");
    double[] weights = {0.4, 0.6};
    investment.dollarCostAveraging("Internet", new MyCalendar("2014-4-1-16")
            , new MyCalendar("2018-4-1-09"), 2, 100, 5.0, weights);
    //first day
    /*String result1 = investment.getPurchaseHistory("Internet", new MyCalendar("2014-4-1-16"));
    System.out.println(result1);*/

    //before buy
    /*String result2 = investment.getPurchaseHistory("Internet", new MyCalendar("2014-4-1-09"));
    System.out.println(result2);*/

    //buy several times
    String result3 = investment.getPurchaseHistory("Internet", new MyCalendar("2014-4-7-09"));
    System.out.println(result3);
    assertEquals("Stock Name: APPLE  Unit Price: " + 538.15 + "  Shares: " + String.format("%.2f", 100 * 0.4 / 538.15)+ "\n"
            + "Stock Name: APPLE  Unit Price: 567.16  Shares: 0.07\n"
            + "Stock Name: APPLE  Unit Price: 569.74  Shares: 0.07\n"
            + "Stock Name: VZ  Unit Price: 47.75  Shares: 1.26\n"
            + "Stock Name: VZ  Unit Price: 48.12  Shares: 2.49\n", result3);
    double cost1 = investment.getCostBasis("Internet", new MyCalendar("2014-4-7-09"));
    assertEquals(100.0 * 3 + 5.0 * 6, cost1, 0.01);
    double value1 = investment.getTotalValue("Internet", new MyCalendar("2014-4-7-09"));
    assertEquals(0.07 * 3 * 540.74 + (1.26 + 2.49) * 47.7, value1, 0.01);

    //getPurchaseHistory

    //getCostBasis

    //getTotalValue
  }

  @Test
  public void getCostBasis() {
    investment.addStock("File", "APPLE");
    investment.addStock("File", "MNL");
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "MNL", 200, 0,
            new MyCalendar("2016-05-25-12"));
    investment.buyStock("File", "APPLE", 200, 0,
            new MyCalendar("2016-10-12-16"));
    // a time before buying
    double result1 = investment.getCostBasis("File", new MyCalendar("2016-04-24-08"));
    assertEquals(0.0, result1, 0);

    //time equal buying time
    double result2 = investment.getCostBasis("File", new MyCalendar("2016-06-24-09"));
    assertEquals(675.1700 * 17 + 1923 * 200, result2, 0);

    //time after buying time
    double result3 = investment.getCostBasis("File", new MyCalendar("2016-12-28-11"));
    assertEquals(675.1700 * 17 + 1923 * 200 + 786.1400 * 200, result3, 0);
  }

  @Test
  public void getCostBasisMissData() {
    //price data at time strategies apply is missed
  }

  @Test
  public void getCostBasisNonExistPort() {

  }

  @Test
  public void getTotalValue() {
    investment.addStock("File", "APPLE");
    investment.addStock("File", "MNL");
    investment.buyStock("File", "APPLE", 17, 0,
            new MyCalendar("2016-06-24-09"));
    investment.buyStock("File", "APPLE", 200, 0,
            new MyCalendar("2016-10-12-16"));

    //a time before buy
    double result1 = investment.getTotalValue("File", new MyCalendar("2016-05-24-07"));
    assertEquals(0.0, result1, 0);
    //time after buying
    //time after to buying time
    double result2 = investment.getTotalValue("File", new MyCalendar("2016-06-24-09"));
    assertEquals(675.1700 * 17, result2, 0);

    //time after buying time
    double result3 = investment.getTotalValue("File", new MyCalendar("2016-10-13-16"));
    assertNotEquals(result2, result3);
    assertEquals(778.1900 * (200 + 17), result3, 0);
  }

  @Test
  public void getTotalValueMissData() {
    //price data at time strategies apply is missed
  }

  @Test
  public void getTotalValueNonExistPort() {

  }

  @Test
  public void addStockApplyAnotherDCA() {
    //apply a strategy
    //then add a new stock to port
    //apply a new strategy

    //get purchase history
  }

  @Test
  public void comboInvestment() {
    //combination of investment
  }
}