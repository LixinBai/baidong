/*
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import virtualinvestment.MyCalendar;
import virtualinvestment.controller.IVController;
import virtualinvestment.controller.VController;
import virtualinvestment.model.InvestmentModel;
import virtualinvestment.model.VirtualInvestment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class VControllerTest {
  private Reader in;
  private StringBuffer out;
  private StringBuilder log;
  private VirtualInvestment model;
  private IVController controller;

  */
/**
   * Initialize some fields of tests.
   *//*

  @Before
  public void setUp() {
    in = new StringReader("");
    out = new StringBuffer();
    log = new StringBuilder();
  }

  @Test
  public void testCalendar() {
    String[] invalid = {"2018-13-1-9", "2018-1-32-9", "2018-1-1-24", "2020-1-1-9"};
    for (String in : invalid) {
      try {
        MyCalendar my1 = new MyCalendar(in);
        fail();
      } catch (IllegalArgumentException e) {
        assertEquals("Operation failed: Invalid date.\n", e.getMessage());
      }
    }
  }


  @Test
  public void testSplit() {
    String s = "A B C ";
    String str [] = s.split(" ");
    System.out.println(str.length);
  }

  @Test
  public void testConstructException() {
    try {
      controller = new VController(null, out);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Readable not found.\n", e.getMessage());
    }
    try {
      controller = new VController(in, null);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Appendable not found.\n", e.getMessage());
    }
  }

  @Test
  public void testNoModelException() {
    try {
      controller = new VController(in, out);
      controller.start(null);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("Model not found.\n", e.getMessage());
    }
  }

  @Test
  public void testCreatePort() {
    in = new StringReader("createPort PortA");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Create a portfolio named PortA.\n", log.toString());
    assertEquals("Please input: portfolio name.\nCreated portfolio PortA successfully.\n",
            out.toString());
  }

  @Test
  public void testCreatePortFail() {
    in = new StringReader("createPort PortA");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: portfolio name.\nCreation failed message.\n",
            out.toString());
  }

  @Test
  public void testViewPort() {
    in = new StringReader("viewPort PortA");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("View portfolio PortA.\n", log.toString());
    assertEquals("Please input: portfolio name.\nContent of PortA.\n", out.toString());
  }

  @Test
  public void testViewPortFail() {
    in = new StringReader("viewPort PortA");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: portfolio name.\nViewing portfolio failed message.\n",
            out.toString());
  }


  @Test
  public void testAddStockFromInternet() {
    in = new StringReader("addStock internet apple");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Add a stock apple from internet.\n", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
                    + "Please input: stock name.\nAdd successfully.\n",
            out.toString());
  }

  @Test
  public void testAddStockFromInternetFail() {
    in = new StringReader("addStock internet apple");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
                    + "Please input: stock name.\nAdding stock from internet failed message.\n",
            out.toString());
  }

  @Test
  public void testAddStockFromFile() {
    in = new StringReader("addStock file apple somePath");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Add a stock apple from file somePath.\n", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
                    + "Please input: stock name + file path.\nAdd successfully.\n",
            out.toString());
  }

  @Test
  public void testAddStockFromFileFail() {
    in = new StringReader("addStock file apple somePath");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + file path.\n"
            + "Adding stock from file failed message.\n", out.toString());
  }

  @Test
  public void testAddStockByManual() {
    in = new StringReader("addStock manual apple 3 2018-10-1-9 10.00 2018-10-1-10 12.00 "
            + "2018-10-1-11 13.00");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Add a stock apple by manual.\n2018-10-1-9 : 10.00\n"
            + "2018-10-1-10 : 12.00\n2018-10-1-11 : 13.00\n", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Add successfully.\n", out.toString());
  }

  @Test
  public void testAddStockByManualFail() {
    in = new StringReader("addStock manual apple 2 2018-10-1-9 10.00 2018-10-1-10 12.00");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Adding stock by manual failed message.\n", out.toString());
  }

  @Test
  public void testAddStockByManualBadInput() {
    Reader in1 = new StringReader("addStock manual apple 0");
    Reader in2 = new StringReader("addStock manual apple 1 "
            + "2018-10-1-9 10.00 2018-10-1-10 12.00");
    Reader in3 = new StringReader("addStock manual apple 2 2018-10-1-9 10.00");
    Reader in4 = new StringReader("addStock manual apple badinput");
    Reader in5 = new StringReader("addStock manual apple 1 badinput");
    Reader in6 = new StringReader("addStock manual apple 1 2018-10-1-9 badinput");

    model = new MockModel1(log, 0);
    controller = new VController(in1, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Operation failed: Number of price should be positive.\n", out.toString());

    out = new StringBuffer();
    log = new StringBuilder();
    model = new MockModel1(log, 0);
    controller = new VController(in2, out);
    controller.start(model);
    assertEquals("Add a stock apple by manual.\n2018-10-1-9 : 10.00\n", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Add successfully.\n"
            + "Command not found: Please try again.\n"
            + "Command not found: Please try again.\n", out.toString());

    out = new StringBuffer();
    log = new StringBuilder();
    model = new MockModel1(log, 0);
    controller = new VController(in3, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Operation failed: Miss price input.\n", out.toString());

    out = new StringBuffer();
    log = new StringBuilder();
    model = new MockModel1(log, 0);
    controller = new VController(in4, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Operation failed: Invalid number of price.\n", out.toString());

    out = new StringBuffer();
    log = new StringBuilder();
    model = new MockModel1(log, 0);
    controller = new VController(in5, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Operation failed: Invalid date format.\n", out.toString());

    out = new StringBuffer();
    log = new StringBuilder();
    model = new MockModel1(log, 0);
    controller = new VController(in6, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Operation failed: Invalid price.\n", out.toString());
  }

  @Test
  public void testAddStockBadInput() {
    in = new StringReader("addStock badinput");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input type of source data: internet/file/manual.\n"
            + "Operation failed: Invalid type of stock data.\n", out.toString());
  }

  @Test
  public void testBuyStock() {
    in = new StringReader("buyStock apple portA 100 2018-10-1-14");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Buy 100.00 shares of apple in portfolio portA at 2018-10-1-14.\n",
            log.toString());
    assertEquals("Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\nBuy successfully.\n", out.toString());
  }

  @Test
  public void testBuyStockFail() {
    in = new StringReader("buyStock apple portA 100 2018-10-1-14");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: stock name + portfolio name + number of shares"
            + " + date(yyyy-mm-dd-hh).\n"
            + "Buying stock failed message.\n", out.toString());
  }

  @Test
  public void testBuyStockBadInput() {
    Reader in1 = new StringReader("buyStock apple portA badInput 2018-10-1-14");
    Reader in2 = new StringReader("buyStock apple portA 100 baiInput");

    model = new MockModel1(log, 0);
    controller = new VController(in1, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\n"
            + "Operation failed: Invalid number of shares.\n", out.toString());

    out = new StringBuffer();
    log = new StringBuilder();
    model = new MockModel1(log, 0);
    controller = new VController(in2, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\n"
            + "Operation failed: Invalid date format.\n", out.toString());
  }

  @Test
  public void testGetCostBasis() {
    in = new StringReader("getCostBasis portA 2018-10-1-14");
    model = new MockModel1(log, 12.23);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Get cost basis of portfolio portA at 2018-10-1-14.\n",
            log.toString());
    assertEquals("Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "The cost basis is: 12.23.\n", out.toString());
  }

  @Test
  public void testGetCostBasisFail() {
    in = new StringReader("getCostBasis portA 2018-10-1-14");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "Getting cost basis failed message.\n", out.toString());
  }

  @Test
  public void testGetTotalValue() {
    in = new StringReader("getTotalValue portA 2018-10-1-14");
    model = new MockModel1(log, 19.96);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Get total value of portfolio portA at 2018-10-1-14.\n",
            log.toString());
    assertEquals("Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "The total value is: 19.96.\n", out.toString());
  }

  @Test
  public void testGetTotalValueFail() {
    in = new StringReader("getTotalValue portA 2018-10-1-14");
    model = new MockModel2();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "Getting total value failed message.\n", out.toString());
  }

  @Test
  public void testQuit() {
    Reader in = new StringReader("quit");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Already quit the program.\n", out.toString());
  }

  @Test
  public void testCommandNotFound() {
    Reader in = new StringReader("badCommand");
    model = new MockModel1(log, 0);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("", log.toString());
    assertEquals("Command not found: Please try again.\n", out.toString());
  }

  @Test
  public void missInput() {
    Reader[] in = {new StringReader("createPort"),
            new StringReader("addStock"),
            new StringReader("addStock internet"),
            new StringReader("addStock file apple"),
            new StringReader("addStock manual apple"),
            new StringReader("buyStock"),
            new StringReader("buyStock apple"),
            new StringReader("buyStock apple portA"),
            new StringReader("buyStock apple portA 100"),
            new StringReader("viewPort"),
            new StringReader("getCostBasis"),
            new StringReader("getCostBasis portA"),
            new StringReader("getTotalValue"),
            new StringReader("getTotalValue portA")};
    model = new MockModel1(log, 0);
    for (Reader r : in) {
      try {
        out = new StringBuffer();
        log = new StringBuilder();
        controller = new VController(r, out);
        controller.start(model);
        fail();
      } catch (IllegalStateException e) {
        assertEquals("", log.toString());
        assertEquals("The Readable object failed: Miss input.\n", e.getMessage());
      }
    }
  }

  @Test
  public void testIOException() {
    model = new MockModel1(log, 0);
    controller = new VController(in, new IOEAppendable());
    try {
      controller.start(model);
    } catch (IllegalStateException e) {
      assertEquals("The Appendable object failed.", e.getMessage());
    }
  }

  @Test
  public void testFullProcessWithMockModel() {
    Reader in = new StringReader("createPort PortA addStock internet apple addStock file startogle "
            + "somePath addStock manual amazon 2 2018-10-1-9 10.00 2018-10-1-10 12.00 buyStock "
            + "apple portA 100 2018-10-1-14 viewPort PortA getCostBasis PortA  2018-11-1-9 "
            + "getTotalValue portA 2018-11-1-9 quit");
    model = new MockModel1(log, 13.33);
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Create a portfolio named PortA.\n"
            + "Add a stock apple from internet.\n"
            + "Add a stock startogle from file somePath.\n"
            + "Add a stock amazon by manual.\n"
            + "2018-10-1-9 : 10.00\n"
            + "2018-10-1-10 : 12.00\n"
            + "Buy 100.00 shares of apple in portfolio portA at 2018-10-1-14.\n"
            + "View portfolio PortA.\n"
            + "Get cost basis of portfolio PortA at 2018-11-1-9.\n"
            + "Get total value of portfolio portA at 2018-11-1-9.\n", log.toString());
    assertEquals("Please input: portfolio name.\n"
            + "Created portfolio PortA successfully.\n"
            + "Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name.\n"
            + "Add successfully.\n"
            + "Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + file path.\n"
            + "Add successfully.\n"
            + "Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Add successfully.\n"
            + "Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\n"
            + "Buy successfully.\n"
            + "Please input: portfolio name.\n"
            + "Content of PortA.\n"
            + "Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "The cost basis is: 13.33.\n"
            + "Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "The total value is: 13.33.\n"
            + "Already quit the program.\n", out.toString());
  }


  @Test
  public void testFullProcessWithInvestmentModel() {
    Reader in = new StringReader("createPort PortA addStock internet startOG addStock file APPLE "
            + "APPLE.txt addStock manual AMAZON 2 2018-10-1-9 1000.00 2018-11-1-9 900.00 buyStock "
            + "APPLE PortA 100 2018-10-1-14  buyStock APPLE PortA 50 2018-10-2-14 buyStock AMAZON "
            + "PortA 50 2018-10-1-9 viewPort PortA getCostBasis PortA  2018-11-1-9 getTotalValue "
            + "PortA 2018-11-1-9 quit");
    model = new InvestmentModel();
    controller = new VController(in, out);
    controller.start(model);
    assertEquals("Please input: portfolio name.\n"
            + "Created portfolio PortA successfully.\n"
            + "Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name.\n"
            + "Add successfully.\n"
            + "Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + file path.\n"
            + "Add successfully.\n"
            + "Please input type of source data: internet/file/manual.\n"
            + "Please input: stock name + number of price + (yyyy-mm-dd-hh + price)*n.\n"
            + "Add successfully.\n"
            + "Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\n"
            + "Buy successfully.\n"
            + "Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\n"
            + "Buy successfully.\n"
            + "Please input: stock name + portfolio name + number of shares "
            + "+ date(yyyy-mm-dd-hh).\n"
            + "Buy successfully.\n"
            + "Please input: portfolio name.\n"
            + "Stock Name: AMAZON  Cost Basis: 1000.0  Shares: 50\n"
            + "Stock Name: APPLE  Cost Basis: 1190.96  Shares: 50\n"
            + "Stock Name: APPLE  Cost Basis: 1199.89  Shares: 100\n"
            + "Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "The cost basis is: 229537.0.\n"
            + "Please input: portfolio name + date(yyyy-mm-dd-hh).\n"
            + "The total value is: 206370.0.\n"
            + "Already quit the program.\n", out.toString());
  }

  */
/**
   * A appendable which will throw an IOException if append is called.
   *//*

  class IOEAppendable implements Appendable {
    @Override
    public Appendable append(CharSequence csq) throws IOException {
      throw new IOException();
    }

    @Override
    public Appendable append(CharSequence csq, int start, int end) throws IOException {
      throw new IOException();
    }

    @Override
    public Appendable append(char c) throws IOException {
      throw new IOException();
    }
  }

  //share是否是正数

}*/
