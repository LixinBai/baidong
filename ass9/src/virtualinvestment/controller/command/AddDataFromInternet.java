package virtualinvestment.controller.command;

import java.io.IOException;

import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the add stock from internet command.
 */
public class AddDataFromInternet implements Command {
  private String name;
  private final Appendable ap;

  /**
   * Initialize an add stock from internet command.
   *
   * @param name the name of the stock to add
   * @param ap   the output
   */
  public AddDataFromInternet(String name, Appendable ap) {
    this.name = name;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      m.addDataFromInternet(name);
      ap.append("Add data successfully.\n");
    } catch (IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
