package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.GregorianCalendar;

import virtualinvestment.MyCalendar;
import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the buy stock command.
 */
public class BuyStock extends AbstractCommand {
  private String pName;
  private String sName;
  private String shares;
  private String fee;
  private String date;
  private final Appendable ap;

  /**
   * Initialize a buy stock command.
   *
   * @param pName  the name of the portfolio to buy in
   * @param sName  the name of the stock to buy
   * @param shares the shares of the stock to buy
   * @param date   the date to buy the stock
   * @param ap     the output
   */
  public BuyStock(String pName, String sName, String shares, String fee, String date, Appendable ap) {
    this.pName = pName;
    this.sName = sName;
    this.shares = shares;
    this.fee = fee;
    this.date = date;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      double shares = getDouble("number of shares", this.shares,false);
      MyCalendar cal = new MyCalendar(date);
      if (cal.after(GregorianCalendar.getInstance())) {
        ap.append("Operation failed: Date should be earlier than current date.\n");
        return;
      }
      double fee = getDouble("commission fee", this.fee,true);
      m.buyStock(pName, sName, shares, fee, cal);
      ap.append("Buy successfully.\n");
    } catch (IllegalArgumentException | IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
