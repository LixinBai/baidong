package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.GregorianCalendar;

import virtualinvestment.MyCalendar;
import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the get cost basis command.
 */
public class GetCostBasis implements Command {
  private String name;
  private String date;
  private final Appendable ap;


  /**
   * Initialize a get cost basis command.
   *
   * @param name the name of the portfolio to get cost basis
   * @param date the date to get cost basis
   * @param ap   the output
   */
  public GetCostBasis(String name, String date, Appendable ap) {
    this.name = name;
    this.date = date;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      MyCalendar cal = new MyCalendar(date);
      if (cal.after(GregorianCalendar.getInstance())) {
        ap.append("Operation failed: Date should be earlier than current date.\n");
        return;
      }
      double costBasis = m.getCostBasis(name, cal);
      ap.append("The cost basis is: ").append(String.valueOf(costBasis)).append(".\n");
    } catch (IllegalArgumentException | IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
