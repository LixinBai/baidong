package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the create port command.
 */
public class CreatePort extends AbstractCommand {
  private String pName;
  private String num;
  private Scanner scan;
  private final Appendable ap;


  /**
   * Initialize a create port command.
   *
   * @param pName the name of the portfolio to create
   * @param ap    the output
   */
  public CreatePort(String pName, String num, Scanner scan, Appendable ap) {
    this.pName = pName;
    this.num = num;
    this.scan = scan;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      int number = getInt("number of stocks", num,0);
      if (number == 0) {
        m.createPortfolio(pName);
        ap.append("Portfolio ").append(pName).append(" created successfully.\n");
      } else {
        String[] stocks = new String[number];
        for (int i = 0; i < stocks.length; i++) {
          stocks[i] = scan.next();
        }
        m.createPortfolio(pName, stocks);
        ap.append("Portfolio ").append(pName).append(" created successfully.\n");
      }
    } catch (NoSuchElementException e0) {
      ap.append("Operation failed: Miss stock name.\n");
    } catch (IllegalArgumentException | IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
