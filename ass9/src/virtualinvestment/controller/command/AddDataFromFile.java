package virtualinvestment.controller.command;

import java.io.File;
import java.io.IOException;

import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the add stock from file command.
 */
public class AddDataFromFile implements Command {
  private String name;
  private String file;
  private final Appendable ap;

  /**
   * Initialize an add stock from file command.
   *
   * @param name the name of the stock to add
   * @param file the file of the stock to add
   * @param ap   the output
   */
  public AddDataFromFile(String name, String file, Appendable ap) {
    this.name = name;
    this.file = file;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      m.addDataFromFile(name, new File(file));
      ap.append("Add data successfully.\n");
    } catch (IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }


}
