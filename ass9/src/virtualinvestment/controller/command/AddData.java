package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Function;

import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the add stock command.
 */
public class AddData implements Command {
  private Scanner scan;
  private Appendable ap;

  /**
   * Initialize an add stock command.
   *
   * @param scan the rest input required of this command
   * @param ap   the output
   */
  public AddData(Scanner scan, Appendable ap) {
    this.scan = scan;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IllegalStateException, IOException {
    Map<String, Function<Scanner, Command>> knownCommands = new HashMap<>();
    Map<String, String> messages = new HashMap<>();
    knownCommands.put("internet", s -> new AddDataFromInternet(s.next(), ap));
    messages.put("internet", "Please input: stock name.\n");
    knownCommands.put("file", s -> new AddDataFromFile(s.next(), s.next(), ap));
    messages.put("file", "Please input: stock name + file path.\n");
    knownCommands.put("manual", s -> new AddDataByManual(s.next(), s.next(), s, ap));
    messages.put("manual", "Please input: stock name + number of price\n");
    Command c;
    String in = scan.next();
    if (in.equalsIgnoreCase("q") || in.equalsIgnoreCase("quit")) {
      return;
    }
    String message = messages.getOrDefault(in, null);
    Function<Scanner, Command> cmd = knownCommands.getOrDefault(in, null);
    if (cmd == null) {
      ap.append("Operation failed: Invalid type of stock data.\n");
    } else {
      try {
        ap.append(message);
        c = cmd.apply(scan);
        c.start(m);
      } catch (NoSuchElementException e) {
        throw new IllegalStateException("Operation failed: Miss input.\n");
      }
    }
  }
}


