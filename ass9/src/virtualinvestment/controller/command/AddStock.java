package virtualinvestment.controller.command;

import java.io.IOException;

import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

public class AddStock implements Command {
  private String pName;
  private String sName;
  private final Appendable ap;

  public AddStock(String pName, String sName, Appendable ap) {
    this.pName = pName;
    this.sName = sName;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IllegalStateException, IOException {
    try {
      m.addStock(pName, sName);
      ap.append("Add stock successfully.\n");
    } catch (IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }
}
