package virtualinvestment.controller.command;

import java.io.IOException;

import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

public class ViewPort implements Command {
  private String name;
  private final Appendable ap;

  /**
   * Initialize a view port command.
   *
   * @param name the name of the portfolio to view
   * @param ap   the output
   */
  public ViewPort(String name, Appendable ap) {
    this.name = name;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IllegalStateException, IOException {
    try {
      String stocks = m.viewPortfolio(name);
      ap.append("Portfolio ").append(name).append(" contains: ").append(stocks).append(".\n");
    } catch (IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }
}
