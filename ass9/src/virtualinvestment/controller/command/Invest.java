package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.Scanner;

import virtualinvestment.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

public class Invest extends AbstractCommand {
  protected String name;
  private String date;
  protected String amount;
  protected String fee;
  protected Scanner scan;
  protected final Appendable ap;

  public Invest(String name, String date, String amount, String fee, Scanner scan, Appendable ap) {
    this.name = name;
    this.date = date;
    this.amount = amount;
    this.fee = fee;
    this.scan = scan;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IllegalStateException, IOException {
    try {
      MyCalendar cal = new MyCalendar(date);
      double amount = getDouble("amount to invest", this.amount,false);
      double fee = getDouble("commission fee", this.fee,true);
      if (cal.after(GregorianCalendar.getInstance())) {
        ap.append("Operation failed: Date should be earlier than current date.\n");
        return;
      }
      m.invest(name, cal, amount, fee, getWeight(m));
      ap.append("Invest successfully.\n");
    } catch (NoSuchElementException e0) {
      ap.append("Operation failed: Miss weight.\n");
    } catch (IllegalArgumentException | IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }
  private double[] equal(int num) {
    double[] weight = new double[num];
    double total = 0;
    for (int i = 0; i < num - 1; i++) {
      weight[i] = 1 / (double) num;
      total = total + weight[i];
    }
    weight[num - 1] = 1 - total;
    return weight;
  }

  protected double [] getWeight(VirtualInvestment m) throws
          NoSuchElementException,IllegalStateException ,IllegalArgumentException,IOException{
    String stocks;
    stocks = m.viewPortfolio(name);
    ap.append("Please input \"equal\" or weights of ").append(stocks)
            .append("(Total weights should add up to 1.):\n");
    String sign = scan.next();
    int num = stocks.split(" ").length;
    if (sign.equals("equal")) {
      return equal(num);
    }
    double[] weights = new double[num];
    weights[0] = getDouble("number of weight", sign,true);
    double total = weights[0];
    for (int i = 1; i < num; i++) {
      weights[i] = getDouble("number of weight", scan.next(),true);
      total = total + weights[i];
    }
    if (total != 1) {
      throw new IllegalArgumentException("Operation failed: Total weights should add up to 1.\n");
    }
    return weights;
  }

}
