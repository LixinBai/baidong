package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.GregorianCalendar;

import virtualinvestment.MyCalendar;
import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the get total value command.
 */
public class GetTotalValue implements Command {
  private String name;
  private String date;
  private final Appendable ap;

  /**
   * Initialize a get total value command.
   *
   * @param name the name of the portfolio to get total value
   * @param date the date to get total value
   * @param ap   the output
   */
  public GetTotalValue(String name, String date, Appendable ap) {
    this.name = name;
    this.date = date;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      MyCalendar cal = new MyCalendar(date);
      if (cal.after(GregorianCalendar.getInstance())) {
        ap.append("Operation failed: Date should be earlier than current date.\n");
        return;
      }
      double totalValue = m.getTotalValue(name, cal);
      ap.append("The total value is: ").append(String.valueOf(totalValue)).append(".\n");
    } catch (IllegalArgumentException | IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
