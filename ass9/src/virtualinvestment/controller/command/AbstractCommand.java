package virtualinvestment.controller.command;

import virtualinvestment.controller.Command;

public abstract class AbstractCommand implements Command {

  protected static int getInt(String name, String num, int low) throws IllegalArgumentException {
    try {
      int number = Integer.valueOf(num);
      if (low == 1 && number < 1) {
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be positive.\n");
      }
      if (low == 0 && number < 0) {
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be no-negative.\n");
      }
      return number;
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Operation failed: Invalid " + name + ".\n");
    }
  }


  protected static double getDouble(String name, String num,boolean equal) throws IllegalArgumentException {
    try {
      double number = Double.parseDouble(num);
      if(equal&&number < 0){
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be no-negative.\n");
      }
      if(!equal&&number <= 0){ // false: can not be 0
        throw new IllegalArgumentException("Operation failed: " + name.substring(0, 1).toUpperCase()
                + name.substring(1) + " should be positive.\n");
      }
      return number;
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Operation failed: Invalid " + name + ".\n");
    }
  }

}
