package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.Scanner;

import virtualinvestment.MyCalendar;
import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the add stock by manual command.
 */
public class AddDataByManual extends AbstractCommand {
  private String name;
  private String num;
  private Scanner scan;
  private final Appendable ap;

  /**
   * Initialize an add stock by manual command.
   *
   * @param name the name of the stock to add
   * @param num  the number of prices of the stock to add
   * @param scan the prices of the stock
   * @param ap   the output
   */
  public AddDataByManual(String name, String num, Scanner scan, Appendable ap) {
    this.name = name;
    this.num = num;
    this.scan = scan;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      int number = getInt("number of price",num,1);
      MyCalendar[] dates = new MyCalendar[number];
      double[] price = new double[number];
      ap.append("Please input price as \"date(yyyy - mm - dd - hh)price\". "
              + "Input q to cancel this operation.\n");
      for (int i = 0; i < number; i++) {
        ap.append("Price ").append(String.valueOf(i+1))
                .append(": ");
        String sign = scan.next();
        if(sign.equalsIgnoreCase("q")){
          ap.append("Operation canceled.\n");
          return;
        }
        dates[i] = new MyCalendar(sign);
        double p = getDouble("price",scan.next(),false);
        if ( dates[i].after(GregorianCalendar.getInstance())) {
          ap.append("Operation failed: Date should be earlier than current date.\n");
          return;
        }
        price[i] = p;
      }
      m.addDataByManual(name, dates, price);
      ap.append("Add data successfully.\n");
    } catch (NoSuchElementException e0) {
      ap.append("Operation failed: Miss price.\n");
    } catch (IllegalArgumentException | IllegalStateException e1) {
      ap.append(e1.getMessage());
    }
  }

}
