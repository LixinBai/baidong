package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.GregorianCalendar;

import virtualinvestment.MyCalendar;
import virtualinvestment.controller.Command;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represents the get total value command.
 */
public class GetPurchaseHistory implements Command {
  private String name;
  private String date;
  private final Appendable ap;

  /**
   * Initialize a get purchase history command.
   *
   * @param name the name of the portfolio to get purchase history
   * @param ap   the output
   */
  public GetPurchaseHistory(String name,String date, Appendable ap) {
    this.name = name;
    this.date = date;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment m) throws IOException {
    try {
      MyCalendar cal = new MyCalendar(date);
      if (cal.after(GregorianCalendar.getInstance())) {
        ap.append("Operation failed: Date should be earlier than current date.\n");
        return;
      }
      ap.append(m.getPurchaseHistory(name,cal));
    } catch (IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
