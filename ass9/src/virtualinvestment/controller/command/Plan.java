package virtualinvestment.controller.command;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import virtualinvestment.MyCalendar;
import virtualinvestment.model.VirtualInvestment;

public class Plan extends Invest {
  private String start;
  private String end;
  private String interval;

  public Plan(String name, String start, String end, String interval, String amount, String fee,
              Scanner scan,
              Appendable ap) {
    super(name,start,amount,fee,scan,ap);
    this.start = start;
    this.end = end;
    this.interval = interval;
  }

  @Override
  public void start(VirtualInvestment m) throws IllegalStateException, IOException {
    try {
      MyCalendar startDate = new MyCalendar(start);
      MyCalendar endDate;
      double fee = getDouble("commission fee", this.fee,true);
      if (end == null) {
        endDate = null;
      } else {
        endDate = new MyCalendar(end);
        if (endDate.before(startDate)) {
          ap.append("Operation failed: End date should be later than start date.\n");
          return;
        }
      }
      int interval = getInt("interval days", this.interval,1);
      double amount = getDouble("amount to invest", this.amount,false);
      m.dollarCostAveraging(name, startDate, endDate, interval, amount,fee, getWeight(m));
      ap.append("Create a dollarCostAveraging successfully.\n");
    } catch (NoSuchElementException e0) {
      ap.append("Operation failed: Miss weight.\n");
    } catch (IllegalArgumentException | IllegalStateException e) {
      ap.append(e.getMessage());
    }
  }

}
