package virtualinvestment.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Function;

import virtualinvestment.controller.command.AddData;
import virtualinvestment.controller.command.AddStock;
import virtualinvestment.controller.command.BuyStock;
import virtualinvestment.controller.command.CreatePort;
import virtualinvestment.controller.command.GetCostBasis;
import virtualinvestment.controller.command.GetPurchaseHistory;
import virtualinvestment.controller.command.GetTotalValue;
import virtualinvestment.controller.command.Invest;
import virtualinvestment.controller.command.Plan;
import virtualinvestment.controller.command.ViewPort;
import virtualinvestment.model.VirtualInvestment;

/**
 * This represent an IV controller which will work with the virtual investment model to provide a
 * irtual investment program.
 */
public class VController implements IVController {
  private final Readable rd;
  private final Appendable ap;

  /**
   * Initialize the IV controller with given input and ouput.
   *
   * @param rd the input.
   * @param ap the output.
   * @throws IllegalArgumentException if and only if the appendable objects are null.
   */
  public VController(Readable rd, Appendable ap) {
    if (rd == null) {
      throw new IllegalArgumentException("Readable not found.\n");
    }
    if (ap == null) {
      throw new IllegalArgumentException("Appendable not found.\n");
    }
    this.rd = rd;
    this.ap = ap;
  }

  @Override
  public void start(VirtualInvestment model)
          throws IllegalArgumentException, IllegalStateException {
    if (model == null) {
      throw new IllegalArgumentException("Model not found.\n");
    }
    Scanner scan = new Scanner(rd);
    Map<String, String> messages = new HashMap<>();
    Map<String, Function<Scanner, Command>> knownCommands = new HashMap<>();
    // create a portfolio
    knownCommands.put("createPort", s -> new CreatePort(s.next(), s.next(), s, ap));
    messages.put("createPort", "Please input: portfolio name + number of stocks to be added"
            + "(0 if empty) + stock names(separated by space).\n");
    // add price data
    knownCommands.put("addData", s -> new AddData(s, ap));
    messages.put("addData", "Please input type of source data: internet/file/manual.\n");
    // add a stock to a portfolio
    knownCommands.put("addStock", s -> new AddStock(s.next(), s.next(), ap));
    messages.put("addStock", "Please input: portfolio name + stock name.\n");
    // view composition of a portfolio
    knownCommands.put("viewPort", s -> new ViewPort(s.next(), ap));
    messages.put("viewPort", "Please input: portfolio name.\n");
    // get purchase history of a portfolio
    knownCommands.put("getHistory", s -> new GetPurchaseHistory(s.next(), s.next(), ap));
    messages.put("getHistory", "Please input: portfolio name.\n");
    // buy some stocks in a portfolio
    knownCommands.put("buyStock", s -> new BuyStock(s.next(), s.next(), s.next(), s.next(),
            s.next(), ap));
    messages.put("buyStock", "Please input: stock name + portfolio name + number of shares "
            + "+ commission fee + date(yyyy-mm-dd-hh).\n");
    // invest some amount in a portfolio
    knownCommands.put("invest", s -> new Invest(s.next(), s.next(), s.next(), s.next(), s, ap));
    messages.put("invest", "Please input: portfolio name + date(yyyy-mm-dd-hh) + amount to invest "
            + "+ commission fee.\n");
    // create an invest dollarCostAveraging of a portfolio
    knownCommands.put("dollarCostAveraging", s -> new Plan(s.next(), s.next(), s.next(), s.next(), s.next(),
            s.next(), s, ap));
    messages.put("dollarCostAveraging", "Please input: portfolio name + start date(yyyy-mm-dd-hh) "
            + "+ end date(yyyy-mm-dd-hh）or null if ongoing + interval days + invest amount "
            + "+ commission fee.\n");
    // get cost basis of a portfolio
    knownCommands.put("getCostBasis", s -> new GetCostBasis(s.next(), s.next(), ap));
    messages.put("getCostBasis", "Please input: portfolio name + date(yyyy-mm-dd-hh).\n");
    // get total value of a portfolio
    knownCommands.put("getTotalValue", s -> new GetTotalValue(s.next(), s.next(), ap));
    messages.put("getTotalValue", "Please input: portfolio name + date(yyyy-mm-dd-hh).\n");
    while (scan.hasNext()) {
      try {
        Command c;
        String in = scan.next();
        if (in.equalsIgnoreCase("q") || in.equalsIgnoreCase("quit")) {
          ap.append("Already quit the program.\n");
          return;
        }else if(in.equalsIgnoreCase("help")){
          ap.append("Please input command: createPort, viewPort, getHistory, addData, addStock, "
                  +"buyStock, invest, dollarCostAveraging, getCostBasis, getTotalValue, quit.\n");
          continue;
        }
        String message = messages.getOrDefault(in, null);
        Function<Scanner, Command> cmd = knownCommands.getOrDefault(in, null);
        if (cmd == null) {
          ap.append("Command not found.\n");
        } else {
          ap.append(message);
          try {
            c = cmd.apply(scan);
            c.start(model);
          } catch (NoSuchElementException e) {
            ap.append("Operation failed: Miss input.\n");
          }
        }
      //}
      //catch(IllegalArgumentException e){
       // ap.append(e.getMessage());
      } catch (IOException e) {
        throw new IllegalStateException("The appendable object failed.");
      }
    }
  }

}
