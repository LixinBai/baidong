package virtualinvestment;

import java.time.Year;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a calendar which represent a certain time.
 */
public class MyCalendar extends GregorianCalendar {

  /**
   * Take the time as a string, initialize a new calendar.
   *
   * @param date the time as a string as "yyyy-mm-dd-hh"
   * @throws IllegalArgumentException if the time is invalid or after now.
   */
  public MyCalendar(String date) throws IllegalArgumentException {
    super();
    int[] cal = match(date);
    set(YEAR, cal[0]);
    set(MONTH, cal[1]);
    set(DATE, cal[2]);
    set(HOUR_OF_DAY, cal[3]);
    if (after(GregorianCalendar.getInstance())) {
      throw new IllegalArgumentException("Operation failed: Invalid date.\n");
    }
    if (get(YEAR) != cal[0] || get(MONTH) != cal[1] || get(DATE) != cal[2] || get(HOUR_OF_DAY) != cal[3]) {
      throw new IllegalArgumentException("Operation failed: Invalid date.\n");
    }
  }

  /*public int  getYear(){
    return get(YEAR);
  }

  public int getMonth(){
    return get(MONTH+1);
  }

  public int getDate(){
    return get(DATE);
  }

  public int getHour(){
    return get(HOUR_OF_DAY);
  }*/

  @Override
  public String toString() {
    return String.valueOf(get(Calendar.YEAR)) + "-" + (get(Calendar.MONTH) + 1) + "-"
            + get(Calendar.DATE) + "-" + get(Calendar.HOUR_OF_DAY);
  }

  /**
   * Determine whether the calendar represents a weekend day.
   *
   * @return true if the calendar represents a weekend day, otherwise no.
   */
  //public Boolean isWeekend() {
  //  int day = this.get(DAY_OF_WEEK);
  //  return day == SATURDAY || day == SUNDAY;
  //}
  public String isClose() {
    int month = this.get(MONTH);
    int date = this.get(DATE);
    int weekOfMonth = this.get(WEEK_OF_MONTH);
    int dayOfWeek = this.get(DAY_OF_WEEK);
    int hour = this.get(HOUR_OF_DAY);
    if (dayOfWeek == SATURDAY || dayOfWeek == SUNDAY) {
      return "weekend";
    } else if (hour < 9) {
      return "before 9 am";
    } else if (hour > 16) {
      return "after 4 pm";
    } else if (month == JANUARY && date == 1) {
      return "New Year's Day";
    } else if (month == DECEMBER && date == 31 && dayOfWeek == FRIDAY) {
      return "New Year's Day";
    } else if (month == JANUARY && date == 2 && dayOfWeek == MONDAY) {
      return "New Year's Day";
    } else if (month == JANUARY && weekOfMonth == 3 && dayOfWeek == MONDAY) {
      return "Martin Luther King, Jr. Day";
    } else if (month == FEBRUARY && weekOfMonth == 3 && dayOfWeek == MONDAY) {
      return "George Washington’s Birthday";
    } else if (this.goodFriday()) {
      return "Good Friday";
    } else if (month == MAY && date + 7 > 31 && dayOfWeek == MONDAY) {
      return "Memorial Day";
    } else if (month == JULY && date == 4) {
      return "Independence Day";
    } else if (month == JULY && date == 3 && dayOfWeek == FRIDAY) {
      return "Independence Day";
    } else if (month == JULY && date == 5 && dayOfWeek == MONDAY) {
      return "Independence Day";
    } else if (month == SEPTEMBER && weekOfMonth == 1 && dayOfWeek == MONDAY) {
      return "Labor Day";
    } else if (month == NOVEMBER && weekOfMonth == 4 && dayOfWeek == THURSDAY) {
      return "Thanksgiving Day";
    } else if (month == DECEMBER && date == 25) {
      return "Christmas Day";
    } else if (month == DECEMBER && date == 24 && dayOfWeek == FRIDAY) {
      return "Christmas Day";
    } else if (month == DECEMBER && date == 26 && dayOfWeek == MONDAY) {
      return "Christmas Day";
    }
    return null;
  }

  private boolean goodFriday() {
    int year = this.get(YEAR);
    int month;
    int date;
    int n = year - 1900;
    int a = n % 19;
    int q = n / 4;
    int b = (7 * a + 1) / 19;
    int m = (11 * a + 4 - b) % 29;
    int w = (n + q + 31 - m) % 7;
    int d = 25 - m - w;
    if (d > 0) {
      month = APRIL;
      date = d;
    } else if (d < 0) {
      month = MARCH;
      date = 31 + d;
    } else {
      month = MARCH;
      date = 31;
    }
    if (date > 2) {
      date = date - 2;
    } else {
      month = month - 1;
      date = 31 - date;
    }
    return this.get(MONTH) == month && this.get(DATE) == date;
  }

  private static int[] match(String date) throws IllegalArgumentException {
    int[] cal = new int[4];
    Pattern toHour = Pattern.compile("^(\\d{4})-{1}(\\d{1,2})-{1}(\\d{1,2})-{1}(\\d{1,2})$");
    Matcher tH = toHour.matcher(date);
    if (!tH.find()) {
      throw new IllegalArgumentException("Operation failed: Invalid date format.\n");
    }
    cal[0] = Integer.valueOf(tH.group(1));
    cal[1] = Integer.valueOf(tH.group(2)) - 1;
    cal[2] = Integer.valueOf(tH.group(3));
    cal[3] = Integer.valueOf(tH.group(4));
    //if (cal[1] < 0 || cal[1] > 11 || cal[2] < 1 || cal[2] > 31 || cal[3] < 0 || cal[3] > 23) {
    //  throw new IllegalArgumentException("Operation failed: Invalid date.\n");
    //}
    return cal;
  }

  /*@Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + getYear();
    result = prime * result + getMonth();
    result = prime * result + getDate();
    result = prime * result + getHour();
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null)
      return false;
    if (getClass() != o.getClass())
      return false;
    MyCalendar other = (MyCalendar) o;
    return this.getYear() == other.getYear()
            && this.getMonth() == other.getMonth()
            && this.getDate() == other.getDate()
            && this.getHour() == other.getHour();
  }*/
}
