package virtualinvestment;

import com.sun.tools.internal.ws.processor.model.Model;

import java.io.InputStreamReader;

import virtualinvestment.controller.IVController;
import virtualinvestment.controller.VController;
import virtualinvestment.model.InvestmentModel;
//import virtualinvestment.model.MockModel1;
import virtualinvestment.model.VirtualInvestment;

/**
 * This class represent the view of this program.
 */
public class View {

  /**
   * The main method.
   */
  public static void main(String[] args) {
    Readable in = new InputStreamReader(System.in);
    Appendable out = System.out;
    VirtualInvestment model = new InvestmentModel();
    //VirtualInvestment model = new MockModel1(new StringBuilder(),12.23);
    IVController controller = new VController(in, out);
    System.out.println("Please input command: createPort, viewPort, getHistory, addData, addStock, "
            +"buyStock, invest, dollarCostAveraging, getCostBasis, getTotalValue, quit.\n"
            + "Input help to get this message again.\n");
    controller.start(model);
  }

}
