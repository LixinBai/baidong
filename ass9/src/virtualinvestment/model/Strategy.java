package virtualinvestment.model;

import java.util.List;

import virtualinvestment.MyCalendar;

public interface Strategy {
  List<Property> apply(MyCalendar time);
}
