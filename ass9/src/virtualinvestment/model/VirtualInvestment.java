package virtualinvestment.model;

import java.io.File;

import virtualinvestment.MyCalendar;

/**
 * This interface represents all possible operations for a virtual investment system.
 */
public interface VirtualInvestment {

  /**
   * Allow users to fetch information from Internet and create a new stock with given name.
   *
   * @param name name of the stock
   * @throws IllegalStateException if the stock is already exist or the alphavantage API has either
   *                               changed or no longer works or there is no price found on the
   *                               Internet.
   */
  void addDataFromInternet(String name) throws IllegalStateException;

  /**
   * Allow users to load information from files and create a new stock with given name. Information
   * in file must in cvs format, which consists of several lines, each line being separated by
   * commas. Each line contains the date, price at opening time, highest price for that date, lowest
   * price for that date, price at closing time and the volume of trade (no. of shares bought/sold)
   * on that date.
   *
   * @param name name of the stock
   * @param file file to load
   * @throws IllegalStateException if the stock is already exist or the given file is not found or
   *                               an I/O error occurred.
   */
  void addDataFromFile(String name, File file) throws IllegalStateException;

  /**
   * Allow users to create a new stock with given name.
   *
   * @param sName stock name
   * @throws IllegalStateException if the given stock name is already exist， or time of the price is
   *                               a holiday
   */
  void addDataByManual(String sName, MyCalendar[] times, double[] prices)
          throws IllegalStateException;

  /**
   * Create a new portfolio.
   *
   * @param pName name of the portfolio
   * @throws IllegalStateException if the portfolio name is already exist
   */
  void createPortfolio(String pName) throws IllegalStateException;

  //new

  /**
   * Create a portfolio with specific composition of stocks.
   *
   * @param pName  name of the portfolio
   * @param sNames names of stocks
   * @throws IllegalStateException if the portfolio name is already exist, or one or more stocks do
   *                               not exist in this system
   */
  void createPortfolio(String pName, String[] sNames) throws IllegalStateException;

  //new

  /**
   * Add a stock to a portfolio.
   *
   * @param pName name of the portfolio
   * @param sName name of the stock
   * @throws IllegalStateException if the portfolio or stock name does not exist, or the stock is
   *                               already exist in given portfolio
   */
  void addStock(String pName, String sName) throws IllegalStateException;

  //new

  /**
   * Get all the stock names in given portfolio.
   *
   * @param pName name of the portfolio
   * @return all stock names
   * @throws IllegalStateException if the portfolio name does not exist
   */
  String viewPortfolio(String pName) throws IllegalStateException;

  /**
   * Get compositions of the given portfolio at a specific time. The compositions will be printed in
   * dictionary order.
   *
   * @param pName name of the portfolio
   * @return composition of given portfolio as a formatted string
   * @throws IllegalStateException if the portfolio name does not exist, or price data is missed for
   *                               some date the user should have bought stocks
   */
  String getPurchaseHistory(String pName, MyCalendar time) throws IllegalStateException;

  /**
   * Buy shares of some stock in a portfolio worth a certain amount at a certain date. Users must
   * chose a portfolio to put the stock in.
   *
   * @param pName  name of the portfolio
   * @param sName  name of the stock
   * @param shares amount of share as a double
   * @param fee    amount of commission fee
   * @param time   the time user want to buy
   * @throws IllegalStateException if the portfolio or stock does not exist, users are trying to buy
   *                               stocks at a close time, there is no price at given time, or the
   *                               stock does not exist in given portfolio
   */
  void buyStock(String pName, String sName, double shares, double fee, MyCalendar time)
          throws IllegalStateException;///////int -> double, add a commission fee

  //new

  //in model
  //portfolio should exist
  //time should not a close day
  //throw exception: in portfolio
  //stock.ableToBuy

  /**
   * Invest a fixed amount into an existing portfolio containing multiple stocks, using a specified
   * weight for each stock in the portfolio.
   *
   * @param pName   name of the portfolio
   * @param time    time of investment
   * @param amount  amount of money to invest
   * @param fee     commission fee for each transaction
   * @param weights weights for each stocks
   * @throws IllegalStateException if given portfolio does not exist, stock market is closed at
   *                               given date, or price data is missed for some of the stocks at
   *                               given date
   */
  void invest(String pName, MyCalendar time, double amount, double fee, double[] weights)
          throws IllegalStateException;

  //new

  /**
   * Apply a dollar-cost averaging strategy. Dollar-cost averaging strategy is a long-term passive
   * investment strategy which invests a fixed amount of money in this portfolio at regular
   * frequency with a fixed proportion for each stocks. The commission fees for each transaction in
   * the same strategy are identical.
   *
   * @param pName    name of the portfolio
   * @param start    start time
   * @param end      end time
   * @param interval frequency of investment
   * @param amount   amount of money to invest for each time
   * @param fee      commission fee
   * @param weights  weights for each stocks
   * @throws IllegalStateException if the given portfolio does not exist
   */
  //--->dollarCostAveraging
  void dollarCostAveraging(String pName, MyCalendar start, MyCalendar end, int interval, double amount, double fee,
                           double[] weights) throws IllegalStateException;

  /**
   * Get cost basis of a portfolio at a specific time. The system will calculate the costs before
   * that given time.
   *
   * @param pName name of the portfolio
   * @param time  time before which user want to get cost basis
   * @return cost basis of given portfolio before given time
   * @throws IllegalStateException if the portfolio name does not exist, or some price data at time
   *                               strategies should apply are missed
   */
  double getCostBasis(String pName, MyCalendar time) throws IllegalStateException;

  /**
   * Get total value of a portfolio at a specific time.
   *
   * @param pName name of the portfolio
   * @param time  time at which user want to get total value
   * @return total value of given portfolio at given time
   * @throws IllegalStateException if the portfolio name does not exist, or some price data at time
   *                               strategies should apply are missed
   */
  double getTotalValue(String pName, MyCalendar time) throws IllegalStateException;

}
