package virtualinvestment.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import virtualinvestment.MyCalendar;

/**
 * This class represents a portfolio in a virtual investment system.
 */
class Portfolio {
  private Set<Stock> stocks;
  private List<Property> properties;
  //private List<Property> planProperties;
  private List<Strategy> plans;///new

  /**
   * A constructor for a portfolio. Initially a portfolio is empty.
   */
  Portfolio() {
    this.stocks = new HashSet<>();
    this.properties = new LinkedList<>();
    this.plans = new LinkedList<>();
    //this.properties = new TreeMap();
  }

  // new
  void addStock(Stock stock) throws IllegalStateException {
    if (stocks.contains(stock)) {
      throw new IllegalStateException("Operation failed: Stock already exists.\n");
    }
    stocks.add(stock);
  }

  //new
  String getStocks() {
    StringBuilder str = new StringBuilder();
    for (Stock s : stocks) {
      str.append(s.getName());
      str.append(" ");
    }
    return str.toString();
  }

  double getStockSize() {
    return stocks.size();
  }

  /**
   * Buy some stocks to given portfolio.
   *
   * @param time  buying time
   * @param stock stock name
   * @param share amount of stock
   */
  // addStock-> buyStock
  void buyStock(MyCalendar time, Stock stock, double share, double commissionFee) {
    if (!stocks.contains(stock)) {
      throw new IllegalStateException("Operation failed: Stock is not in this portfolio.\n");
    }
    Property p = new Property(time, stock, share, commissionFee);
    properties.add(p);
  }

  /**
   * Get cost basis of this portfolio before given time.
   *
   * @param time time with which to find cost basis
   * @return cost basis of this portfolio
   */
  double getCostBasis(MyCalendar time) {
    //add dollarCostAveraging properties first
    double plain = properties.stream().filter(p -> !p.getTime().after(time))
            .map(c -> c.getStock().getValue(c.getTime()) * c.getShare() + c.getCommissionFee())
            .reduce(0.0, (a, b) -> a + b);
    //+ planProperties
    List<Property> planProperties = new LinkedList<>();
    for (Strategy strategy : plans) {
      planProperties.addAll(strategy.apply(time));
    }
    double plan = planProperties.stream().map(c -> c.getStock().getValue(c.getTime()) * c.getShare()
            + c.getCommissionFee()).reduce(0.0, (a, b) -> a + b);
    return plain + plan;
  }

  /**
   * Get total value of this portfolio.
   *
   * @param time time with which to find total value
   * @return total value of this portfolio
   */
  double getTotalValue(MyCalendar time) {
    //add dollarCostAveraging properties first
    double plain = properties.stream().filter(p -> !p.getTime().after(time))
            .map(c -> c.getStock().getValue(time) * c.getShare()).reduce(0.0, (a, b) -> a + b);
    //+ planProperties.
    List<Property> planProperties = new LinkedList<>();
    for (Strategy strategy : plans) {
      planProperties.addAll(strategy.apply(time));
    }
    double plan = planProperties.stream().map(c -> c.getStock().getValue(time) * c.getShare()).reduce(0.0, (a, b) -> a + b);
    return plain + plan;
  }

  //new
  void invest(double[] amounts, MyCalendar time, double commissionFee) throws IllegalStateException {
    //buy stocks
    List<Property> invest = new LinkedList<>();
    Iterator i = stocks.iterator();
    for (int j = 0; j < amounts.length; j++) {
      Stock s = (Stock) i.next();
      if (s.ableToBuy(time)) {
        double share = s.getShare(time, amounts[j]);
        Property p = new Property(time, s, share, commissionFee);
        invest.add(p);
      } else {
        throw new IllegalStateException("Operation failed: Miss price data.\n");
      }
    }
    properties.addAll(invest);
  }

  //new
  void addStrategy(Strategy strategy) {
    plans.add(strategy);
  }


  /**
   * Get composition of this portfolio.
   *
   * @return composition as a formatted string
   */
  public String toString(MyCalendar time) {
    List<Property> filter = properties.stream().filter(c -> !c.getTime().after(time)).collect(Collectors.toList());

    //+ planPropertie.
    for (Strategy strategy : plans) {
      filter.addAll(strategy.apply(time));
    }

    SortedMap<String, Double> result = new TreeMap<>();
    Iterator i = filter.iterator();
    while (i.hasNext()) {
      Property p = (Property) i.next();
      String stock = p.getStock().getName();
      String cost = Double.toString(p.getStock().getValue(p.getTime()));
      String key = "Stock Name: " + stock + "  Unit Price: " + cost;
      if (result.containsKey(key)) {
        //merge
        result.compute(key, (k, v) -> v += p.getShare());
      } else {
        result.put(key, p.getShare());
      }
    }
    StringBuilder sb = new StringBuilder();
    Iterator<Map.Entry<String, Double>> i1 = result.entrySet().iterator();
    while (i1.hasNext()) {
      Map.Entry<String, Double> entry = i1.next();
      sb.append(entry.getKey()).append("  Shares: ").append(String.format("%.2f", entry.getValue()))
              .append("\n");
    }
    if (sb.toString().equals("")) {
      return "This portfolio is empty.\n";
    }
    return sb.toString();
  }

}