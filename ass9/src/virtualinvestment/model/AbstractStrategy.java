package virtualinvestment.model;

import java.util.HashMap;

import virtualinvestment.MyCalendar;

public abstract class AbstractStrategy implements Strategy {
  protected MyCalendar start;
  protected MyCalendar end;
  protected HashMap<Stock, Double> amounts;
  protected double fee;

  public AbstractStrategy(MyCalendar start, MyCalendar end, HashMap<Stock, Double> amounts, double fee) {
    this.start = start;
    this.end = end;
    this.amounts = amounts;
    this.fee = fee;
  }
}
