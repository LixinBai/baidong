package virtualinvestment.model;

import virtualinvestment.MyCalendar;

/**
 * This cass represents a property in a portfolio. A property consists of stock, its buying time
 * and amount of shares.
 */
////////// add commission fee
class Property {
  private MyCalendar time;
  private Stock stock;
  private double share;
  private double commissionFee;

  /////add commission fee
  Property(MyCalendar time, Stock stock, double share, double commissionFee) {
    this.time = time;
    this.stock = stock;
    this.share = share;
    this.commissionFee = commissionFee;
  }

  MyCalendar getTime() {
    return time;
  }

  Stock getStock() {
    return stock;
  }

  double getShare() {
    return share;
  }

  double getCommissionFee() {
    return commissionFee;
  }

}
