package virtualinvestment.model;

import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import virtualinvestment.MyCalendar;

/**
 * This class represent a single stock in a virtual investment system.
 */
class Stock {
  private String name;
  private SortedMap<String, Double> price;

  /**
   * A constructor for a stock.
   *
   * @param name name of the stock
   */
  Stock(String name) {
    this.name = name;
    price = new TreeMap<>();
  }

  /**
   * Add a new entry to price table of this stock.
   *
   * @param time time of the price to be added
   * @param p    price of given time
   * @throws IllegalArgumentException if there is already a price for given time
   */
  void addPrice(MyCalendar time, double p) throws IllegalArgumentException {
    if (price.containsKey(time.toString())) {
      throw new IllegalArgumentException("Operation failed: This time already has price");
    }
    price.put(time.toString(), p);
  }

  /**
   * Judge if the stock can be bought at the given time. A stock cannot be bought before it has
   * prices.
   *
   * @param time buying time
   * @return whether the stock can be bought
   */
  //change
  boolean ableToBuy(MyCalendar time) {
    return price.containsKey(time.toString());
  }

  /**
   * Get proper value of a stock ar given time.
   *
   * @param time time with which to find a value
   * @return value at given time
   */
  double getValue(MyCalendar time) throws IllegalStateException{
    //int i = 0;
    ////delete this part
    /*Iterator<Map.Entry<MyCalendar, Double>> iterator = price.entrySet().iterator();
    Map.Entry<MyCalendar, Double> current = iterator.next();
    while (iterator.hasNext()) {
      Map.Entry<MyCalendar, Double> next = iterator.next();
      if (!next.getKey().after(time)) {
        //i += 1;
        current = next;
      } else {
        return current.getValue();
      }
    }
    return current.getValue();*/
    if(price.containsKey(time.toString())) {
      return price.get(time.toString());
    }
    else {
      throw new IllegalStateException("Operation failed: No data for " + time.toString() + ".\n");
    }
  }

  /**
   * Get the name of this stock.
   *
   * @return name of this stock
   */
  String getName() {
    return this.name;
  }

  double getShare(MyCalendar time, double amount) {
    double p = price.get(time.toString());
    return amount / p;
  }
}
