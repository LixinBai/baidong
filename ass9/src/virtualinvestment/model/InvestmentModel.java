
package virtualinvestment.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import virtualinvestment.MyCalendar;


/**
 * This is an implementation of virtual investment system.
 */
public class InvestmentModel implements VirtualInvestment {
  //private double commissionFee = 5.0;
  private HashMap<String, Stock> stocks;
  private HashMap<String, Portfolio> portfolios;

  /**
   * A constructor for investment system. initially the system contains no stocks and portfolios.
   */
  public InvestmentModel() {
    stocks = new HashMap<>();
    portfolios = new HashMap<>();
  }

  @Override
  public void addDataFromInternet(String name) throws IllegalStateException {
    if (stocks.containsKey(name)) {
      throw new IllegalStateException("Operation failed: Stock name is already exist.\n");
    }
    String apiKey = "IHM5FSYKD3V4O31Y";//IHM5FSYKD3V4O31Y
    String stockSymbol = name; //ticker symbol for Google
    URL url = null;

    try {
      url = new URL("https://www.alphavantage"
              + ".co/query?function=TIME_SERIES_DAILY"
              + "&outputsize=full"
              + "&symbol"
              + "=" + stockSymbol + "&apikey=" + apiKey + "&datatype=csv");
    } catch (MalformedURLException e) {
      throw new RuntimeException("the alphavantage API has either changed or "
              + "no longer works");
    }

    InputStream in = null;
    try {
      in = url.openStream();
      Stock stock = new Stock(name);
      Scanner sc = new Scanner(in);
      sc.next();
      while (sc.hasNext()) {
        String line = sc.next();
        //System.out.println(line);
        addDataHelper(line, stock);
      }
      sc.close();
      stocks.put(name, stock);
    } catch (IOException e) {
      throw new IllegalStateException("No price data found for " + stockSymbol);
    }
  }

  @Override
  public void addDataFromFile(String name, File file) throws IllegalStateException {
    //file format: yyyy,mm,dd,hh,price
    if (stocks.containsKey(name)) {
      throw new IllegalStateException("Operation failed: Stock name is already exist.\n");
    }
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      Stock stock = new Stock(name);
      for (String line; (line = br.readLine()) != null; ) {
        // process the line.
        addDataHelper(line, stock);
      }
      stocks.put(name, stock);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalStateException("Operation failed: File not found.\n");
    } catch (IOException e) {
      //e.printStackTrace();
      throw new IllegalStateException("Operation failed: I/O Exception.\n");
    }
  }

  private void addDataHelper(String line, Stock stock) {
    String[] entry = line.split(",");
    try {
      MyCalendar openTime = new MyCalendar(entry[0] + "-9");
      stock.addPrice(openTime, Double.parseDouble(entry[1]));
    } catch (IllegalArgumentException e) {
      //ignore this entry
    }
    try {
      MyCalendar closeTime = new MyCalendar(entry[0] + "-16");
      stock.addPrice(closeTime, Double.parseDouble(entry[4]));
    } catch (IllegalArgumentException e) {
      //ignore this entry
    }
  }

  @Override
  public void addDataByManual(String sName, MyCalendar[] times, double[] prices)
          throws IllegalStateException {
    //throw exception if input a price on weekend or holiday
    if (stocks.containsKey(sName)) {
      throw new IllegalStateException("Operation failed: Stock name is already exist.\n");
    }

    Stock stock = new Stock(sName);
    String close;
    for (int i = 0; i < prices.length; i++) {
      close = times[i].isClose();
      if(close != null) {
        throw new IllegalStateException("Operation failed: " + times[i].toString() + "is " + close + ".\n");
      }
      stock.addPrice(times[i], prices[i]);
    }
    stocks.put(sName, stock);
  }

  @Override
  public void createPortfolio(String pName) throws IllegalStateException {
    if (portfolios.containsKey(pName)) {
      throw new IllegalStateException("Creation failed: Portfolio name already exists.\n");
    }
    Portfolio port = new Portfolio();
    portfolios.put(pName, port);
  }

  @Override
  // new constructor
  public void createPortfolio(String pName, String[] sNames) throws IllegalStateException {
    if (portfolios.containsKey(pName)) {
      throw new IllegalStateException("Creation failed: Portfolio name already exists.\n");
    }
    Portfolio port = new Portfolio();
    for (String s : sNames) {
      if (!stocks.containsKey(s)) {
        throw new IllegalStateException("Operation failed: Stock " + s + " does not exist.\n");
      }
      port.addStock(stocks.get(s));
    }

    portfolios.put(pName, port);
  }

  @Override
  //new
  public void addStock(String pName, String sName) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not found.\n");
    }
    Portfolio port = portfolios.get(pName);
    if (stocks.containsKey(sName)) {
      port.addStock(stocks.get(sName));
    } else {
      throw new IllegalStateException("Operation failed: Stock " + sName + " does not exist.\n");
    }
  }

  //new
  @Override
  public String viewPortfolio(String pName) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not found.\n");
    }

    return portfolios.get(pName).getStocks();
  }

  @Override
  public String getPurchaseHistory(String pName, MyCalendar time) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not found.\n");
    }
    return portfolios.get(pName).toString(time);
  }

  // commission fee
  // int -> double
  @Override
  public void buyStock(String pName, String sName, double shares, double fee, MyCalendar time)
          throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Portfolio p = portfolios.get(pName);
    if (!stocks.containsKey(sName)) {
      throw new IllegalStateException("Operation failed: Stock does not exist.\n");
    }
    Stock s = stocks.get(sName);
    /*if (!p.getStocks().contains(s)) {
      throw new IllegalStateException("Operation failed: Stock is not in this portfolio.\n");
    }*/
    String close = time.isClose();
    if (close != null) {
     throw new IllegalStateException("Operation failed: This time is " + close + ".\n");
    }
    if(!s.ableToBuy(time)){
      throw new IllegalStateException("Operation failed: Miss price data.\n");
    }
    p.buyStock(time, s, shares, fee);
  }

  //throw exception: in model
  //portfolio should exist
  //time should not a close day
  //throw exception: in portfolio
  //stock.ableToBuy
  @Override
  public void invest(String pName, MyCalendar time, double amount, double fee, double[] weights)
          throws IllegalStateException {
    //portfolio should exist
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    //time to buy should not close
    String close = time.isClose();
    if (close != null) {
      throw new IllegalStateException("Operation failed: This time is " + close + ".\n");
    }
    Portfolio port = portfolios.get(pName);
    double[] amounts;
    amounts = new double[weights.length];
    for(int i = 0; i < weights.length; i++) {
      amounts[i] = amount * weights[i];
    }
    port.invest(amounts, time, fee);
  }

  /*@Override
  public void investEqualWeights(String pName, MyCalendar time, double amount, double fee)
          throws IllegalStateException {
    //portfolio should exist
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    //time to buy should not close
    String close = time.isClose();
    if (close != null) {
      throw new IllegalStateException("Operation failed: This time is " + close + ".\n");
    }
    Portfolio port = portfolios.get(pName);
    double[] amounts;
    amounts = new double[(int)port.getStockSize()];
    double weight = 1 / port.getStockSize();
    for (int i = 0; i < port.getStockSize(); i ++) {
      amounts[i] = amount * weight;
    }
    port.invest(amounts, time, fee);
  }*/

  //dollarCostAveraging
  @Override
  public void dollarCostAveraging(String pName, MyCalendar start, MyCalendar end, int interval, double amount, double fee,
                                  double[] weights) throws IllegalStateException {
    //portfolio should exist
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    double[] amounts;
    amounts = new double[weights.length];
    for(int i = 0; i < weights.length; i++) {
      amounts[i] = amount * weights[i];
    }
    Portfolio port = portfolios.get(pName);
    HashMap<Stock, Double> w = addStrategyHelper(port, amounts);
    Strategy strategy = new DollarCostAveraging(start, end, w, fee, interval);
    port.addStrategy(strategy);
  }

  private HashMap<Stock, Double> addStrategyHelper(Portfolio port, double[] amounts) {
    HashMap<Stock, Double> result = new HashMap<>();
    String s = port.getStocks();
    String[] stocks = s.split(" ");

    for(int i = 0; i < port.getStockSize(); i++) {
      result.put(this.stocks.get(stocks[i]), amounts[i]);
    }
    return result;
  }

  @Override
  public double getCostBasis(String pName, MyCalendar time) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Portfolio port = portfolios.get(pName);
    return port.getCostBasis(time);
  }

  @Override
  public double getTotalValue(String pName, MyCalendar time) throws IllegalStateException {
    if (!portfolios.containsKey(pName)) {
      throw new IllegalStateException("Operation failed: Portfolio name does not exist.\n");
    }
    Portfolio port = portfolios.get(pName);
    return port.getTotalValue(time);
  }

}
