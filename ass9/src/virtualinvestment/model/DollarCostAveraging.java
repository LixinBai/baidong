package virtualinvestment.model;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import virtualinvestment.MyCalendar;

import static java.util.Calendar.DATE;

public class DollarCostAveraging extends AbstractStrategy {
  private int interval;
  public DollarCostAveraging(MyCalendar start, MyCalendar end, HashMap<Stock, Double> amounts, double fee, int interval) {
    super(start, end, amounts, fee);
    this.interval = interval;
  }

  @Override
  public List<Property> apply(MyCalendar time) {
    List<Property> result = new LinkedList<>();
    if(time.before(start)) {
      return result;
    }
    if(end == null) {
      //calculate properties from start to given time
      result = applyHelper(start, time);
    }
    else {
      if(time.after(end)) {
        //calculate properties from start to end
        result = applyHelper(start, end);
      }
      else {
        //calculate properties from start to given time
        result = applyHelper(start, time);
      }
    }
    return result;
  }

  private List<Property> applyHelper(MyCalendar start, MyCalendar end) {
    List<Property> result = new LinkedList<>();
    //find times
    List<MyCalendar> times = new LinkedList<>();
    MyCalendar temp = new MyCalendar(start.toString());
    while(!temp.after(end)) {
      times.add(new MyCalendar(temp.toString()));
      temp.roll(DATE, interval);
    }
    MyCalendar prev = null;
    for (MyCalendar m: times) {
      if(m.equals(prev)) {
        times.remove(m);
        continue;
      }
      else if(m.isClose() != null) {
        //next available day
        while(m.isClose() != null) {
          m.roll(DATE, 1);
        }
      }
      prev = m;
    }

    //for each stocks, buy it at each time
    Iterator<Map.Entry<Stock, Double>> i = amounts.entrySet().iterator();
    while(i.hasNext()) {
      Map.Entry<Stock, Double> entry = i.next();
      Stock s = entry.getKey();
      for (MyCalendar t :
              times) {
        /*//if the stock is close, choose the next available day to invest.
        double share = s.getShareAtProperDay(t, entry.getValue());
        Property p = new Property(t, s, share, commissionFee);
        result.add(p);*/
        if(!s.ableToBuy(t)) {
          throw new IllegalStateException("Operation failed: No data for " + t.toString() + ".\n");
        }
        double share = s.getShare(t, entry.getValue());
        Property p = new Property(t, s, share, fee);
        result.add(p);
      }
    }

    return result;
  }
}
